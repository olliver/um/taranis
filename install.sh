#! /usr/bin/env bash

set -e

tar -zxf asn1crypto-0.18.5.tar.gz
tar -zxf m2m_certificates-0.1.1.tar.gz
tar -zxf nfcpy-0.10.3.tar.gz
tar -zxf pynfc-1.7.1.tar.gz

source_pkgs=(pynfc-1.7.1 nfcpy-0.10.3 m2m_certificates-0.1.1 asn1crypto-0.18.5)

for subdir in "${source_pkgs[@]}"
do
  echo "Installing ${subdir}..."
  pushd .
  cd "${subdir}"
  sudo python3 setup.py install
  popd
done

sudo dpkg -i opinicus*-Linux.deb
sudo dpkg -i taranis-0.9.0-Linux.deb

sudo apt-get -f install  # Installing missing Debian dependencies

sudo mkdir -p /usr/share/fdm_materials
sudo git clone https://github.com/Ultimaker/fdm_materials /usr/share/fdm_materials
