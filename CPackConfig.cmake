set(CPACK_PACKAGE_VENDOR "Ultimaker")
set(CPACK_PACKAGE_CONTACT "Ultimaker <info@ultimaker.com>")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Ultimaker taranis")
set(CPACK_PACKAGE_VERSION_MAJOR 0)
set(CPACK_PACKAGE_VERSION_MINOR 3)
set(CPACK_PACKAGE_VERSION_PATCH 0)
if(NOT DEFINED CPACK_PACKAGE_VERSION)
    set(CPACK_PACKAGE_VERSION "0.9.0")
endif()

set(CPACK_GENERATOR "DEB")

set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE all)

#set(CPACK_DEBIAN_PACKAGE_DEPENDS "systemd")
set(CPACK_DEBIAN_PACKAGE_SECTION utils)
set(CPACK_DEBIAN_PACKAGE_PRIORITY optional)
set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/system_config/postinst" "${CMAKE_CURRENT_SOURCE_DIR}/system_config/prerm")

set(DEB_DEPENDS
	"python3 (>= 3.4.0)"
	"python3-dev  (>= 3.4.0)"
	"python3-setuptools (>= 1.8.0)"
	"python3-pyqt5 (>= 5.3)"
	"python3-nfcpy (>= 0.10.3)"
	"python3-pynfc (>= 1.7.1)"
	"libnfc5 (>= 1.7.1)"
    # "griffin" # TODO: specify version
    "dbus (>= 1.8.0)"
    "openssl (>= 1.0.1)"
    "libssl-dev (>= 1.0.1)"
    "libffi-dev (>= 3.1)"
)

string(REPLACE ";" ", " DEB_DEPENDS "${DEB_DEPENDS}")
set(CPACK_DEBIAN_PACKAGE_DEPENDS ${DEB_DEPENDS})

include(CPack)
