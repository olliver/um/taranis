#! /usr/bin/env bash

set -e

mkdir -p build

pushd .
cd build

if ! [ -d "pynfc" ];
then
  git clone https://github.com/LoyVanBeek/pynfc
fi

if ! [ -d "nfcpy" ];
then
    git clone https://github.com/LoyVanBeek/nfcpy
fi

if ! [ -d "m2m_certificates" ];
then
  git clone https://github.com/LoyVanBeek/m2m_certificates
fi

if ! [ -d "asn1crypto" ];
then
  git clone https://github.com/wbond/asn1crypto -b 0.18.5
fi

if ! [ -d "opinicus" ];
then
  git clone https://github.com/Ultimaker/opinicus -b EM-1032
fi

source_pkgs=(pynfc nfcpy m2m_certificates asn1crypto)

for subdir in "${source_pkgs[@]}"
do
  echo "Packaging ${subdir}..."
  pushd .
  cd "${subdir}"
  python3 setup.py --command-packages=stdeb.command sdist_dsc
  cp "${subdir}"*.tar.gz ..
  popd
done

deb_pkgs=(opinicus)
for subdir in "${deb_pkgs[@]}"
do
  echo "Packaging ${subdir}..."
  pushd .
  cd "${subdir}"
  mkdir -p build
  cd build
  cmake ..
  make package
  cp "${subdir}"*.deb ../..
  popd
done

# Now Taranis itself
cmake ..
make package 

tar -cvzf taranis-with-dependencies.tar.gz asn1crypto-0.18.5.tar.gz m2m_certificates-0.1.1.tar.gz nfcpy-0.10.3.tar.gz pynfc-1.7.1.tar.gz opinicus*-Linux.deb taranis-0.9.0-Linux.deb ../install.sh

popd
