"""Taranis setup file."""

from setuptools import setup, find_packages

setup(
    name = "taranis",
    version = "0.9.0",
    description = "Library and application to write NFC tags on Ultimaker filament spools",
    author="Ultimaker",
    author_email="l.vanbeek@ultimaker.com",
    url="http://software.ultimaker.com/",
    scripts=["taranis/scripts/filament_spool_writer.py", "taranis/user_interface/views/qt/taranis_qt_gui.py", "taranis/scripts/provision_new_station.py", "taranis/scripts/fdm_whitelist_creator.py"],
    packages=find_packages(exclude=["test*"]),
    include_package_data=True,
    install_requires = ["cryptography", "tzlocal"],
)

