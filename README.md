# Taranis

NFC Programming station

Automatic installation
======================

Automatic installation comes in 2 flavor: one for Olimex embedded boards and one for more standard devices, such as a laptop with a graphical desktop environment. 

### For Olimex embedded boards
First, you need an image, which packages the full environment for the GUI and services to run in. 

#### Packaging 
An image for Taranis is created similar to an image for Jedi-based UM3 3D-printers.
You will need some repositories from the Ultimaker organisation on GitHub (https://github.com/Ultimaker): 
- jedi-build (on the ``taranis-build`` branch)
- jedi-installer (on the ``taranis`` branch)
- jedi-package-repository (on the ``taranis`` branch)
- linux-sunxi (on the ``taranis`` branch)
- nfcpy (from https://github.com/LoyVanBeek/nfcpy)
- pynfc (from https://github.com/LoyVanBeek/pynfc)
- opinicus (on the ``EM-1032`` branch which includes an installer and not all the services)

In order to create .deb packages from pynfc and nfcpy, you need stdeb (obtained via ```pip3 install stdeb```) 
With stdeb installed, run, in their respective directories:
```bash
$ cd nfcpy
$ python3 setup.py --command-packages=stdeb.command bdist_deb
$ cd ../pynfc
$ python3 setup.py --command-packages=stdeb.command bdist_deb
$ cd ..
```
and then add the resulting .deb's from the ```deb_dist``` subdirectory to jedi-package-repository. 

To create a .deb for taranis, run:
```bash
$ cd taranis
$ mkdir build
$ cd build
$ cmake ..
$ make package
```
and also add the resulting .deb to jedi-package-repository. 

With these repositories checked out and the .deb's included, run this in jedi-build:
```bash
$ ./build build image sd
```

Once finished building, put the image on an SD card:
```bash
$ sudo dd if=images/Taranis_installer.img of=/dev/sdXXX bs=1M status=progress
```
**Note**: In case you're using a new SD card, be sure to completely erase it first with 
```bash
$ sudo dd if=/dev/zero of=/dev/sdXXX bs=8M status=progress
```

#### Installing
To install the image on a Olimex target, you will need a few things:
- Power supply for the board
- The image created above, on an SD-card
- A USB stick with 2 labeled partitions: 
    - nfc_config_pub: NTFS formatted
    - nfc_config: ext4 formatted. 
    The filesystem (/etc/fstab) is configured for these partitions to be present. 
    There does not have to be any data present on the partitions, they only need to be present to bootstrap the installer.
- Network cable
- Optional: FTDI debugging cable in case of trouble. 
 
1. Plug the SD card in the slot
2. Plug the USB stick into any USB slot
3. Plug in the network cable
4. Optionally: plug the debugging cable onto the board and the FTDI-USB in your computer and run 
```bash
$screen /dev/ttyUSB0 115200
```
5. Power up the board.
6. After a little time, LED1 should start to blink slowly, at a 2 Hz frequency. This means the installer is running and copying files from the SD-card to the on-board memory
7. Optionally: monitor the installer's progress. Using the ```screen``` you started, log in and run ```journalctl -fu jedi-installer.service``` on the target.  
8. When the LED stops blinking, the installation is done. 
9. In case the LED starts blinking very fast, an error occurred. Use ```screen``` to see what the problem might be. 
10. When the installer is done, power off the board and unplug the USB-stick, the SD-card, network cable and debugging cable. 
11. Plug in the USB-stick and SD-card with the configuration specific for this instance.  

### For Graphical Desktop Environments
Run ./package.sh in the root directory. 
This will gather all the dependencies which are not available via apt-get into a single tarball. 

```bash
development-machine $ ./package.sh
```

Then, copy the resulting build/taranis-with-dependencies.tar.gz to the installation target machine.
Unzip it on the target machine and there, run the install script. 
```bash
target-machine $ tar -zxf taranis-with-dependencies.tar.gz
target-machine $ ./install.sh
```

This will unpack and install both the non-Debian and Debian dependencies.

You may want to install taranis_qt_gui.py as a startup application in the window manager. 

After rebooting, the services should start and you should be able to program tags using the GUI. 

See below for manual installation or for a development installation

Provisioning
============
In order to function, a programming station needs the following data data:

 - fdm_whitelist.csv: An encrypted fdm_whitelist
 - fdm_whitelist_signature.sig: Signature over the encrypted whitelist by Ultimaker
 - station_certificate.pem: M2M certificate to be written to each tag
 - station_id: file with a number in it, e.g. 11
 - station_tag_signing_key.pem: ECDSA private key to sign the tag content with. The corresponding public key must be included in the station_certificate
 - station_tag_verification_key.pem: ECDSA public key that is also included in the station_certificate 
 - station_whitelist_decryption_key.pem: RSA private key to decrypt rows of fdm_whitelist.csv with
 - station_whitelist_encryption_key.pem; RSA public key to encrypt rows of fdm_whitelist with. This file is not needed *on* the programming station but at Ultimaker
 - ultimaker_certificate_verification_key.pem: Public key that can prove the station_certificate is signed by Ultimaker. 
 - ultimaker_whitelist_verification_key.pem: Public key to prove that the fdm_whitelist_signature.sig is signed by Ultimaker. 
 
Some of this data is private to the station and hidden from its users, some of it is public to the station's users and some of the information stays privately at Ultimaker.
The station's public data is stored on a USB stick and the station's private data is on an SD card inside the station, not exposed to the outside.

 1. The USB stick: formatted in NTFS, with label "nfc_config_pub" 
 2. The SD card: formatted in EXT4, with label "nfc_config".

Then, run the provisioning tool:
```bash
python3 -m taranis.scripts.provision_new_station --help
```

For example, to create the files for station 999 for a manufacturer named Dummy in Dummytown, Netherlands:
```bash
python3 -m taranis.scripts.provision_new_station \
999 Dummy Dummytown Netherlands \
~/taranis-private-data/ultimaker_certificate_signing_key.pem \
~/taranis-private-data/ultimaker_whitelist_verification_key.pem
```
The last parameter indicates which private key to use to sign the manufacturer's certificate with.
When this script is done, you still need 1 more step: edit the plaintext whitelist and generate and encrypted materials whitelist from it.

See the section on [Publishing FDM whitelist](#Publishing-FDM-whitelist) for that below. 

To set the PIN code that is needed to enter the administration mode, take the PIN number and add 866929256790 to it. Save that number in pin.txt on the USB stick. 
This constant is defined in the [UiConfig](taranis/taranis/user_interface/configuration/ui_config.py). 

In the graph below, you can see how the generated files are linked:
![Encryption file links](http://g.gravizo.com/g?
    digraph G {
    "fdm_whitelist.csv";
    "fdm_whitelist_signature.sig";
    "Manually" [shape=box];
    "OpenSSL" [shape=box];
    "plaintext_fdm_whitelist.csv";
    "station_certificate.pem";
    "station_id"
    "station_tag_signing_key.pem";
    "station_tag_verification_key.pem";
    "station_whitelist_decryption_key.pem";
    "taranis.scripts.fdm_whitelist_creator" [shape=box];
    "taranis.scripts.generate_manufacturer_certificate" [shape=box];
    "taranis.scripts.generate_tag_signing_keys" [shape=box];
    "taranis.scripts.generate_whitelist_encryption_keys" [shape=box];
    "ultimaker_certificate_verification_key.pem";
    "ultimaker_whitelist_verification_key.pem";
    "Manually" -> "plaintext_fdm_whitelist.csv";
    "plaintext_fdm_whitelist.csv" -> "taranis.scripts.fdm_whitelist_creator";
    "taranis.scripts.fdm_whitelist_creator" -> "fdm_whitelist.csv";
    "taranis.scripts.fdm_whitelist_creator" -> "fdm_whitelist_signature.sig";
    "taranis.scripts.generate_manufacturer_certificate" -> "station_certificate.pem";
    "Manually" -> "station_id";
    "taranis.scripts.generate_tag_signing_keys" -> "station_tag_signing_key.pem";
    "taranis.scripts.generate_tag_signing_keys" -> "station_tag_verification_key.pem";
    "taranis.scripts.generate_whitelist_encryption_keys" -> "station_whitelist_decryption_key.pem";
    "taranis.scripts.generate_whitelist_encryption_keys" -> "station_whitelist_encryption_key.pem";
    "OpenSSL" -> "ultimaker_certificate_verification_key.pem";
    "OpenSSL" -> "ultimaker_whitelist_verification_key.pem";
    "station_tag_signing_key.pem" -> "taranis.scripts.generate_manufacturer_certificate";
    "station_tag_verification_key.pem" -> "taranis.scripts.generate_manufacturer_certificate";
  }
)

Publishing FDM whitelist (Add new materials/stations)
=====================================================
To alter the list of materials that a manufacturer is permitted to make, an fdm_whitelist must be created and signed. There is a tool to do this, based on a matrix of materials vs. station IDs.
As input, it takes a csv-file, with a column for `material_id`s and one for each station's ID. Each individual material has a row in this matrix with it's ID in the material_id column.

For example:

|material_id |1|2|3|
|------------|-|-|-|
|some_mat_id |X|-|X|
|other_mat_id|-|X|X|
|weird_mat_id|-|-|X|

To grant a permission, put an X (make it non-empty) in the row and column of the combination to grant permission for.
In the example, station 1 can only program `some_mat_id`, 2 can only program `other_mat_id` and station 3 can do all 3 materials.

The matrix used for administration is stored in taranis-private-data/station_permission_matrix.csv (which is a subset of, and can be derived from taranis-private-data/station_permission_matrix.xlsx)

To generate permissions based on this matrix:
```bash
cd taranis-private-data
python3 -m <optional path_to_taranis>taranis.scripts.matrix_to_whitelist_compiler \
--input-path=station_permission_matrix.csv \
--time-limit="2018-01-01 00:00:00" \
--output=fdm_whitelist.csv \
--signature fdm_whitelist_signature.sig \
--signing-key ultimaker_whitelist_signing_key.pem
```

As you can see, there is also a `time_limit` specified that tells when the permissions expire. 

In this example, the publishing is done by storing the output and signature to the common administration directory, but they need to be sent to the relevant programming station(s). 

These files can now be copied to the stations USB drive in the root directory (the nfc_config_pub partition) and on software.ultimaker.com in /webroot/software/taranis/ so that the auto update mechanism (as described in the user manual) can also find these files.

Cryptographic signing
=====================
The tags that Taranis writes can be cryptographically signed. It is currently **switched off**.

The NFC NDEF format provides a SignatureRecord for this, that signs the *preceding* NDEF Records in the Message.
A SignatureRecord consists of a signature (calculated from the filament manufacturer's private key and the bytes of the preceding records) and a certificate chain.
This chain starts with the manufacturer's certificate and then links to Ultimakers certificate via an URI. 
As the printers are usually offline, Ultimaker's certificate is cached on the printer.
Ultimaker's certificate contains Ultimaker's public key, which is used to verify the manufacturer certificate. 
A filament manufacturer that wants to sell unapproved filament would need cooperation from the user to spoof Ultimaker's certificate.
It is possible to spoof/replace this by the printer user, but there is no user benefit from it. 

The manufacturer's certificate is stored on the tag in the M2M format, as defined by the NFC Forum. 
The manufacturer certificate contains the manufacturer's public key, derived from the manufacturer's private key.
This private key, of the manufacturer, is stored each programming station.
A manufacturer certificate is signed by Ultimaker's private key that we keep safe.  
  
To generate a certificate for a manufacturer: 
```bash
$ python3 -m taranis.scripts.generate_manufacturer_certificate 
``` 
This script will tell then you its exact usage itself. 
For the ultimaker_private_key.pem-parameter, you may use taranis/scripts/configuration/dummy_private.pem. The certificate will not be accepted by printers of course, but its good for testing. 

![Cryptographic signing overview](signing_diagram.png)

Manual/Development Installation
===============================
This application depends on:
 
 1. libnfc
 2. Our fork of [PyNFC](https://github.com/LoyVanBeek/pynfc)
 3. Our fork of [NFCpy](https://github.com/LoyVanBeek/nfcpy)

To enable the desktop icon:
 
 1. Launch the tweak tool in Activities
 2. Go to the Desktop tab
 3. Enable "Icons on Desktop"
 
To install it all:
```bash
sudo apt-get install python3-setuptools libnfc* python3-pip git openssl build-essential libssl-dev libffi-dev python3-dev python-pyqt5

mkdir -p ~/git

cd ~/git
git clone https://github.com/LoyVanBeek/pynfc
cd pynfc
sudo python3 setup.py install

cd ~/git
git clone https://github.com/LoyVanBeek/nfcpy -b feature/python3
cd nfcpy
sudo python3 setup.py install

# Then clone this repo:
cd ~/git
git clone https://github.com/Ultimaker/taranis
cd taranis
sudo python3 setup.py install

cp ~/git/taranis/FilamentSpoolWriter.desktop ~/Desktop/FilamentSpoolWriter.desktop
sed -i s/lvanbeek/$USER/g ~/Desktop/FilamentSpoolWriter.desktop # Replace the username
cp ~/git/taranis/TagEraser.desktop ~/Desktop/TagEraser.desktop
sed -i s/lvanbeek/$USER/g ~/Desktop/TagEraser.desktop # Replace the username
cp ~/git/taranis/Taranis.desktop ~/Desktop/Taranis.desktop
sed -i s/lvanbeek/$USER/g ~/Desktop/Taranis.desktop # Replace the username

# Set up links to materials
ln -s ~/git/taranis/data/ ~/Desktop/data

# Configure libnfc
sudo cp ~/git/taranis/system_config/nfc/libnfc.conf /etc/nfc/libnfc.conf # Use this sample config
sudo usermod -a -G dialout $USER # Add the current user to the group that can access the USB device
#If needed, you may try to override the connection string of libnfc: $ export LIBNFC\_DEFAULT\_DEVICE="pn532\_uart:/dev/ttyUSB0"

# Link the .json files to the writer software
sudo cp ~/Desktop/FilamentSpoolWriter.desktop /usr/share/applications/
sudo sh -c 'echo "application/json=FilamentSpoolWriter.desktop" >> /usr/share/applications/defaults.list'

# **Now, finally log out and in again**, then the software should be usable from the desktop"
```

To work with the USB sticks for configuration, symlink /media/<username> to /media/ultimaker and allow the user ultimaker to access it:
```bash
cd /media
sudo chmod o+x <username>
sudo ln -s <username> ultimaker
```

Name
====
[Taranis](https://en.wikipedia.org/wiki/Taranis) is the Celtic god of thunder. Thunder is transmitted wirelessly, as is NFC

Attribution
===========
The negative beep sound "Beep1: Resonant Error Beep" by tcpp is licensed under CC BY 3.0, obtained from https://freesound.org/people/tcpp/sounds/151309/ 
The positive beep sound "tone beep lower slower" by pan14 is licenced under CC0 1.0 Universal, obtained from https://freesound.org/people/pan14/sounds/263126/ 

Developer mode
==============
To enable/disable developer mode: go to the Administation screen and press the "Update system" button for 5 seconds.
The system will reboot with the new mode active. It is off by default
The non-loopback IP address(es) of the station will appear in the SystemUpdateScreen
