import unittest

from nfc.ndef import Record, Message

from ..common.data import Material, MaterialStatistics
from ..common.data.unit import Unit
from ..common.serialization import MaterialSerializerV0, MaterialStatisticsSerializerV0

import datetime
import uuid

## Iest whether encoding our data classes into bytes, into NDEF Records, NDEF Message and back up works
class TestRoundTripIntegration(unittest.TestCase):
    def setUp(self):
        then = datetime.datetime(year=2016, month=6, day=15, hour=15, minute=15, second=15)

        self.material_serializer = MaterialSerializerV0()
        self.material = Material(manufacturing_timestamp=then,
                                 material_id=uuid.UUID(int=1234567890),
                                 programming_station_id=0,
                                 batch_code="testbatch")

        self.stats_serializer = MaterialStatisticsSerializerV0()
        self.stats = MaterialStatistics(total_usage_duration=datetime.timedelta(seconds=0),
                                        total=750,
                                        remaining=750,
                                        unit=Unit.milligram)


    def test1(self):
        material_payload_in = self.material_serializer.serialize(self.material)
        material_record_in = Record(record_type="urn:nfc:ext:ultimaker.nl:material", record_name=0x01,
                                 data=material_payload_in)

        signature_record_in = Record(record_type="urn:nfc:wkt:Sig", record_name="", data=bytes([]))

        stats_payload_in = self.stats_serializer.serialize(self.stats)
        stats_record_in = Record(record_type="urn:nfc:ext:ultimaker.nl:stat", record_name=0x02, data=stats_payload_in)

        message_in = Message(material_record_in, signature_record_in, stats_record_in)

        tag_user_memory = message_in.to_bytes()

        message_out = Message(tag_user_memory)

        self.assertEquals(len(message_out), 3)

        material_record_out = message_out[0]
        signature_record_out = message_out[1]
        stats_record_out = message_out[2]

        self.assertEqual(material_record_in, material_record_out)
        self.assertEqual(signature_record_in, signature_record_out)
        self.assertEqual(stats_record_in, stats_record_out)

        material_payload_out = material_record_out.data
        stats_payload_out = stats_record_out.data

        self.assertEqual(len(material_payload_in), len(material_payload_out))
        self.assertEqual(len(stats_payload_in), len(stats_payload_out))

        material_out, serial = self.material_serializer.deserialize(material_payload_out)
        stats_out = self.stats_serializer.deserialize(stats_payload_out)

        self.assertEqual(self.material, material_out)
        self.assertEqual(self.stats, stats_out)
