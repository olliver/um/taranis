#! /usr/bin/env python3

import logging

from griffin import dbusif

from taranis.scripts import colors

log = logging.getLogger(__name__.split(".")[-1])

if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    logging.basicConfig(filename=".eraserFrontend.log", level=logging.DEBUG,
                        format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    filamentService = dbusif.RemoteObject("filament_spool_interfacing")

    def handleSpoolDetected(spool_id, has_content):
        print("Erasing...")
        try:
            filamentService.eraseSpool(spool_id)
            print(colors.green("Tag {uid} cleared".format(uid=spool_id)))
        except:
            pass
        print("Waiting for 1 tag...")


    filamentService.connectSignal("spoolDetected", handleSpoolDetected)
    print("Waiting for 1 tag...")

    try:
        dbusif.runMainLoop()
    except KeyboardInterrupt:
        print("Exiting")