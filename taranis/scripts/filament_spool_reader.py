#! /usr/bin/env python3
import os
import binascii
import logging

from taranis.errors import showError
from taranis.scripts import colors
from taranis.scripts.tag_application import TagApplication

from nfc.ndef import Message, SignatureRecord, SignatureType, FormatError, error
from pynfc.ntag_read import NTagInfo, NTagReadWrite, UnknownTagTypeException
from taranis.common.configuration.tag_configuration import TagConfiguration
from taranis.common.serialization import MaterialSerializerV0
from taranis.common.serialization import MaterialStatisticsSerializerV0
from taranis.scripts.configuration.signatures import SignatureConfiguration

if SignatureConfiguration.signature_enabled:
    import m2m_certificates as m2m

log = logging.getLogger("filamentSpoolReader")

EXIT_ON_EXCEPTION = False

## @brief Read a filament spool
class FilamentSpoolReader(TagApplication):
    def __init__(self, ntag_interface, material_serializer, stats_serializer):
        super(FilamentSpoolReader, self).__init__(ntag_interface)

        self.material_serializer = material_serializer
        self.stats_serializer = stats_serializer

        # Some configuration
        self.uid_mirror_page, self.uid_mirror_byte = divmod(TagConfiguration.UID_MIRROR_POSITION,
                                                            NTagInfo.BYTES_PER_PAGE)

    ##  Read the content of a tag and parse its data into Material, the tag serial number and MaterialStatistics
    def readTagContent(self, tag_type, debug=False):
        uid = self.tag_interface.setup_target()
        log.info("Reading uid = {id}".format(id=binascii.hexlify(uid)))
        self.tag_interface.set_easy_framing()

        content = self.tag_interface.fast_read_user_memory(tag_type)

        uid_mirroring_ok = self.tag_interface.check_uid_mirror(tag_type) == (self.uid_mirror_page, self.uid_mirror_byte)

        try:
            message = Message(content)

            material_rec = \
                [record for record in message if record.type == self.material_serializer.ULTIMAKER_NL_MATERIAL][0]
            signature_rec = [record for record in message if record.type == SignatureRecord.RECORD_TYPE][0]
            stats_records = [record for record in message if record.type == self.stats_serializer.ULTIMAKER_NL_STAT]

            material, serial = self.material_serializer.deserialize(material_rec.data)
            signature = SignatureRecord(signature_rec)
            decoded_stats = [self.stats_serializer.deserialize(stats_rec.data) for stats_rec in stats_records]
            stats = max(decoded_stats,
                        key=lambda stat: stat.total_usage_duration)  # Take the stats that are the most recent

            certificate_ok, signature_match = self.checkSigning(debug, material_rec, signature, uid)

            signing_ok = uid_mirroring_ok and signature_match and certificate_ok

            return material, serial, stats, signing_ok
        except FormatError as ndef_format_error:
            # import ipdb; ipdb.set_trace()
            log.error("Could not decode content of tag {uid}: {content}. Error: {err}".format(content=content,
                                                                                              uid=binascii.hexlify(uid),
                                                                                              err=ndef_format_error))
            raise ndef_format_error
        except error.LengthError as length_error:
            log.debug("Tag user memory contains data: {content}".format(content=content))
            raise length_error

    def checkSigning(self, debug, material_rec, signature, uid):
        signature_match = False
        certificate_ok = False
        if signature.signature_type == SignatureType.NoSignaturePresent:
            log.warning("The signature on tag {uid} is empty and can thus not be verified".format(
                uid=binascii.hexlify(uid)))
            certificate_ok, signature_match = False, False
        else:
            # 1. Verify that the signature matches the content &
            #       that the signature was created using the M2M cert privkey
            #       (by using the M2M cert pubkey) of the manufacturer
            #   This is done with the m2m.verify_signature-function.
            # 2. Verify that the manufacturer"s M2M certificate was issued and signed by Ultimaker
            #       To do this:
            #           1. Get the signature and the ToBeSigned-bytes of the certificate.
            #           2. Check that the pair (signature, TobeSigned-bytes, ultimaker-pubkey) matches.
            # 3. Verify Ultimaker"s certificate, e.g. using $ openssl verify -CAfile root.pem -untrusted ultimaker.pem
            signed_material_bytes = material_rec.to_bytes()

            if debug:
                print("Signed_material_bytes ({len}): {b}".format(b=binascii.hexlify(signed_material_bytes),
                                                                  len=len(signed_material_bytes)))
                print("Signature on read ({len}): {b}".format(b=binascii.hexlify(signature.signature),
                                                              len=len(signature.signature)))

            certificate_ok, signature_match = self.checkCertificateChain(signature, signature_match,
                                                                         signed_material_bytes, uid)
        return certificate_ok, signature_match

    def checkCertificateChain(self, signature, signature_match, signed_material_bytes, uid):
        certificate_chain_ok = False
        if signature.certificate_chain:
            manufacturer_certificate_bytes = signature.certificate_chain[0]
            manufacturer_certificate = m2m.m2m_certificate_from_bytes(manufacturer_certificate_bytes)

            pubkey_bytes = manufacturer_certificate.native["tbsCertificate"]["pubKey"]
            signature_match = m2m.verify_signature(signed_material_bytes, signature.signature,
                                                   public_key_bytes=pubkey_bytes)

            if not signature_match:
                log.error("The signature on tag {uid} does not match".format(uid=binascii.hexlify(uid)))
            else:
                log.debug("The signature on tag {uid} matches".format(uid=binascii.hexlify(uid)))
                certificate_chain_ok = self.checkManufacturerCertificate(manufacturer_certificate)

                if not certificate_chain_ok:
                    # print("Certificate is not signed by Ultimaker")
                    log.error("The certificate on tag {uid} is not signed by Ultimaker".format(
                        uid=binascii.hexlify(uid)))
        else:
            log.error("There is no certificate on tag {uid}".format(uid=binascii.hexlify(uid)))
            certificate_chain_ok = False
        return certificate_chain_ok, signature_match

    def checkManufacturerCertificate(self, manufacturer_certificate):
        # When the signature matches, it makes sense to also check whether the certificate used to sign the certificate is OK
        # It being OK means that the manufacturer certificate, that is included in the tag is M2M format, is signed by Ultimaker.
        # We can do this by checking the signature under the certificate with the content of the certificate with out public key.
        # The M2M module offers a verifier for that, which takes Ultimakers public key (file) as a parameter.
        certificate_verifier = m2m.CertificateVerifier(SignatureConfiguration.ultimaker_verification_key_path)
        certificate_ok = certificate_verifier.verify(manufacturer_certificate)
        return certificate_ok


if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    logging.basicConfig(filename=".filament_spool_reader.log", level=logging.DEBUG,
                        format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    material_serializer = MaterialSerializerV0()
    stats_serializer = MaterialStatisticsSerializerV0()
    ntag_interface = NTagReadWrite(logging.getLogger("ntagReadWrite"))

    reader = FilamentSpoolReader(ntag_interface, material_serializer, stats_serializer)

    while True:
        try:
            print("Waiting for 1 tag...")
            reader.waitForOneTag()
            os.system("clear")  # Clear the screen

            try:
                current_tag_type, current_uid = reader.determineTagType()
            except OSError:
                print(colors.red("ERROR: 007: Could not determine tag type, is there a tag present?"))
                continue
            except UnknownTagTypeException as unknown_tag_type_error:
                print(colors.red("ERROR: 008: Tag type unknown. Capability byte is {cap_byte}".format(
                    cap_byte=unknown_tag_type_error.capability_byte)))
                continue

            try:
                print("Reading...")
                read_material, serial_number, read_stats, crypto_ok = reader.readTagContent(tag_type=current_tag_type)
                if crypto_ok:
                    print(colors.green("Cryptographic signing is OK"))
                else:
                    showError(13, log)

                write_color = colors.yellow

                print(write_color("The serial number of this tag is {serial}".format(serial=serial_number)))
                print(write_color(
                    "This tag contains: \n{mat}".format(mat=read_material).replace(",", ",\n\t").replace("(",
                                                                                                         "(\n\t ").replace(
                        ")", "\n)")))
                print(write_color(
                    "{stats}".format(stats=read_stats).replace(",", ",\n\t").replace("(", "(\n\t ").replace(")",
                                                                                                            "\n)")))
            except (IndexError, ValueError) as read_error:
                showError(10, log, detail=read_error)
                if EXIT_ON_EXCEPTION:
                    exit(-10)
            except error.LengthError as length_error:
                showError(11, log, detail=length_error)
                if EXIT_ON_EXCEPTION:
                    exit(-11)
            except OSError as os_error:
                showError(2, log, detail=os_error)
                if EXIT_ON_EXCEPTION:
                    exit(-2)
            except FormatError as ndef_format_error:
                showError(10, log, detail=ndef_format_error)
                if EXIT_ON_EXCEPTION:
                    exit(-10)
        except KeyboardInterrupt:
            print("Exit filament spool writer")
            exit(0)

