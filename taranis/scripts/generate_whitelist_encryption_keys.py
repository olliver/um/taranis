#! /usr/bin/env python3

import argparse
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

def generateWhitelistEncryptionKeys(private_decryption, public_encryption):
    decryption_key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
    encryption_key = decryption_key.public_key()


    decryption_pem = decryption_key.private_bytes(
                                                    encoding=serialization.Encoding.PEM,
                                                    format=serialization.PrivateFormat.TraditionalOpenSSL,
                                                    encryption_algorithm=serialization.NoEncryption())

    encryption_pem = encryption_key.public_bytes(encoding=serialization.Encoding.PEM,
                                       format=serialization.PublicFormat.SubjectPublicKeyInfo)

    with open(public_encryption, "wb") as encryption_pem_file, \
        open(private_decryption, "wb") as decryption_pem_file:
        decryption_pem_file.write(decryption_pem)
        encryption_pem_file.write(encryption_pem)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Generate FDM whitelist encryption end decryption keys for a programming station.
Rows in the whitelist are encrypted with the public key, only to be decrypted by the stations's private key""")

    parser.add_argument("--encryption",
                        default="station_whitelist_encryption_key.pem",
                        required=False,
                        type=str,
                        help="""Destination for encryption key""")

    parser.add_argument("--decryption",
                        default="station_whitelist_decryption_key.pem",
                        required=False,
                        type=str,
                        help="Destination for decryption key")

    args = parser.parse_args()

    try:
        generateWhitelistEncryptionKeys(args.decryption, args.encryption)
        print("Keys are saved to {enc} and {dec}".format(enc=args.encryption, dec=args.decryption))
        exit()
    except Exception as ex:
        print("There was an issue creating or storing the keys at {enc} and {dec}: {ex}".format(enc=args.encryption, dec=args.decryption, ex=ex))
        exit(-1)