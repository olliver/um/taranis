#! /usr/bin/env python3
import logging
import time
import binascii

from taranis.scripts import colors
from taranis.scripts.tag_application import TagApplication

from pynfc.ntag_read import NTagReadWrite

log = logging.getLogger("tagEraser")


## @brief Write and read a filament spool with the given data
class TagClearing(TagApplication):
    def __init__(self, tag_interface):
        super(TagClearing, self).__init__(tag_interface)

    ##  Write the given bytes to the tag. The maximal size of the content depends on the tag type
    def eraseTag(self, tag_type):
        uid = self.tag_interface.setup_target()
        log.info("Erasing uid = {id}".format(id=binascii.hexlify(uid)))

        self.tag_interface.set_easy_framing()
        length = tag_type["user_memory_end"] - tag_type["user_memory_start"] + 1
        content = bytes(length) # There all default to 0

        self.tag_interface.write_user_memory(content, tag_type)

        return uid

    def configureTag(self, tag_type):
        self.tag_interface.enable_uid_mirror(tag_type, 0, 0)
        self.tag_interface.set_password(tag_type)

    def reopen(self):
        self.tag_interface.close()
        self.tag_interface.open()

def inputToBytes(input_str):
    """Convert a string of the format 01,02,03,04 to b"\x01\x02\x03\x04" etc """
    num_strs = input_str.split(",")
    return bytes([int(num_str) for num_str in num_strs])

if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    logging.basicConfig(filename=".eraseTag.log", level=logging.INFO, format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    ntag_interface = NTagReadWrite(logging.getLogger("ntagReadWrite"))
    eraser = TagClearing(ntag_interface)

    while True:
        try:
            print("Waiting for 1 tag...")
            eraser.waitForOneTag()

            tag_type, _ = eraser.determineTagType()

            try:
                uid = eraser.eraseTag(tag_type)
                print(colors.green("Tag {uid} cleared".format(uid=binascii.hexlify(uid))))
            except OSError as e:
                print(colors.yellow("Could not erase tag on first attempt"))

                eraser.reopen()

                try:
                    eraser.authenticate()
                    uid = eraser.eraseTag(tag_type)
                    print(colors.green("Tag {uid} cleared".format(uid=binascii.hexlify(uid))))
                except OSError as e:
                    print(colors.yellow("Could not erase tag on second attempt, after authenticating with default password and ack"))

                    eraser.reopen()

                    # import ipdb; ipdb.set_trace()
                    pwd_str = input("Password byte string (or <enter> to cancel and go to next tag): ")
                    if not pwd_str:
                        wait = 1.0
                        print(colors.yellow("Remove tag, will continue with next tag in {wait} seconds".format(wait=wait)))
                        time.sleep(wait)
                        continue

                    password = inputToBytes(pwd_str)
                    acknowledge = inputToBytes(input("Acknowledge byte string: "))

                    try:
                        eraser.authenticate(password, acknowledge)
                        uid = eraser.eraseTag(tag_type)
                        print(colors.green("Tag {uid} cleared".format(uid=binascii.hexlify(uid))))
                    except OSError as e:
                        print(colors.red("Could not erase tag on third attempt, after authenticating with given password and ack"))
        except KeyboardInterrupt:
            break





