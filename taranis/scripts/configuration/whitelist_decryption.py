"""Configuration for Fdm whitelist encryption and decryption keys.

Both keys, for encryption and signing are generated using
$ openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048
$ openssl rsa -pubout -in private_key.pem -out public_key.pem
"""

import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key
from taranis.scripts.configuration.crypto import _loadPemBytes

public_configuration_path = os.path.join("/media/", "ultimaker", "nfc_config_pub/")
private_configuration_path = os.path.join("/media/", "ultimaker", "nfc_config/")
here = os.path.abspath(os.path.dirname(__file__))


class WhitelistDecryptionConfiguration(object):
    encrypted_path = os.path.join(public_configuration_path, "fdm_whitelist.csv")
    signature_path = os.path.join(public_configuration_path, "fdm_whitelist_signature.sig")

    # The key used to decrypt a whitelist element, private to a manufacturer
    station_private_key_path = os.path.join(private_configuration_path, "station_whitelist_decryption_key.pem")

    @staticmethod
    def getWhitelistDecryptionKey():
        return load_pem_private_key(_loadPemBytes(WhitelistDecryptionConfiguration.station_private_key_path),
                                    password=None,
                                    backend=default_backend())

    # The key used to verify that Ultimaker signed the whitelist file
    whitelist_verification_key_path = os.path.join(private_configuration_path, "ultimaker_whitelist_verification_key.pem")

    @staticmethod
    def getWhitelistVerificationKey():
        return load_pem_public_key(_loadPemBytes(WhitelistDecryptionConfiguration.whitelist_verification_key_path),
                                   backend=default_backend())


