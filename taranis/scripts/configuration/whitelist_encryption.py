import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from taranis.scripts.configuration.crypto import _loadPemBytes, loadPrivateKey


configuration_path = os.path.join("/media/",os.getenv("USER"), "nfc_config_pub/")
here = os.path.abspath(os.path.dirname(__file__))


class WhitelistEncryptionConfiguration(object):
    encrypted_path = os.path.join(configuration_path, "fdm_whitelist.csv")
    signature_path = os.path.join(configuration_path, "fdm_whitelist_signature.sig")

    # Load the key used to encrypt a whitelist element from the given path.
    # This key must be derived from WhitelistDecryptionConfiguration.station_private_key
    @staticmethod
    def loadStationPublicKey(path):
        station_public_key = load_pem_public_key(_loadPemBytes(path),
                                                      backend=default_backend())
        return station_public_key

    @staticmethod
    def deriveStationPublicKey(station_private_key):
        station = station_private_key.public_key()
        return station

    whitelist_encryption_key_path = os.path.join(configuration_path, "station_whitelist_encryption_key.pem")

    # The private key used to sign the authenticity of the encrypted whitelist

    whitelist_signing_key_path = os.path.join(here, "ultimaker_whitelist_signing_key.pem")
    whitelist_signing_key = loadPrivateKey(whitelist_signing_key_path)