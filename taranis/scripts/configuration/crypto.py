from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key


def _loadPemBytes(path):
    with open(path, "rb") as bytes_file:
        content = bytes_file.read()
        return content


def loadPrivateKey(path):
    return load_pem_private_key(_loadPemBytes(path),
                                password=None,
                                backend=default_backend())