import os

configuration_path = os.path.join("/media/",os.getenv("USER"), "nfc_config/")   # os.path.abspath(os.path.dirname(__file__))

class Station(object):
    _station_id_file = os.path.join(configuration_path, "station_id")

    station_id = int(open(_station_id_file).read().strip())