#! /usr/bin/env python3
"""This script takes a csv file with rows for every material.
The first column is the material_id and each other column is a station ID.
An "X" in a station's column on a material's row indicates that that material is permitted for programming on that station"""

import os
import argparse
import csv
import datetime
import uuid
import dateutil.parser

from taranis.scripts.configuration.whitelist_encryption import WhitelistEncryptionConfiguration
from .fdm_whitelist_creator import serializeFdmWhitelist
from taranis.common.data.fdm_whitelist import MaterialPermission, FdmWhitelist

BASE_PATH = "~/taranis-private-data/{station_id}/ultimaker_private/station_whitelist_encryption_key.pem"

def generatePermissionsFromMatrix(matrix_file_path, time_limit):
    with open(matrix_file_path, "r") as matrix_file:
        matrix = csv.DictReader(matrix_file)

        station_ids = matrix.fieldnames[1:]  # Skip the first column, which is the material_ID and not a station_id

        for row in matrix:
            material_id = row["material_id"]

            for station_id in station_ids:
                if row[station_id] != "":
                    pubkey_path = BASE_PATH.format(station_id=station_id)

                    try:
                        station_pubkey = WhitelistEncryptionConfiguration.loadStationPublicKey(os.path.expanduser(pubkey_path))
                    except ValueError as ve:
                        print("Does the file at {pk_path} exist? Is it a key of the correct type? {err}".format(
                            pk_path=row["pubkey_path"], err=ve))
                        exit(-1)
                        return

                    yield MaterialPermission(uuid.UUID(material_id), time_limit, station_pubkey)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Compile a plaintext FDM whitelist based on a sparse matrix of material_ids and stations""",
                                     epilog="""Pass as input a file with a column for every material_id and further only columns with station_id's
                                     Every non-empty will be used to give permission for that combination (of material_id and station_id) is valid.
                                     This permission is valid until the date specified with the parameter "expire" """,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("--input-path",
                        default="station_permission_matrix.csv",
                        type=str,
                        help="""CSV file with at least 2 columns:
                            "material_id" (material UUID),
                            "N", which is a station ID. There can be as much station ID's as needed""")

    parser.add_argument("--time-limit",
                        default=None,
                        type=str,
                        help="""To which date are the permissions valid? Time limit in Python strptime format %%Y-%%m-%%d %%H:%%M:%%S, e.g. "2016-08-31 08:49:30" """)

    parser.add_argument("--output",
                        dest="encrypted_output_path",
                        default=WhitelistEncryptionConfiguration.encrypted_path,
                        type=str,
                        help="Path to encrypted whitelist destination")

    parser.add_argument("--signature",
                        dest="signature_output_path",
                        default=WhitelistEncryptionConfiguration.signature_path,
                        type=str,
                        help="Path to signature destination file")

    parser.add_argument("--signing-key",
                        dest="signing_key_path",
                        default=WhitelistEncryptionConfiguration.whitelist_signing_key_path,
                        required=False,
                        type=str,
                        help="Path to Ultimaker signing key. See help of this utility for generation of these keys.")

    args = parser.parse_args()

    args.input_path = os.path.expanduser(args.input_path)
    args.encrypted_output_path = os.path.expanduser(args.encrypted_output_path)

    if not os.path.exists(args.input_path):
        print("No such file or directory: {p}".format(p=args.input_path))
        exit(-1)

    if args.time_limit:
        time_limit = dateutil.parser.parser().parse(args.time_limit)
    else:
        now = datetime.datetime.now()
        time_limit = now.replace(year=now.year+1)


    permissions = list(generatePermissionsFromMatrix(args.input_path, time_limit))
    whitelist = FdmWhitelist(permissions)
    print("Granting {l} permissions in total".format(l=len(permissions)))

    serializeFdmWhitelist(whitelist=whitelist,
                          output_path=args.encrypted_output_path,
                          signature_path=args.signature_output_path,
                          signing_key_path=args.signing_key_path)