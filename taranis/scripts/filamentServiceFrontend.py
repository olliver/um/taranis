#! /usr/bin/env python3
import os
import json
import logging
from dbus.exceptions import DBusException

import time
from griffin import dbusif
from taranis.common.data.unit import Unit
from taranis import errors

from taranis.scripts import colors
from taranis.scripts import beep

log = logging.getLogger(__name__.split(".")[-1])

def printUsage():
    print("""Filament spool writer

filament_spool_writer.py info.json

Global options:
    --help: display this help message"
    --dry-run: do not actually write the tag"
    --dump: dump the content to dump.hex
    --overwrite: if there already is any content on the tag, overwrite with new content
    --exit-on-exception: Exit the application after an exception. Combined with LIBNFC_LOG_LEVEL=3, this helps debugging""")


def describeMaterial(material_to_program):
    try:
        materialService = dbusif.RemoteObject(name="material")
        brand, name, color = materialService.getMaterialNameFields(material_to_program)
        material_description = "{brand} {color} {name}".format(brand=brand, name=name, color=color)
        if material_description == "? ? ?":
            material_description = material_to_program
    except DBusException as e:
        log.exception(e)
        showError(16, log, detail=e)
        material_description = material_to_program

    return material_description

def describeAmount(number, unit):
    if unit == Unit.tostring(Unit.milligram):
        amount_description = str(int(number) / 1000) + " gram"
    else:
        amount_description = str(int(number)) + unit
    return amount_description

def showError(*args, **kwargs):  # Decorate showError with a beep
    errors.showError(*args, **kwargs)
    beep.negativeBeep()


if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    logging.basicConfig(filename=".filamentServiceFrontend.log", level=logging.DEBUG,
                        format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    filamentService = dbusif.RemoteObject("filament_spool_interfacing")

    import sys
    if "--help" in sys.argv or "-h" in sys.argv:
        printUsage()
        exit(0)
    do_dump = "--dump" in sys.argv
    overwrite = "--overwrite" in sys.argv
    EXIT_ON_EXCEPTION = "--exit-on-exception" in sys.argv


    try:
        data_file_path = sys.argv[1]
        if not data_file_path:
            showError(5, log)
            exit(-5)
        data_to_program = json.load(open(data_file_path))
        material_to_program = data_to_program["material_uuid"]
    except IndexError:
        showError(6, log)
        exit(-6)

    amount_description = describeAmount(data_to_program["total"], data_to_program["unit"])

    material_description = describeMaterial(material_to_program)

    try:
        whitelist = dbusif.RemoteObject("material_permission")

        if not whitelist.isPermitted(material_to_program):
            showError(14, log, detail=" Material_id: {mat_id} {desc}".format(mat_id=material_to_program, desc=material_description))
            exit(-14)
    except DBusException as e:
        log.exception(e)
        showError(16, log, detail=e)
        exit(-16)


    def readSpool(spool_id):
        os.system("clear")  # Clear the screen
        print("Reading...")

        material_id, total, remaining, unit_, batch_code, crypto_ok, error_code = filamentService.getSpoolSummary(spool_id)

        print("Reading finished")
        if error_code != 0:
            showError(error_code, log)

        color = colors.yellow if crypto_ok else colors.red
        print(color(
            "Spool contains:\n - total: {amount}\n - remaining: {remaining}\n - material: {desc}\n - batch: {batch}".format(
                amount=describeAmount(total, unit_),
                remaining=describeAmount(remaining, unit_),
                desc=describeMaterial(material_id),
                batch=batch_code)))

    def programSpool(spool_id):
        os.system("clear")  # Clear the screen
        print("Writing...")
        success, error = filamentService.initializeSpool(spool_id, material_to_program,
                                                         int(data_to_program["total"]), data_to_program["unit"],
                                                         data_to_program["batch"])

        if success:
            print(colors.green("Writing succeeded:\n - total: {amount}\n - remaining: {amount}\n - material: {desc}\n - batch: {batch}".format(
                amount=amount_description,
                desc=material_description,
                batch=data_to_program["batch"])))
            beep.positiveBeep()
        else:
            showError(error, log)

    def handleSpoolDetected(spool_id, has_content):
        if not has_content:
            programSpool(spool_id)
        else:
            readSpool(spool_id)
        print("Waiting for 1 tag...")

    filamentService.connectSignal("spoolDetected", handleSpoolDetected)
    print("Waiting for 1 tag...")

    try:
        dbusif.runMainLoop()
    except KeyboardInterrupt:
        print("Exiting")