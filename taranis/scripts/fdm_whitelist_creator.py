#! /usr/bin/env python3

"""
- its UUID,
- permission time limit
- path to the manufacturer"s public key

It will optionally also be given a path to Ultimaker"s private signing key
"""
from taranis.common.data.fdm_whitelist import MaterialPermission, FdmWhitelist
from taranis.scripts.configuration.crypto import loadPrivateKey
from taranis.scripts.configuration.whitelist_encryption import WhitelistEncryptionConfiguration

import argparse
import datetime
import uuid
import csv
from os import path

DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

## @brief generate an Permissions based on a csv file with permitted materials
# @param input_path path to a plain text FDM whitelist as documented in FdmWhitelist
def generatePermissionsFromFile(input_path):
    with open(input_path) as csv_file:
        csv_reader = csv.DictReader(csv_file)

        assert "material_id" in csv_reader.fieldnames
        assert "time_limit" in csv_reader.fieldnames
        assert "pubkey_path" in csv_reader.fieldnames

        for row in csv_reader:
            try:
                station_pubkey = WhitelistEncryptionConfiguration.loadStationPublicKey(path.expanduser(row["pubkey_path"]))
            except ValueError as ve:
                print("Does the file at {pk_path} exist? Is it a key of the correct type? {err}".format(pk_path=row["pubkey_path"], err=ve))
                exit(-1)
                return

            permission = MaterialPermission(uuid.UUID(row["material_id"]),
                                            datetime.datetime.strptime(row["time_limit"], DATE_FORMAT),
                                            station_pubkey)
            yield permission

## @brief serialize an FDM whitelist of permitted materials
# @param whitelist an FdmWhitelist
# @param output_path path to where the encrypted whitelist should be stored
# @param signature_path path to where the signature over the encrypted file should be stored
# @param signing_key_path path to the private signing key to be used to generate the signature
def serializeFdmWhitelist(whitelist, output_path, signature_path, signing_key_path):
    with open(output_path, "wb") as encrypted_output_file, \
            open(signature_path, "wb") as signature_output_file:
        ultimaker_private_key = loadPrivateKey(signing_key_path)

        whitelist.encryptAndSign(ultimaker_private_key,
                                 encrypted_output_file,
                                 signature_output_file)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="""Create material whitelist for NFC programming stations""",
                                     epilog="""Generate whitelist en/decryption keys with:
$ openssl genpkey -algorithm RSA -out station_whitelist_decryption_key.pem -pkeyopt rsa_keygen_bits:2048
$ openssl rsa -pubout -in station_whitelist_decryption_key.pem -out station_whitelist_encryption_key.pem
Signing keys are created the same way""",
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("input_path",
                        default="whitelist.csv",
                        type=str,
                        help="""CSV file with 3 columns:
                        "material_id" (material UUID),
                        "time_limit" (Permission time limit in Python strptime format %%Y-%%m-%%d %%H:%%M:%%S, e.g. "2016-08-31 08:49:30),
                        "pubkey_path" (Path to the station"s public key)""")

    parser.add_argument("--output",
                        dest="encrypted_output_path",
                        default=WhitelistEncryptionConfiguration.encrypted_path,
                        type=str,
                        help="Path to encrypted whitelist destination")

    parser.add_argument("--signature",
                        dest="signature_output_path",
                        default=WhitelistEncryptionConfiguration.signature_path,
                        type=str,
                        help="Path to signature destination file")

    parser.add_argument("--signing-key",
                        dest="signing_key_path",
                        default=WhitelistEncryptionConfiguration.whitelist_signing_key_path,
                        required=False,
                        type=str,
                        help="Path to Ultimaker signing key. See help of this utility for generation of these keys.")

    args = parser.parse_args()

    permissions = list(generatePermissionsFromFile(input_path=args.input_path))
    whitelist = FdmWhitelist(permissions)

    print("Granting {l} permissions in total".format(l=len(permissions)))

    serializeFdmWhitelist(whitelist, args.encrypted_output_path, args.signature_output_path, args.signing_key_path)