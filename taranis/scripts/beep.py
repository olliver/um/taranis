import os


def beep(freq=4000, duration=0.1):
    os.system("( speaker-test -t sine -f {freq} )>> /dev/null & pid=$! ; sleep {duration}s ; kill -9 $pid".format(freq=freq, duration=duration))


def positiveBeep():
    beep(freq=2000)


def negativeBeep():
    beep(freq=500, duration=0.2)
