#! /usr/bin/env python3

import base64
import hashlib

import m2m_certificates as m2m
from taranis.scripts.configuration.signatures import SignatureConfiguration


def generateStationCertificate(manufacturer_name, manufacturer_locality, manufacturer_country, station_verification_key_path, ultimaker_certificate_signing_key_path, certificate_output_path):
    """
    Generate a certificate for the manufacturer.
    The manufacturer is the certificate, Ultimaker is the issuer.

    :param manufacturer_name: Name of the manufacturer, e.g. Ultimaker
    :param manufacturer_locality: What locality is the manufacturer located in. For Ultimaker, this would be Geldermalsen
    :param manufacturer_country: Country the manufacturer is located in, e.g. NL
    :param ultimaker_certificate_signing_key_path:
    :param certificate_output_path:
    :return:
    """
    subject = m2m.Name()
    subject[0] = m2m.AttributeValue(name="country", value=m2m.PrintableString(value=manufacturer_country))
    subject[1] = m2m.AttributeValue(name="organization", value=m2m.UTF8String(value=manufacturer_name))
    subject[2] = m2m.AttributeValue(name="locality", value=m2m.UTF8String(value=manufacturer_locality))

    manufacturer_public_key_bytes = m2m.contentbytes_from_pem_file(station_verification_key_path)

    builder = m2m.CertificateBuilder(subject=subject, subject_public_key=manufacturer_public_key_bytes)

    issuer = m2m.Name()
    issuer[0] = m2m.AttributeValue(name="country", value=m2m.PrintableString(value="Netherlands"))
    issuer[1] = m2m.AttributeValue(name="organization", value=m2m.UTF8String(value="Ultimaker B.V."))
    issuer[2] = m2m.AttributeValue(name="locality", value=m2m.UTF8String(value="Geldermalsen"))

    builder.issuer = issuer

    builder.version = 0
    # Do not specify builder.serial_number, let the builder generate one for us

    # Elliptic Curve Parameters (e.g.  which curve)
    # We choose prime256v1(7) [other identifier: secp256r1]
    # http://oid-info.com/get/1.2.840.10045.3.1.7
    builder.ca_algorithm_parameters = m2m.ObjectIdentifier(value="1.2.840.10045.3.1.7").dump()  # EC PARAMETERS
    builder.ca_algorithm = "1.2.840.10045.4.3.2"  # ECDSA with SHA256, see http://oid-info.com/get/1.2.840.10045.4.3.2

    builder.pk_algorithm = "1.2.840.10045.4.3.2"  # Same as cAAlgorithm. Specifying its parameters is optional and thus omitted

    # for the subject_key_id, see https://www.rfc-editor.org/rfc/rfc5280.txt section 4.2.1.2.:
    # " For end entity certificates, the subject key identifier extension
    #   provides a means for identifying certificates containing the
    #   particular public key used in an application."
    # It should be derived from the public key, for example:
    #  - by taking the SHA1 hash of the public key
    #  - by taking the 60 least significant bits of the SHA1 hash
    #  - Other methods of generating unique numbers are also acceptable.
    # We"ll use the full sha-1 hash of the public key"s bytes
    builder.subject_key_id = hashlib.sha1(manufacturer_public_key_bytes).digest()

    builder.key_usage = 0b10100000.to_bytes(1,
                                            byteorder="big")  # digitalSignature & keyEncipherment bits must be set according to spec
    # builder.basicConstraints =  # Omit if end-entity cert, which a manufacturer is.
    builder.certificate_policy = "2.5.29.32.0"  # Any policy: http://www.oid-info.com/get/2.5.29.32.0
    builder.extended_key_usage = "2.16.840.1.114513.29.37"  # Optional in ASN1 but explanation in spec says it MUST be present. Variant of X509 http://www.oid-info.com/get/2.5.29.37.0
    builder.crl_distribution_point_uri = m2m.IA5String(u"http://www.ultimaker.com/rfid/revokationlist.crl")

    manufacturer_certificate = builder.build(signing_private_key_path=ultimaker_certificate_signing_key_path)

    m2m.m2m_certificate_to_file(manufacturer_certificate, certificate_output_path)

    return manufacturer_certificate


if __name__ == "__main__":
    import sys
    import pprint

    import os

    rows, columns = os.popen("stty size", "r").read().split()

    if len(sys.argv[1:]) == 6:
        try:
            print("Generating a certificate ...")
            name, locality, country, pubkey, privkey, outfile = sys.argv[1:]
            certificate = generateStationCertificate(name, locality, country, pubkey, privkey, outfile)
            pprint.pprint(certificate.native, width=columns, indent=0)  # Print as wide as we can
            print("Certificate stored in {certfile}".format(certfile=sys.argv[-1]))
            print("You need to move this file to {cert_path} on a programming station".format(
                cert_path=SignatureConfiguration.station_certificate_path))
        except OSError as ose:
            print(ose)
    else:
        print(
            "Usage: python3 -m taranis.scripts.generate_station_certificate manufacturer_name manufacturer_locality manufacturer_country "
            "station_tag_verification_key.pem ultimaker_certificate_signing_key.pem certificate.pem")
