import time
import logging

from pynfc.ntag_read import UnknownTagTypeException
from taranis.common.configuration.tag_configuration import TagConfiguration

log = logging.getLogger("TagApplication")


class TagApplication(object):
    def __init__(self, tag_interface):
        self.tag_interface = tag_interface

    def determineTagType(self):
        """
        Check the type of the tag
        """
        try:
            return self.tag_interface.determine_tag_type()
        except (OSError, IOError) as os_io_error:
            log.exception("Could not read tag capability container")
            raise os_io_error
        except UnknownTagTypeException as tag_type_unknown_error:
            log.exception(tag_type_unknown_error)
            raise tag_type_unknown_error

    ## Determine what exact type of NTag is present at our hardware
    # @return pynfc.ntag_read.TagType
    ## Find the number of tags present at the device
    # @return the number of tags
    def checkTagCount(self):
        count = self.tag_interface.count_targets()
        return count

    ## Blocking wait until the tag count is exactly 1, not more or less
    # @param until_event: a threading.Event that stops the waiting when it is set
    # @return when there is exactly 1 tag present at the device
    def waitForOneTag(self, until_event=None):
        while self.checkTagCount() != 1:
            if until_event and until_event.is_set():
                break
            time.sleep(0.1)

        return

    ## Check whether the user memory of the tag has some data
    # @return True if there is content, False if there is no content
    def checkTagHasContent(self, tag_type):
        first_page = tag_type["user_memory_start"]
        # According to the data sheet, the first page has some bytes initialized on delivery. So check the second
        first_content = self.tag_interface.read_page(first_page + 1)
        log.info("Tag has data {data} on first page".format(data=first_content))

        last_content = self.tag_interface.read_page(TagConfiguration.LAST_MESSAGE_PAGE)
        log.info("Tag has data {data} on last message page".format(data=last_content))

        return any(first_content) and any(last_content)

    ## Use the pasword to get the permissions it grants
    def authenticate(self, password=TagConfiguration.WRITE_PASSWORD, acknowledge=TagConfiguration.PASSWORD_ACK):
        return self.tag_interface.authenticate(password, acknowledge)
