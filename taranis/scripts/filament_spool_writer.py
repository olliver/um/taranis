#! /usr/bin/env python3
import binascii
import datetime
import fcntl
import json
import logging
import os
import uuid

from nfc.ndef import Record, Message, error
from nfc.ndef.signature import SignatureRecord, SignatureType
from pynfc.ntag_read import NTagReadWrite, NTagInfo, UnknownTagTypeException, TagType
from taranis.common.configuration.tag_configuration import TagConfiguration
from taranis.common.data import Material, MaterialStatistics
from taranis.common.data.fdm_whitelist import FdmWhitelist, MaterialNotPermittedException
from taranis.common.data.material_statistics import Unit
from taranis.common.serialization import MaterialSerializerV0, MaterialStatisticsSerializerV0
from taranis import errors
from taranis.scripts import colors
from taranis.scripts import beep
from taranis.scripts.configuration.signatures import SignatureConfiguration
from taranis.scripts.configuration.station import Station
from taranis.scripts.configuration.whitelist_decryption import WhitelistDecryptionConfiguration
from taranis.scripts.filament_spool_reader import FilamentSpoolReader
from taranis.scripts.tag_application import TagApplication

if SignatureConfiguration.signature_enabled:
    import m2m_certificates as m2m

log = logging.getLogger("filamentSpoolWriter")

EXIT_ON_EXCEPTION = False


## @brief Write a filament spool with the given data
#  @param fdm_whitelist an FdmWhitelist instance that tells whether the selected material is allowed (on the whitelist)
class FilamentSpoolWriter(TagApplication):
    def __init__(self, ntag_interface, material_serializer, stats_serializer, fdm_whitelist=None):
        super(FilamentSpoolWriter, self).__init__(ntag_interface)
        self.material_serializer = material_serializer
        self.stats_serializer = stats_serializer
        self.fdm_whitelist = fdm_whitelist

        # Some configuration
        self.uid_mirror_page, self.uid_mirror_byte = divmod(TagConfiguration.UID_MIRROR_POSITION,
                                                            NTagInfo.BYTES_PER_PAGE)

    ##  Assemble an NDEF Message that contains the following data:
    #   @param material: Which material is on the coil?

    #   @return: and NDEF message to be programmed into the tag
    #   @rtype Message
    def assembleMessage(self, uid, material, stats, debug=False):
        # UID must be 14 bytes, ascii encoded, UPPERCASE since that is how it will be mirrored in the tag itself.
        material_payload = self.material_serializer.serialize(material, binascii.hexlify(uid).upper())
        material_record = Record(record_type=self.material_serializer.ULTIMAKER_NL_MATERIAL, record_name=0x01,
                                 data=material_payload)

        stats_payload = self.stats_serializer.serialize(stats)
        stats_record = Record(record_type=self.stats_serializer.ULTIMAKER_NL_STAT, record_name=0x02, data=stats_payload)

        if not SignatureConfiguration.signature_enabled:
            signature_record = SignatureRecord(signature_uri=None, signature_type=SignatureType.NoSignaturePresent)
        else:
            signature_record = SignatureRecord(signature_uri=SignatureConfiguration.signature_uri,
                                               signature_type=SignatureConfiguration.signature_type,
                                               certificate_chain=SignatureConfiguration.getCertificateChain(),
                                               certificate_format=SignatureConfiguration.certificate_format,
                                               next_certificate_uri=SignatureConfiguration.next_certificate_uri)
            signature_record.signature = b"NOT_A_REAL_SIGNATURE"

            message = Message(material_record, signature_record, stats_record,
                              stats_record)  # Include the same stats record twice according to format

            # The signature must be calculated over the record in the form its written to the tag.
            # That means including the flags set at the first few bits of the message the record is embedded in.
            # Since I really don"t want to tweak these bits myself (they are influenced by message begin/end and length),
            # the content is serialized exactly how it would be written to the tag. Then we deserialize that and calculate the signature over that.
            # This is more or less guaranteed to be the same as what the verification step uses, unless the content is changed of course.
            message_bytes = message.to_bytes()
            message2 = Message(message_bytes)
            material_record2 = \
                [record for record in message2 if record.type == self.material_serializer.ULTIMAKER_NL_MATERIAL][0]
            bytes_to_sign = material_record2.to_bytes()

            signature = m2m.generate_signature(bytes_to_sign,
                                               private_key_path=SignatureConfiguration.station_signing_key_path)
            if debug:
                print("bytes_to_sign ({len}): {b}".format(b=binascii.hexlify(bytes_to_sign), len=len(bytes_to_sign)))
                print("Signature on write ({len}): {sig}".format(sig=binascii.hexlify(signature), len=len(signature)))
            signature_record.signature = signature

        return Message(material_record, signature_record, stats_record, stats_record)

    ##  @brief Generate the objects that will be serialized to the tag
    #   @type material uuid.UUID
    #   @param manufacturer: Which manufacturer is this programmer stationed at?
    #   @param programming_station: Which station is this?
    #   @param batch: What batch is this currently?
    #   @param total: How much material is on the coil?
    #   @param unit: In what unit tis the amount defined?
    #   @type unit Unit
    #   @param fallback_material: Which material is the fallback for the material on the coil? Defaults to None
    #   @return a tuple of the material and stats
    def generateObjects(self, material, programming_station, batch, total, unit):
        now = datetime.datetime.utcnow()
        # Our representation stores with only second accuracy

        if self.fdm_whitelist and not self.fdm_whitelist.isPermitted(material):  # Only check the whitelist when we have one
            raise MaterialNotPermittedException(material_id=material)

        material = Material(manufacturing_timestamp=now,
                            material_id=material,
                            programming_station_id=programming_station,
                            batch_code=batch)

        stats = MaterialStatistics(total=total,
                                   remaining=total,
                                   unit=unit)

        return material, stats

    ##  Write and NDEF Message to a binary file
    #   @param message: message to write
    #   @type message Message
    #   @param filename: filename that will be overwritten with the message, in binary
    #   @type filename str
    #   @return:
    def messageToFile(self, message, filename):
        message_bytes = message.to_bytes()
        self.bytesToFile(message_bytes, filename)

    ##  Put the message and config data in the correct place for an NTAG 213 NFC tag
    #   @param message: bytes of the Message
    #   @param config: configuration bytes
    #   @return: bytes in the correct memory layout
    #
    #   An NTAG 21x consists of a a variable number of 4-byte pages:
    #   - Pages 0 and 1 hold the serial number
    #   - Page 2 has 1 byte of the serial and some internal plus locking bytes
    #   - Page 3 has the Capability Container
    #   - Pages 4 to and including X are for user memory
    #   - Page after the user memory: 3 dynamic lock bytes and byte Reserved for Future Use - Implemented (RFUI)
    #   - Then 4 pages with configuration bytes
    @staticmethod
    def concatenateTagContent(message, config=None):
        internal_bytes = [0b00000000] * 16

        message_bytes = message.to_bytes()

        dynamic_lock_bytes = [0b00000000] * 4

        if config:
            tag_memory = list(internal_bytes) + list(message_bytes) + list(dynamic_lock_bytes) + list(config)
        else:
            tag_memory = []

        return bytes(tag_memory)

    ##  Write the given bytes to a file
    @staticmethod
    def bytesToFile(data, filename):
        try:
            with open(filename, "wb") as f:
                f.write(data)
        except IOError as io_error:
            log.exception("Could not write data to file '{file}': {err}".format(file=filename, err=io_error))

    ##  Write the given bytes to the tag. The maximal size of the content depends on the tag type
    def writeTag(self, content, tag_type):
        uid = self.tag_interface.setup_target()
        log.info("Writing uid = {id}".format(id=binascii.hexlify(uid)))

        self.tag_interface.set_easy_framing()

        self.tag_interface.write_user_memory(content, tag_type)


    def configureTag(self, tag_type):
        uid = self.tag_interface.setup_target()

        self.tag_interface.enable_uid_mirror(tag_type, self.uid_mirror_page, self.uid_mirror_byte)
        # TODO: Set the password to apply to all the data, password_from-0x00
        self.tag_interface.set_password(tag_type, TagConfiguration.WRITE_PASSWORD, TagConfiguration.PASSWORD_ACK)

        log.info("Tag {uid} has password {pwd} and ack {ack}".format(uid=binascii.hexlify(uid),
                                                                     pwd=TagConfiguration.WRITE_PASSWORD,
                                                                     ack=TagConfiguration.PASSWORD_ACK))

## Exit the application if the given lockfile is opened by a different process
def lock(lock_path="/tmp/filament_spool_writer.lock"):
    lock_file = os.open(lock_path, os.O_WRONLY | os.O_CREAT | os.O_TRUNC)
    try:
        fcntl.flock(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except BlockingIOError:
        print("Another process is already running.")
        exit(-1)


def printUsage():
    print("""Filament spool writer

filament_spool_writer.py info.json

Global options:
    --help: display this help message"
    --dry-run: do not actually write the tag"
    --dump: dump the content to dump.hex
    --overwrite: if there already is any content on the tag, overwrite with new content
    --exit-on-exception: Exit the application after an exception. Combined with LIBNFC_LOG_LEVEL=3, this helps debugging""")


def showError(*args, **kwargs):  # Decorate showError with a beep
    errors.showError(*args, **kwargs)
    beep.negativeBeep()


def tagTypeToString(val):
    for k, v in vars(TagType).items():
        if v == val:
            return k

if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    logging.basicConfig(filename=".filament_spool_writer.log", level=logging.INFO,
                        format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    lock()

    import sys
    if "--help" in sys.argv or "-h" in sys.argv:
        printUsage()
        exit(0)
    do_dump = "--dump" in sys.argv
    overwrite = "--overwrite" in sys.argv
    EXIT_ON_EXCEPTION = "--exit-on-exception" in sys.argv


    whitelist = FdmWhitelist.fromEncryptedFile(open(WhitelistDecryptionConfiguration.encrypted_path, "rb"),
                                               open(WhitelistDecryptionConfiguration.signature_path, "rb"),
                                               WhitelistDecryptionConfiguration.getWhitelistVerificationKey(),
                                               WhitelistDecryptionConfiguration.getWhitelistDecryptionKey())

    try:
        data_file_path = sys.argv[1]
        if not data_file_path:
            showError(5, log)
            exit(-5)
        data_to_program = json.load(open(data_file_path))
        material_to_program = uuid.UUID(data_to_program["material_uuid"])
        if not whitelist.isPermitted(material_to_program):
            showError(14, log, detail=" Material_id: {mat_id}".format(mat_id=material_to_program))
            exit(-14)
    except IndexError:
        showError(6, log)
        exit(-6)

    material_serializer = MaterialSerializerV0()
    stats_serializer = MaterialStatisticsSerializerV0()

    try:
        ntag_interface = NTagReadWrite(logging.getLogger("ntagReadWrite"))
    except OSError as ose:
        showError(18, log, ose)
        exit(-18)

    writer = FilamentSpoolWriter(ntag_interface, material_serializer, stats_serializer)  # Do not pass whitelist, only optional
    reader = FilamentSpoolReader(ntag_interface, material_serializer, stats_serializer)

    while True:
        try:
            print("Waiting for 1 tag...")
            writer.waitForOneTag()
            os.system("clear")  # Clear the screen

            try:
                current_tag_type, current_uid = writer.determineTagType()

                if current_tag_type != TagType.NTAG_216:
                    showError(17, log, detail="Tag is a {typ}".format(typ=current_tag_type["name"]))
                    continue
            except OSError as ose:
                showError(7, log, detail=ose)
                continue
            except UnknownTagTypeException as unknown_tag_type_error:
                showError(8, log, detail="Capability byte is {cap_byte}".format(
                    cap_byte=unknown_tag_type_error.capability_byte))
                continue

            tag_was_empty = True
            if not overwrite and writer.checkTagHasContent(current_tag_type):
                showError(3, log)
                tag_was_empty = False

            write_success = False
            write_material, write_stats = None, None
            if tag_was_empty:
                try:
                    write_material, write_stats = writer.generateObjects(material=material_to_program,
                                                                         programming_station=Station.station_id,
                                                                         batch=data_to_program["batch"],
                                                                         total=int(data_to_program["total"]),
                                                                         unit=Unit.fromstring(data_to_program["unit"]))
                    message_for_tag = writer.assembleMessage(current_uid, write_material, write_stats)
                except AttributeError as attr_error:
                    showError(9, log, detail=" Error with value {error}".format(error=attr_error))
                    if EXIT_ON_EXCEPTION:
                        exit(-9)
                    continue

                print("Writing...")
                if overwrite:
                    try:
                        authenticated = writer.authenticate()
                        if not authenticated:
                            showError(12, log)
                    except OSError as ose:
                        showError(15, log, detail=ose)
                try:
                    writer.writeTag(content=message_for_tag.to_bytes(), tag_type=current_tag_type)
                    writer.configureTag(tag_type=current_tag_type)
                    print(colors.green("Writing succeeded"))
                except (OSError, ValueError) as write_error:
                    err_text = showError(1, log, detail=write_error)
                    if EXIT_ON_EXCEPTION:
                        exit(-1)

            try:
                print("Reading...")
                read_material, serial_number, read_stats, crypto_ok = reader.readTagContent(tag_type=current_tag_type)
                if SignatureConfiguration.signature_enabled:
                    if crypto_ok:
                        print(colors.green("Cryptographic signing is OK"))
                    else:
                        showError(13, log)

                material_ok = read_material == write_material
                stats_ok = read_stats == write_stats
                write_success = (material_ok and stats_ok and crypto_ok)

                # Maps the (write_success,tag_was_empty)-tuple to a color for feedback.
                write_color_map = {(False, False): colors.yellow,  # write error (or did not occur) and tag had data
                                   (False, True): colors.red,
                                   # write error (or did not occur) and tag was empty: that is an error
                                   (True, False): colors.red,
                                   # Write successful but there was already data on the tag: should not happen, that means overwriting it.
                                   (True, True): colors.green}  # successfully wrote an empty tag, this is the happy flow.
                if tag_was_empty and not write_success:
                    showError(4, log,
                              detail="material: {mat}, stats: {stats}, crypto: {crypto}".format(mat=material_ok,
                                                                                                 stats=stats_ok,
                                                                                                 crypto=crypto_ok))
                write_color = write_color_map[(write_success, tag_was_empty)]

                if write_color == colors.green:
                    beep.positiveBeep()
                else:
                    beep.negativeBeep()

                print(write_color("The serial number of this tag is {serial}".format(serial=serial_number)))
                print(write_color(
                    "This tag contains: \n{mat}".format(mat=read_material).replace(",", ",\n\t").replace("(",
                                                                                                         "(\n\t ").replace(
                        ")", "\n)")))
                print(write_color(
                    "{stats}".format(stats=read_stats).replace(",", ",\n\t").replace("(", "(\n\t ").replace(")",
                                                                                                            "\n)")))

            except (IndexError, ValueError) as read_error:
                showError(10, log, detail=read_error)
                if EXIT_ON_EXCEPTION:
                    exit(-10)
            except error.LengthError as length_error:
                showError(11, log, detail=length_error)
                if EXIT_ON_EXCEPTION:
                    exit(-11)
            except OSError as os_error:
                showError(2, log, detail=os_error)
                if EXIT_ON_EXCEPTION:
                    exit(-2)
        except KeyboardInterrupt:
            print("Exit filament spool writer")
            exit(0)
