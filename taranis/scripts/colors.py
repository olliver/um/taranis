def red(string):
    return "\033[30;41m" + string + "\033[0m"


def green(string):
    return "\033[30;42m" + string + "\033[0m"


def yellow(string):
    return "\033[30;43m" + string + "\033[0m"