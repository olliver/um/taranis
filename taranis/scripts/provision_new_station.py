#! /usr/bin/env python3

## Generate all the configuration files needed for a new programming station
import argparse
import os
import shutil
import logging

from taranis.scripts.colors import red, green

import m2m_certificates

from taranis.scripts.generate_manufacturer_certificate import generateStationCertificate
from taranis.scripts.generate_whitelist_encryption_keys import generateWhitelistEncryptionKeys
from taranis.scripts.configuration.whitelist_decryption import WhitelistDecryptionConfiguration

log = logging.getLogger(__name__.split(".")[-1])

station_private_conf_path = os.path.join("/media/", "ultimaker", "nfc_config/")
station_public_conf_path = os.path.join("/media/", "ultimaker", "nfc_config_pub/")


class Provisioner():
    def __init__(self):
        pass

    def provision(self):
        pass


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG,
                        format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    parser = argparse.ArgumentParser(
        description="""Generate all the files needed to provision a new programming station""")

    parser.add_argument("station_id",
                        type=int,
                        help="""ID number of the station""")

    parser.add_argument("manufacturer_name",
                        type=str,
                        help="""Name of the manufacturer""")

    parser.add_argument("--manufacturer_alias",
                        required=False,
                        type=str,
                        help="""Alias for the manufacturer in case the real name should not be in the certificate""")

    parser.add_argument("manufacturer_locality",
                        type=str,
                        help="""Locality where the manufacturer is located""")

    parser.add_argument("manufacturer_country",
                        type=str,
                        help="""Country where the manufacturer is located""")

    parser.add_argument("certificate_signing_key",
                        type=str,
                        help="""Private key of Ultimaker used to sign manufacturer certificates with""")

    parser.add_argument("whitelist_verification_key",
                        type=str,
                        help="""Ultimaker's public verification key stations will use to verify that a whitelist is signed by Ultimaker""")

    parser.add_argument("--station_storage",
                        default=station_private_conf_path,
                        required=False,
                        type=str,
                        help="""Destination for files that will go to the station""")

    parser.add_argument("--local_storage",
                        default=os.path.expanduser("~/taranis-private-data"),
                        required=False,
                        type=str,
                        help="""Destination for files that will stay private, at Ultimaker""")

    args = parser.parse_args()

    admin_storage = os.path.join(args.local_storage, str(args.station_id))  # Where to store files locally
    # Locally, we have 3 types of files:
    # - Stuff that is known by us, is for station operation, but the station-owner should not edit: station_private
    # - Stuff known by us that the station-owner should edit to operate the station: station_public
    # - Stuff known only by Ultimaker: ultimaker_private
    station_private_storage_loc = os.path.join(admin_storage, "station_private")
    station_public_storage_loc = os.path.join(admin_storage, "station_public")
    ultimaker_private_storage_loc = os.path.join(admin_storage, "ultimaker_private")

    station_private_storage_usb = args.station_storage  # location of the USB stick that goes to the station
    station_id = os.path.join(station_private_storage_loc, "station_id")

    tag_signing_key_private = os.path.join(station_private_storage_loc, "station_tag_signing_key.pem")
    tag_signing_key_public = os.path.join(station_private_storage_loc, "station_tag_verification_key.pem")

    station_certificate = os.path.join(station_private_storage_loc, "station_certificate.pem")

    whitelist_encryption = os.path.join(ultimaker_private_storage_loc, "station_whitelist_encryption_key.pem")
    whitelist_decryption = os.path.join(station_private_storage_loc, "station_whitelist_decryption_key.pem")

    ultimaker_whitelist_verification_key_src = args.whitelist_verification_key
    ultimaker_whitelist_verification_key_dst = os.path.join(station_private_storage_loc, "ultimaker_whitelist_verification_key.pem")

    if not os.path.exists(admin_storage):
        os.mkdir(admin_storage)
        os.mkdir(station_private_storage_loc)
        os.mkdir(station_public_storage_loc)
        os.mkdir(ultimaker_private_storage_loc)
    log.info("Using local storage at {store}".format(store=admin_storage))

    with open(station_id, "w") as f:
        f.write(str(args.station_id))
        log.info("Station ID {id} stored at {id_file}".format(id_file=station_id, id=args.station_id))

    m2m_certificates.generate_ec_private_key(private_key_path=tag_signing_key_private)
    m2m_certificates.extract_ec_public_key(private_key_path=tag_signing_key_private, public_key_path=tag_signing_key_public)
    log.info("Tag signing keys for station {id} stored in {private}, {pub}".format(private=tag_signing_key_private, pub=tag_signing_key_public, id=args.station_id))

    generateStationCertificate(args.manufacturer_alias if args.manufacturer_alias else args.manufacturer_name,
                               args.manufacturer_locality, args.manufacturer_country,
                               tag_signing_key_public, args.certificate_signing_key,
                               station_certificate)
    log.info("Certificate for station stored at {cert}".format(cert=station_certificate))

    generateWhitelistEncryptionKeys(whitelist_decryption, whitelist_encryption)
    log.info("Whitelist encryption and decryption keys for station {id} stored in {private} and {pub}".format(private=whitelist_decryption, pub=whitelist_encryption, id=args.station_id))

    shutil.copy(ultimaker_whitelist_verification_key_src, ultimaker_whitelist_verification_key_dst)
    log.info("Copied {src} to {dst}".format(src=ultimaker_whitelist_verification_key_src, dst=ultimaker_whitelist_verification_key_dst))

    try:
        src_files = os.listdir(station_private_storage_loc)
        for file_name in src_files:
            full_file_name = os.path.join(station_private_storage_loc, file_name)
            if (os.path.isfile(full_file_name)):
                shutil.copy(full_file_name, station_private_storage_usb)
                log.info("Copied {src} to {dst}".format(src=full_file_name, dst=station_private_storage_usb))
        log.info("All station files are copied from local storage to the station storage")
        log.info(green("Provisioning for station {id} done".format(id=args.station_id)))
    except Exception as ex:
        log.error(ex)
        log.warning(red("Files are not yet copied to the station storage. Please do this manually"))


