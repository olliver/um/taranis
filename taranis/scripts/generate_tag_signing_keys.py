#! /usr/bin/env python3

import m2m_certificates
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Generate tag signing and verification keys for a programming station""")

    parser.add_argument("--private",
                        default="station_tag_signing_key.pem",
                        required=False,
                        type=str,
                        help="""Destination for signing key""")

    parser.add_argument("--public",
                        default="station_tag_verification_key.pem",
                        required=False,
                        type=str,
                        help="Destination for verification key")

    args = parser.parse_args()

    private = args.private
    m2m_certificates.generate_ec_private_key(private_key_path=args.private)
    m2m_certificates.extract_ec_public_key(private_key_path=args.private, public_key_path=args.public)

    print("Keys are saved to {priv} and {pub}".format(priv=args.private, pub=args.public))