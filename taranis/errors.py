from taranis.scripts import colors

error_messages = {  1: "Could not write data to tag",
                    2: "Could not read tag, is there a tag present?",
                    3: "Tag already contains data",
                    4: "Write-verification failed, read data does not match what was written",
                    5: "No correct data file given. Try with -h for help",
                    6: "No data file given. Try with -h for help",
                    7: "Could not determine tag type, is there a tag present?",
                    8: "Tag type unknown. ",
                    9: "Cannot generate tag data when including a given value",
                    10: "Could not read data",
                    11: "Expected more data available for reading",
                    12: "Tag protected with unknown password, overwrite likely to fail",
                    13: "The signature of the tag could not be verified",
                    14: "Material with ID {mat_id} is not permitted for programming on this station",
                    15: "Error during authentication",
                    16: "Error when creating service RemoteObject",
                    17: "Wrong tag type",
                    18: "Could not connect with NFC Programming device"}

## @brief Log an error and print it to the console in red text
# @param error_number the number of the error, as defined in error_Messages
# @param log an instance of logging.Logger
# @param detail Additional details for the error that will be written to the log
# @returns formatted text with the error descriptions and the detail
def showError(error_number, log, detail=None):
    text = "{number:03d}: {message}".format(number=error_number, message=error_messages[error_number])
    text_with_detail = text + ". Detail: {detail}".format(detail=detail) if detail else text + ". See log for details"
    if log:
        log.error(text_with_detail)
    print(colors.red("ERROR: "+text_with_detail))
    return text_with_detail