import datetime
import contextlib
import logging

@contextlib.contextmanager
def stopwatch(label=None):
    start = datetime.datetime.now()

    yield

    end = datetime.datetime.now()
    dt = end - start
    if label:
        logging.info("{lbl} took {dur}".format(lbl=label, dur=dt))
    else:
        logging.info("Unlabeled block took {dur}".format(lbl=label, dur=dt))