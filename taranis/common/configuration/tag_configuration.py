## @brief Specific information on how we use and configure NTags
class TagConfiguration(object):
    WRITE_PASSWORD = b"\x01\x02\x03\x04"
    PASSWORD_ACK = b"\xaa\xaa"  # recognizable bitstring.
    UID_MIRROR_POSITION = 44  # At what byte must the UID mirror be placed? This follows from the tag definitions

    LAST_MESSAGE_PAGE = 0x3c  # What is the last page a message's bytes will land?
