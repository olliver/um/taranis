import os
import csv
import tarfile
import shutil
import time
import errno
import functools
import logging

from taranis.common.data.material import Material
from taranis.common.data.material_statistics import MaterialStatistics
from taranis.common.data.unit import Unit

## Decorator with an argument: fixer. Fixer is a function that attempts to fix a problem (which caused an exception)
#  After the fixer finishes, the original function is ran again
def retry_with_fixer(fixer):
    ## Decorator for a given func that handles a specific OSError and calls 'fixer' if this occurs.
    def retry(func):
        @functools.wraps(func)
        def wrap(*args, **kwargs):
            try:
                func(*args, **kwargs)
            except OSError as error:
               if error.errno == errno.ENOSPC:  # If there is no space left for more logs:
                   fixer()
                   return func(*args, **kwargs)
               else:
                   raise error
        return wrap
    return retry

## Log data for each programmed spool
class TraceabilityLogger(object):
    fieldnames = ["timestamp", "spool_id", "station_id", "material_id", "batch_code", "amount", "unit"]
    def __init__(self, filename):
        self.filename = filename

        self.log_dir = os.path.dirname(self.filename)

        self.open_logfile = retry_with_fixer(self._make_space_for_logs)(self.open_logfile)
        self.trace = retry_with_fixer(self._make_space_for_logs)(self.trace)
        self.export = retry_with_fixer(self._make_space_for_logs)(self.export)

        self.open_logfile()

    ## Open the log file and write a header
    def open_logfile(self, mode="a"):
        logging.info("Opening logfile {f}".format(f=self.filename))
        with open(self.filename, mode) as trace_logfile:
            writer = csv.DictWriter(trace_logfile, fieldnames=self.fieldnames)
            writer.writeheader()  # In case the fieldnames change in the future, the fieldnames are known by going back up the file

    ## write a row to the log file
    def trace(self, spool_id, material, stats):
        assert isinstance(material, Material)
        assert isinstance(stats, MaterialStatistics)

        with open(self.filename, 'a') as trace_logfile:
            writer = csv.DictWriter(trace_logfile, fieldnames=self.fieldnames)

            entry = dict()
            entry["timestamp"] = material.manufacturing_timestamp
            entry["spool_id"] = spool_id
            entry["station_id"] = material.programming_station_id
            entry["material_id"] = material.material_id
            entry["batch_code"] = material.batch_code
            entry["amount"] = stats.total
            entry["unit"] = Unit.tostring(stats.unit)

            writer.writerow(entry)
            logging.info("Wrote entry: {entry}".format(entry=entry))

            trace_logfile.flush()

    ## Export the old log, compress the old one and create a new log
    def export(self, destination):
        #Copy the existing file to the selected destination
        logging.info("Export logfile {orig} to {dest}".format(orig=self.filename, dest=destination))
        shutil.copy(self.filename, destination)

        self.compress_log()

        self.open_logfile(mode="w")  # Start a new log file as we open it for writing rather than appending

    ## Remove old logs and compress the current
    def _make_space_for_logs(self):
        logging.warning("There is no space left on the device, making space")
        self.remove_old_logs()
        self.compress_log()

    ## Compress the current log toa timestamped .tar.gz file
    def compress_log(self):
        log_file_ext = os.path.basename(self.filename)
        name, ext = os.path.splitext(log_file_ext)
        # If the logfile originally is at /var/log/taranis/tracing.csv   ,  the compressed file must go to /var/log/taranis/tracing_20161114_144837.tar.gz
        #                                 |----log_dir----|--name-|-ext-|
        compressed_log_name = os.path.join(self.log_dir, name + "_" + time.strftime("%Y%m%d_%H%M%S") + ".tar.gz")

        logging.info("Compressing logs: add {orig} to {dest}".format(orig=self.filename, dest=compressed_log_name))
        # Compress the existing log file
        with tarfile.open(compressed_log_name, "w:gz") as tar:
            tar.add(self.filename, arcname=log_file_ext)

    ## Remove the old logs
    def remove_old_logs(self):
        dir_entries = [os.path.join(self.log_dir, entry) for entry in os.listdir(self.log_dir)]

        tar_gzs = filter(lambda dir_entry: os.path.splitext(dir_entry)[1] == ".gz", dir_entries)

        sorted_entries = sorted(tar_gzs, key=lambda dir_entry: os.stat(dir_entry).st_ctime)  # Oldest to youngest file

        oldest = sorted_entries[-1:]  # Only leave some files

        for old_dir_entry in oldest:
            logging.info("Removing old log {old}".format(old=old_dir_entry))
            os.remove(old_dir_entry)

if __name__ == "__main__":
    from taranis.common.data.material import Material
    from taranis.common.data.material_statistics import MaterialStatistics
    import datetime
    from taranis.common.data.unit import Unit
    from uuid import uuid4

    logging.basicConfig(level=logging.DEBUG)

    mat = Material(datetime.datetime.now(), uuid4(), 99, "34678909654")
    stat = MaterialStatistics(Unit.milligram, 750000, 750000)

    tracer = TraceabilityLogger("/media/ultimaker/nfc_config/trace.csv")

    # tracer.export("/media/ultimaker/nfc_config/dump/")

    for i in range(1000):
        tracer.trace(bytes([1,2,3,4,5,6,7]), mat, stat)

    tracer.export("/media/ultimaker/nfc_config/dump/")