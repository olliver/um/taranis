"""Classes to (de)serialize data.Material-classes.
There will be multiple versions of the binary format and thus multiple (de)serializers,
that are compatible with various versions"""

from ..data.material_statistics import MaterialStatistics
from ..data.unit import Unit

from datetime import timedelta
from . import checksum_error
from .crc8 import Crc8


##    @brief Serialize and deserialize a MaterialStatistics to and from a bytearray,
#     according to the payload format as described in the NFC tag definition
#     https://docs.google.com/a/ultimaker.com/document/d/1QlCaHTPI9n3rWuUkVwX_0l9z5IzrI1hfUMJQ5V2Lirk/edit?usp=sharing
#     dd. June 15, 2016
class MaterialStatisticsSerializerV0(object):
    _version = 0
    # Records encoded with this type can be decoded with a (version of) this deserializer
    ULTIMAKER_NL_STAT = "urn:nfc:ext:ultimaker.nl:stat"

    def __init__(self):
        pass
        
    ##  Serialize a Material-class to a byte array
    #   @param stats: MaterialStatistics to be serialized
    #   @type stats: MaterialStatistics
    #   @return the MaterialStatistics serialized into a bytearray
    #   @rtype bytearray
    def serialize(self, stats):
        assert isinstance(stats, MaterialStatistics)

        payload = bytearray(20)
        # A memoryview enforces that the slice assignments do not change the size and shape of the array
        payload_view = memoryview(payload)

        payload_view[0] = self._version  # Payload version

        payload_view[1] = self._version  # Compatibility record version

        payload_view[2] = stats.unit.value

        # Python slices *IN*clude the start, but *EX*clude the end
        payload_view[3:7] = stats.total.to_bytes(4, "big").rjust(4, bytes([0]))

        payload_view[7:11] = stats.remaining.to_bytes(4, "big").rjust(4, bytes([0]))

        payload_view[11:19] = int(stats.total_usage_duration.total_seconds()).to_bytes(8, "big").rjust(4, bytes([0]))

        payload_view[19] = Crc8.crc(payload_view[:19])

        return payload

    ##  Parse bytes into MaterialStatistics
    #   @param payload: bytes that encode a MaterialStatistics
    #   @return: MaterialStatistics
    def deserialize(self, payload):
        version = payload[0]
        compatibility_record_version = payload[1]

        if compatibility_record_version > self._version:
            raise ValueError(
                "The payload indicates compatibility record version {payload_version} while '{self}'' has version {own_version}".format(
                    payload_version=version,
                    self=self,
                    own_version=self._version))

        unit = Unit(payload[2])

        total = int.from_bytes(payload[3:7], "big")

        remaining = int.from_bytes(payload[7:11], "big")


        duration = timedelta(seconds=int.from_bytes(payload[11:19], byteorder="big"))

        expected_checksum = payload[19]
        actual_checksum = Crc8.crc(payload[:19])

        if expected_checksum != actual_checksum:
            raise checksum_error.ChecksumError("Checksum {act} does not match expected checksum {exp}".format(act=actual_checksum, exp=expected_checksum))

        return MaterialStatistics(unit, total, remaining, duration)