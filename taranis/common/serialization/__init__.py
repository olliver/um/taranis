## Serializers serialize and deserialize bytes into a usable class.
# In the future, there may be multiple versions of the encoding format, in which case a new (de)serializer is needed.
# For backward compatibility, the older versions of these (de)serializers must still be available.
# Perhaps the gain several subclasses of simply new (super)classes wil be created.
# Every (de)Serializer has a version though, which can be inspected without parsing its class name.

from .material_serializer import MaterialSerializerV0
from .material_statistics_serializer import MaterialStatisticsSerializerV0
from .fallback_material_serializer import FallbackMaterialSerializerV0