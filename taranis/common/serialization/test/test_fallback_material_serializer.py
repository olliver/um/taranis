#! /usr/bin/env python
"""Test of various stats serializations"""
import unittest

import datetime
import uuid

from ..fallback_material_serializer import FallbackMaterialSerializerV0, FallbackMaterial
from ..checksum_error import ChecksumError


class TestFallbackMaterialSerializerSimple(unittest.TestCase):
    def setUp(self):
        self.material = FallbackMaterial(material_id=uuid.UUID(int=1234567890))
        self.serializer = FallbackMaterialSerializerV0()
        self.payload = self.serializer.serialize(self.material)

    def testLengthIs108Bytes(self):
        self.assertEqual(len(self.payload), 19)

    def testFields(self):
        self.assertEqual(self.payload[0], 0)  # Version
        self.assertEqual(self.payload[1], 0)  # Compatibility record version

class TestSerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.material = FallbackMaterial(material_id=uuid.UUID(int=1234567890))
        self.serializer = FallbackMaterialSerializerV0()
        self.payload = self.serializer.serialize(self.material)

    def testDeserializedEqualsOriginal(self):
        deserialized = self.serializer.deserialize(self.payload)

        self.assertEqual(deserialized.material_id, self.material.material_id)
        self.assertEqual(deserialized, self.material)


class TestChecksum(unittest.TestCase):
    def setUp(self):
        self.material = FallbackMaterial(material_id=uuid.UUID(int=1234567890))
        self.serializer = FallbackMaterialSerializerV0()

        self.payload = self.serializer.serialize(self.material)

    def testRaiseOnFailedChecksumWhenContentChanges(self):
        corrupted_payload = bytearray(self.payload)

        corrupted_payload[9] += 1 # The payload has an alteration

        self.assertRaises(ChecksumError, self.serializer.deserialize, corrupted_payload)

    def testRaiseOnFailedChecksumWhenChecksumByteChanges(self):
        corrupted_payload = bytearray(self.payload)

        corrupted_payload[18] += 1 # The checksum itself has an alteration

        self.assertRaises(ChecksumError, self.serializer.deserialize, corrupted_payload)

class TestManySerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.serializer = FallbackMaterialSerializerV0()

    def testRoundtrip1(self):
        material = FallbackMaterial(material_id=uuid.UUID(int=1234567890))

        deserialized = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)

    def testRoundtrip2(self):
        material = FallbackMaterial(material_id=uuid.UUID(int=12345678901234567890123456789012))

        deserialized = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)


class TestDeserializerCannotHandleNewerCompatVersion(unittest.TestCase):

    def testCompatVersion2(self):
        payload = bytearray([6, 2, 1,1,1,1,1,1,1,1])  # Everything past the 2nd byte should not matter

        serializer = FallbackMaterialSerializerV0()
        self.assertRaises(ValueError, serializer.deserialize, payload)


if __name__ == "__main__":
    unittest.main()