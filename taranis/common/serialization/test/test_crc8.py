import unittest

from ..crc8 import Crc8

## Verify that CRC8 using http://www.sunshine2k.de/coding/javascript/crc/crc_js.html as a reference implementation.
# Set the "polynomial" there to 0x07, "initial value" to 0x00 and "final xor value" to 0x00
class TestCyclicRedundancyCheck(unittest.TestCase):
    def testOutput1(self):
        data = bytearray([0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00])  # 16 0x00 bytes

        checksum = Crc8.crc(data)

        self.assertEqual(checksum, 0x00)

    def testOutput2(self):
        data = bytearray([0x00, 0x00, 0x00, 0x00,
                          0x00, 0x10, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00])

        checksum = Crc8.crc(data)

        self.assertEqual(checksum, 0xf7)

    def testOutput3(self):
        data = bytearray([0x00, 0x00, 0x00, 0x00,
                          0x00, 0x10, 0x00, 0x00,
                          0x00, 0x00, 0xff, 0x00,
                          0x00, 0x00, 0x00, 0x00])

        checksum = Crc8.crc(data)

        self.assertEqual(checksum, 0x58)


    def testSensitivity1(self):
        data1 = bytearray([0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00])  # 16 0x00 bytes

        data2 = bytearray([0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x08, 0x00,
                           0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00])  # A single bit changed (Can you spot the differences?!)

        checksum1 = Crc8.crc(data1)
        checksum2 = Crc8.crc(data2)

        self.assertNotEqual(checksum1, checksum2)