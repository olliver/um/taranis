#! /usr/bin/env python
"""Test of various stats serializations"""
import unittest

import datetime
import uuid

from ..material_serializer import MaterialSerializerV0, Material


class TestMaterialSerializerSimple(unittest.TestCase):
    def setUp(self):
        self.material = Material(manufacturing_timestamp=datetime.datetime(year=2016, month=6, day=15, hour=15,
                                                                           minute=15, second=15,
                                                                           tzinfo=datetime.timezone.utc),
                                 material_id=uuid.UUID(int=1234567890),
                                 programming_station_id=0,
                                 batch_code="testbatch")
        self.serializer = MaterialSerializerV0()
        self.payload = self.serializer.serialize(self.material)

    def testLengthIs108Bytes(self):
        self.assertEqual(len(self.payload), 108)

    def testFields(self):
        self.assertEqual(self.payload[0], 0)  # Version
        self.assertEqual(self.payload[1], 0)  # Compatibility record version


class TestSerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.material = Material(manufacturing_timestamp=datetime.datetime(year=2016, month=6, day=15, hour=15,
                                                                           minute=15, second=15),
                                 material_id=uuid.UUID(int=1234567890),
                                 programming_station_id=0,
                                 batch_code="testbatch")
        self.serializer = MaterialSerializerV0()
        self.payload = self.serializer.serialize(self.material)

    def testDeserializedEqualsOriginal(self):
        deserialized, serial_number = self.serializer.deserialize(self.payload)

        self.assertEqual(deserialized.manufacturing_timestamp, self.material.manufacturing_timestamp)
        self.assertEqual(deserialized.material_id, self.material.material_id)
        self.assertEqual(deserialized.programming_station_id, self.material.programming_station_id)
        self.assertEqual(deserialized.batch_code, self.material.batch_code)

        self.assertEqual(deserialized, self.material)


class TestManySerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.serializer = MaterialSerializerV0()

    def testRoundtrip1(self):
        material = Material(manufacturing_timestamp=datetime.datetime(year=2016, month=6, day=15, hour=15,
                                                                            minute=15, second=15),
                                  material_id=uuid.UUID(int=1234567890),
                                  programming_station_id=0,
                                  batch_code="testbatch")

        deserialized, serial_number = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)

    def testRoundtrip2(self):
        material = Material(manufacturing_timestamp=datetime.datetime(year=2029, month=12, day=24, hour=15,
                                                                            minute=15, second=15),
                                  material_id=uuid.UUID(int=12345678901234567890123456789012),
                                  programming_station_id=0,
                                  batch_code="testbatch")

        deserialized, serial_number = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)

    ##  Test manufacturing time past the 32bit timestamp range
    def testRoundtrip3(self):
        material = Material(manufacturing_timestamp=datetime.datetime(year=2140, month=12, day=24, hour=15,
                                                                            minute=15, second=15),
                                  material_id=uuid.UUID(int=12345678901234567890123456789012),
                                  programming_station_id=0,
                                  batch_code="testbatch")

        deserialized, serial_number = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)

    ##  Test manufacturing time past the 32bit timestamp range and a randomly generated UUID
    def testRoundtrip4(self):
        material = Material(manufacturing_timestamp=datetime.datetime(year=2140, month=12, day=24, hour=15,
                                                                            minute=15, second=15),
                                  material_id=uuid.uuid4(), # Random UUID
                                  programming_station_id=0,
                                  batch_code="testbatch")

        deserialized, serial_number = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)

    ##  Test manufacturing time past the 32bit timestamp range, a randomly generated UUID, max values for manufacturer and programming station ID
    def testRoundtrip5(self):
        material = Material(manufacturing_timestamp=datetime.datetime(year=2140, month=12, day=24, hour=15,
                                                                      minute=15, second=15),
                            material_id=uuid.uuid4(),  # Random UUID
                            programming_station_id=2**16-1,  # max value for 2 bytes
                            batch_code="testbatch")

        deserialized, serial_number = self.serializer.deserialize(self.serializer.serialize(material))
        self.assertEqual(deserialized, material)


class TestDeserializerCannotHandleNewerCompatVersion(unittest.TestCase):

    def testCompatVersion2(self):
        payload = bytearray([6, 2, 1,1,1,1,1,1,1,1])  # Everything past the 2nd byte should not matter

        serializer = MaterialSerializerV0()
        self.assertRaises(ValueError, serializer.deserialize, payload)


if __name__ == "__main__":
    unittest.main()