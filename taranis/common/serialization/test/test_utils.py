import unittest

import datetime

from ..datetime_conversions import DateTimeConversions


class TestDateTimeRoundtrip(unittest.TestCase):
    def testNow(self):
        dt = datetime.datetime.utcnow()
        dt_as_bytes = DateTimeConversions.datetimeToBytes(dt)

        dt_from_bytes = DateTimeConversions.datetimeFromBytes(dt_as_bytes)

        self.assertEqual(dt, dt_from_bytes)

    def testSummer2016(self):
        """
        At 06:28:15 UTC on Sunday, 7 February 2106, the Unix time will reach FFFFFFFF or 4,294,967,295 seconds which,
        for systems that hold the time on 32 bit unsigned numbers, is the maximum attainable.
        For these systems, the next second will be incorrectly interpreted as 00:00:00 Thursday 1 January 1970 UTC.

        """
        dt = datetime.datetime(year=2016, month=7, day=7, hour=9, minute=45, second=15)
        dt_as_bytes = DateTimeConversions.datetimeToBytes(dt)

        dt_from_bytes = DateTimeConversions.datetimeFromBytes(dt_as_bytes)

        self.assertEqual(dt, dt_from_bytes)

    def testSigned32BitMax(self):
        """
        At 03:14:08 UTC on Tuesday, 19 January 2038, 32-bit versions of the Unix time stamp will cease to work,
        as it will overflow the largest value that can be held in a signed 32-bit number (7F FF FF FF or 2,147,483,647).

        As such, the second use below is 7, not the 8 as stated above here.
        """

        dt = datetime.datetime(year=2038, month=1, day=19, hour=3, minute=14, second=7)
        dt_as_bytes = DateTimeConversions.datetimeToBytes(dt)

        dt_from_bytes = DateTimeConversions.datetimeFromBytes(dt_as_bytes)

        self.assertEqual(dt, dt_from_bytes)

    def testUnsigned32BitMax(self):
        """
        At 06:28:15 UTC on Sunday, 7 February 2106, the Unix time will reach FFFFFFFF or 4,294,967,295 seconds which,
        for systems that hold the time on 32 bit unsigned numbers, is the maximum attainable.
        For these systems, the next second will be incorrectly interpreted as 00:00:00 Thursday 1 January 1970 UTC.

        """
        dt = datetime.datetime(year=2106, month=2, day=7, hour=6, minute=28, second=15)
        dt_as_bytes = DateTimeConversions.datetimeToBytes(dt)

        dt_from_bytes = DateTimeConversions.datetimeFromBytes(dt_as_bytes)

        self.assertEqual(dt, dt_from_bytes)
