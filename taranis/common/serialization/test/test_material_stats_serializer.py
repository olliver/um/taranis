#! /usr/bin/env python
"""Test of various stats serializations"""
import unittest

import datetime

from ..material_statistics_serializer import MaterialStatisticsSerializerV0, MaterialStatistics, Unit
from ..checksum_error import ChecksumError


class TestMaterialSerializerSimple(unittest.TestCase):
    def setUp(self):
        self.stats = MaterialStatistics(Unit.milligram, 750, 500, datetime.timedelta(seconds=1234567890))
        self.serializer = MaterialStatisticsSerializerV0()

        self.payload = self.serializer.serialize(self.stats)

    def testLengthIs20Bytes(self):
        self.assertEqual(len(self.payload), 20)

    def testFields(self):
        self.assertEqual(self.payload[0], 0)  # Version
        self.assertEqual(self.payload[1], 0)  # Compatibility record version


class TestChecksum(unittest.TestCase):
    def setUp(self):
        self.stats = MaterialStatistics(Unit.milligram, 750, 500, datetime.timedelta(seconds=1234567890))
        self.serializer = MaterialStatisticsSerializerV0()

        self.payload = self.serializer.serialize(self.stats)

    def testRaiseOnFailedChecksumWhenContentChanges(self):
        corrupted_payload = bytearray(self.payload)

        corrupted_payload[9] -= 1 # The payload has an alteration

        self.assertRaises(ChecksumError, self.serializer.deserialize, corrupted_payload)

    def testRaiseOnFailedChecksumWhenChecksumByteChanges(self):
        corrupted_payload = bytearray(self.payload)

        corrupted_payload[19] -= 1 # The checksum itself has an alteration

        self.assertRaises(ChecksumError, self.serializer.deserialize, corrupted_payload)


class TestSerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.stats = MaterialStatistics(Unit.milligram, 750, 500, datetime.timedelta(seconds=1234567890))
        self.serializer = MaterialStatisticsSerializerV0()
        self.payload = self.serializer.serialize(self.stats)

    def testDeserializedEqualsOriginal(self):
        deserialized = self.serializer.deserialize(self.payload)

        assert isinstance(deserialized, MaterialStatistics)  # Helps the IDE with type inference and completion

        self.assertEqual(deserialized.unit,  self.stats.unit)
        self.assertEqual(deserialized.total,  self.stats.total)
        self.assertEqual(deserialized.remaining,  self.stats.remaining)
        self.assertEqual(deserialized.total_usage_duration, self.stats.total_usage_duration)

        self.assertEqual(deserialized, self.stats)


class TestManySerializeDeserialize(unittest.TestCase):

    def setUp(self):
        self.serializer = MaterialStatisticsSerializerV0()

    def testRoundtrip1(self):
        stats = MaterialStatistics(Unit.milligram, 750, 500, datetime.timedelta(seconds=1234567890))

        deserialized = self.serializer.deserialize(self.serializer.serialize(stats))

        self.assertEqual(deserialized, stats)

    def testRoundtrip2(self):
        stats = MaterialStatistics(Unit.milliliter, 2**16-1, 2**16-2, datetime.timedelta(seconds=1234567890))

        deserialized = self.serializer.deserialize(self.serializer.serialize(stats))

        self.assertEqual(deserialized, stats)

    def testRoundtrip3(self):
        stats = MaterialStatistics(Unit.millimeter, 2**16-1, 2**16-2, datetime.timedelta(seconds=1234567890))

        deserialized = self.serializer.deserialize(self.serializer.serialize(stats))

        self.assertEqual(deserialized, stats)

    def testRoundtrip4(self):
        """Note that the remaining is larger than the total. This is a valid content of the data.
        Process-wise, it makes no sense, but that is irrelevant here."""
        stats = MaterialStatistics(Unit.millimeter, 10, 99, datetime.timedelta(seconds=1234567890))

        deserialized = self.serializer.deserialize(self.serializer.serialize(stats))

        self.assertEqual(deserialized, stats)


class TestDeserializerCannotHandleNewerCompatVersion(unittest.TestCase):

    def testCompatVersion2(self):
        payload = bytearray([6, 2, 1,1,1,1,1,1,1,1])  # Everything past the 2nd byte should not matter

        serializer = MaterialStatisticsSerializerV0()
        self.assertRaises(ValueError, serializer.deserialize, payload)


if __name__ == "__main__":
    unittest.main()