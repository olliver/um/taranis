"""Utility functions for handling bytes and other serialization related functions"""

import datetime
import struct

## @brief Convert datetime instances from/to bytes.
# Would be more nicely expressed via a Rust-style Trait though
class DateTimeConversions(object):

    ##  Convert the datetime to a bytearray of the given width
    #   @param date_time the datetime to be converted
    #   @type date_time datetime.datetime
    #   @param width in how many bytes to represent this datetime?
    #   @type width int
    #   @return a bytearray representing the datetime
    #   @rtype bytearray
    @staticmethod
    def datetimeToBytes(date_time, width=8):

        second_since_epoch = float(date_time.timestamp())
        byte_repr = struct.pack(">d", second_since_epoch)
        return byte_repr
        # return second_since_epoch.to_bytes(width, "big").rjust(width, bytes([0]))  # 8 bytes Unix 64 bit timestamp, in big endian

    ##  Convert a bytearray to a datetime instance
    #   @param byte_repr the bytes to be converted
    #   @return the datetime encoded in the bytes
    #   @rtype datetime.datetime
    @staticmethod
    def datetimeFromBytes(byte_repr):
        second_since_epoch = struct.unpack(">d", byte_repr)[0]
        return datetime.datetime.fromtimestamp(second_since_epoch)