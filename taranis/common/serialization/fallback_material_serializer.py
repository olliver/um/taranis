"""Classes to (de)serialize data.FallbackMaterial-classes.
There will be multiple versions of the binary format and thus multiple (de)serializers,
that are compatible with various versions"""
from . import checksum_error
from .crc8 import Crc8
from ..data.fallback_material import FallbackMaterial

from . import datetime_conversions

import uuid

##  @brief Serialize and deserialize a FallbackMaterial to and from a bytearray,
#   according to the payload format as described in the NFC tag definition
#   https://docs.google.com/a/ultimaker.com/document/d/1QlCaHTPI9n3rWuUkVwX_0l9z5IzrI1hfUMJQ5V2Lirk/edit?usp=sharing
#   dd. June 15, 2016
class FallbackMaterialSerializerV0(object):
    _version = 0
    # Records encoded with this type can be decoded with a (version of) this deserializer
    ULTIMAKER_NL_FALLBACK = "urn:nfc:ext:ultimaker.nl:fallback"


    def __init__(self):
        pass

    ##  Serialize a FallbackMaterial-class to a byte array
    #   @param material: FallbackMaterial to be serialized
    #   @type material FallbackMaterial
    #   @return the FallbackMaterial serialized into a bytearray
    #   @rtype bytearray
    def serialize(self, material):
        assert isinstance(material, FallbackMaterial)

        payload = bytearray(19)
        # A memoryview enforces that the slice assignments do not change the size and shape of the array
        payload_view = memoryview(payload)

        payload_view[0] = self._version  # Payload version

        payload_view[1] = self._version  # Compatibility record version

        # Python slices *IN*clude the start, but *EX*clude the end
        payload_view[2:18] = material.material_id.bytes.rjust(16, bytes([0])) # FallbackMaterial ID 16 bytes UUID

        payload_view[18] = Crc8.crc(payload_view[:18])

        return payload

    ##  Parse bytes into FallbackMaterial
    #   @param payload: bytes that encode a FallbackMaterial
    #   @return: FallbackMaterial
    def deserialize(self, payload):
        version = payload[0]
        compatibility_record_version = payload[1]

        if compatibility_record_version > self._version:
            raise ValueError("The payload indicates compatibility record version {payload_version} while '{self}' has version {own_version}".format(
                payload_version=version,
                self=self,
                own_version=self._version))

        uuid_bytes = None
        try:
            uuid_bytes = bytes(payload[2:18])  # Python slices *IN*clude the start, but *EX*clude the end
            material_id = uuid.UUID(bytes=uuid_bytes)
        except ValueError as value_error:
            raise ValueError("Cannot create UUID from uuid_bytes {bytes}: {err}".format(bytes=uuid_bytes, err=value_error))

        expected_checksum = payload[18]
        actual_checksum = Crc8.crc(payload[:18])

        if expected_checksum != actual_checksum:
            raise checksum_error.ChecksumError(
                "Checksum {act} does not match expected checksum {exp}".format(act=actual_checksum, exp=expected_checksum))

        return FallbackMaterial(material_id=material_id)