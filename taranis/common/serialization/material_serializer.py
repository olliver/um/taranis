"""Classes to (de)serialize data.Material-classes.
There will be multiple versions of the binary format and thus multiple (de)serializers,
that are compatible with various versions"""

from ..data.material import Material

from .datetime_conversions import DateTimeConversions

import datetime
import uuid

##   @brief Serialize and deserialize a Material to and from a bytearray,
#    according to the payload format as described in the NFC tag definition
#    https://docs.google.com/a/ultimaker.com/document/d/1QlCaHTPI9n3rWuUkVwX_0l9z5IzrI1hfUMJQ5V2Lirk/edit?usp=sharing
#    dd. June 15, 2016

MAX_BATCHCODE_LEN = 64


class MaterialSerializerV0(object):
    _version = 0
    # Records encoded with this type can be decoded with a (version of) this deserializer
    ULTIMAKER_NL_MATERIAL = "urn:nfc:ext:ultimaker.nl:material"

    def __init__(self):
        pass

    ##  Serialize a Material-class to a byte array
    #   @param material Material to be serialized
    #   @type material Material
    #   @return the Material serialized into a bytearray
    #   @rtype bytearray
    def serialize(self, material, tag_uid=None):
        assert isinstance(material, Material)

        payload = bytearray(108)
        # A memoryview enforces that the slice assignments do not change the size and shape of the array
        payload_view = memoryview(payload)

        payload_view[0] = self._version  # Payload version

        payload_view[1] = self._version  # Compatibility record version

        # Python slices *IN*clude the start, but *EX*clude the end

        # The serial number is static and cannot be written. It will be filled in by the tag itself when read
        # However, for signing, the correct UID needs to be known because it is included in the signature
        # Because the tag UID is not important for the actual material, it is passed in here
        payload_view[2:16] =  bytearray([0xff] * 14) if not tag_uid else tag_uid

        payload_view[16:24] = DateTimeConversions.datetimeToBytes(material.manufacturing_timestamp)

        payload_view[24:40] = material.material_id.bytes.rjust(16, bytes([0]))  # Material ID 16 bytes UUID

        payload_view[40:42] = material.programming_station_id.to_bytes(2, "big").rjust(2, bytes([0]))

        batch_bytes = bytearray(material.batch_code, "utf-8")
        if len(batch_bytes) > MAX_BATCHCODE_LEN:
            raise ValueError("Batch_code '{batch}' is longer than the max length of 64 bytes: {batchlen}".format(
                batch=material.batch_code,
                batchlen=len(material.batch_code)))
        payload_view[42:106] = batch_bytes.ljust(MAX_BATCHCODE_LEN, bytes([0]))

        return payload

    ##  Parse bytes into a material and the serial number embedded in the material payload.
    #   The serial is not part of Material and returned as the second return value
    #   @param payload:
    #   @return: material and serial embedded in the payload
    #   @rtype (Material, bytes)
    def deserialize(self, payload):
        version = payload[0]
        compatibility_record_version = payload[1]

        if compatibility_record_version > self._version:
            raise ValueError(
                "The payload indicates compatibility record version {payload_version} while '{self}' has version {own_version}".format(
                    payload_version=version,
                    self=self,
                    own_version=self._version))

        serial_number = payload[2:16]

        timestamp = DateTimeConversions.datetimeFromBytes(payload[16:24])

        uuid_bytes = None
        try:
            uuid_bytes = bytes(payload[24:40])
            material_id = uuid.UUID(bytes=uuid_bytes)
        except ValueError as value_error:
            raise ValueError("Cannot create UUID from uuid_bytes {bytes}: {err}".format(bytes=uuid_bytes, err=value_error))

        programming_station_id = int.from_bytes(payload[40:42], "big")

        batch_code_bytes = bytearray(payload[42:106])
        batch_code_content_bytes = batch_code_bytes[0:batch_code_bytes.index(0)]  # Up to first null byte
        batch_code = batch_code_content_bytes.decode(encoding="utf-8")

        return Material(manufacturing_timestamp=timestamp,
                                 material_id=material_id,
                                 programming_station_id=programming_station_id,
                                 batch_code=batch_code),\
               serial_number