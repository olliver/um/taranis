## Definition of a batch, with its code, the material and the amount used per spool


from .mixins import CommonEqualityMixin
from .unit import Unit

import uuid
import csv

import logging
log = logging.getLogger(__name__.split(".")[-1])

REQUIRED_FIELDS = ["batch_code", "material_id", "quantity", "unit"]
OPTIONAL_FIELDS = ["label", "material_type", "material_color"]
ORDERED_FIELDS = ["batch_code", "material_id", "quantity", "unit", "label"] # Ordered as a human would put them

## @brief Hold all the information needed to define a batch
class BatchDefinition(CommonEqualityMixin):
    ##
    #   @param batch_code str the code given to this batch
    #   @param material_id uuid.UUID what material is put on the spools of this batch
    #   @param quantity int how much material is there per spool in this batch
    #   @param unit Unit in what unit is the amount defined
    def __init__(self, batch_code, material_id, quantity, unit, label=None):
        self.batch_code = batch_code
        self.material_id = material_id
        self.quantity = quantity
        self.unit = unit
        self.material_name = None
        self.label = label

    def __repr__(self):
        return "BatchDefinition('{batch}', {mat}, {qnt}, {unit}, '{label}')".format(batch=self.batch_code,
                                                                         mat=self.material_id,
                                                                         qnt=self.quantity,
                                                                         unit=self.unit,
                                                                         label=self.label)

    def __str__(self):
        material = self.material_name if self.material_name else self.material_id
        quantity_desc = self.describeAmount(self.quantity, self.unit)
        if self.label:
            return "{label}, {batch}: {mat}, {qnt_desc}".format(batch=self.batch_code,
                                                                mat=material,
                                                                qnt_desc=quantity_desc,
                                                                label=self.label)
        else:
            return "{batch}: {mat}, {qnt_desc}".format(batch=self.batch_code,
                                                       mat=material,
                                                       qnt_desc=quantity_desc)

    ## @brief Generate a human readable description of an amount, given the number and the unit
    # @param the number of units in the amount
    # @param the unit in which the amount is described
    # @returns str with a human readable description of the amount
    def describeAmount(self, number, unit):
        if unit == Unit.milligram:
            amount_description = str(int(number) / 1000) + " g"
        elif unit == Unit.millimeter:
            amount_description = str(int(number) / 1000) + " m"
        elif unit == Unit.milliliter:
            amount_description = str(int(number) / 1000) + " L"
        else:
            amount_description = str(int(number)) + Unit.tostring(unit)
        return amount_description

    ## @brief Load a BatchDefinition from a dictionary
    # This of course requires the dictionary to have some fields (See REQUIRED_FIELDS for those)
    # @param material_lut dict (material_type: str, material_color: str) --> UUID that maps the type and color of the material to its UUID
    @staticmethod
    def fromDict(d, material_lut=None):
        for req_field in REQUIRED_FIELDS:
            assert req_field in d

        if d["material_id"] and d["material_id"] != "?":
            d["material_id"] = uuid.UUID(d["material_id"])
        # else:
        #     log.warning("Material_id is empty in {row}".format(row=d))
        d["quantity"] = int(d["quantity"])

        try:
            parsed_unit = Unit.fromstring(d["unit"])
            d["unit"] = parsed_unit
        except AttributeError:
            # The given unit is not known, so we"ll try to convert it instead
            # These mappings map a unit name to a base unit. Eg. a gram = 1000 milligram
            mappings = {"mg":   (1, Unit.milligram),
                        "gram": (1000, Unit.milligram),
                        "g":    (1000, Unit.milligram),
                        "meter":(1000, Unit.millimeter),
                        "m":    (1000, Unit.millimeter),
                        "ml":   (1,    Unit.milliliter),
                        "liter":(1000, Unit.milliliter),
                        "l":    (1000, Unit.milliliter),
                        "L":    (1000, Unit.milliliter),
                        "kilo": (1000*1000, Unit.milligram),
                        "kg":   (1000*1000, Unit.milligram)}
            if d["unit"] in mappings:
                factor, new_unit= mappings[d["unit"]]
                update = {"unit":new_unit, "quantity":d["quantity"]*factor}
                d.update(update)
            else:
                raise ValueError("Unit definition in {d} cannot be understood. "
                                 "Understood units are {units}".format(d=d, units=mappings.keys()))
        if "label" in d:
            pass  # Its already a dict

        if "material_type" in d and "material_color" in d \
                and d["material_type"] and d["material_color"] \
                and material_lut:
            material_key = (d["material_type"].lower(), d["material_color"].lower())
            material_id = material_lut[material_key]

            d["material_id"] = material_id

        # Update the dictionary to have only the fields needed
        if "material_type" in d and "material_color" in d:
            del d["material_type"]
            del d["material_color"]

        return BatchDefinition(**d)

    ## @brief Convert a BatchDefinition to a dictionary
    def toDict(self):
        return {"material_id":str(self.material_id),
                "batch_code":self.batch_code,
                "quantity":self.quantity,
                "unit":Unit.tostring(self.unit),
                "label":self.label}

    ## @brief Load a list of BatchDefinitions from a CSV file with columns for the REQUIRED_FIELDS
    # @param material_lut, see BatchDefinition.fromDict for description
    @staticmethod
    def fromCsv(csv_file, material_lut=None):
        required_fields = REQUIRED_FIELDS

        default_dialect = csv.excel

        sample = csv_file.read(1024)
        try:
            dialect = csv.Sniffer().sniff(sample)
            dialect.delimiter = ","  # TODO: EM-1489: apparently this is needed, otherwise the Sniffers sets it to 'h' ????
        except csv.Error as csv_error:
            log.info("Could not determine CSV dialect from sample '{sample}'".format(sample=sample))
            dialect = default_dialect
        finally:
            csv_file.seek(0)  # Go back to the start of the file, otherwise the first bytes read are halfway some row

        reader = csv.DictReader(csv_file, dialect=dialect)
        log.debug("Fieldnames: {fn}".format(fn=reader.fieldnames))
        given_fields = set(reader.fieldnames) if reader.fieldnames else set()

        missing_fields = set(required_fields) - set(given_fields)
        if missing_fields:
            raise ValueError("The batch definition files does not have all the required fields. "
                             "No data for {miss} in file. Given fields are {given}, but required are {req}".format(miss=missing_fields, given=given_fields, req=required_fields))

        batch_definitions = []
        for i, row in enumerate(reader):
            try:
                batch_definitions += [BatchDefinition.fromDict(row, material_lut)]
            except Exception as ex:
                # import ipdb; ipdb.set_trace()
                log.error("Could not parse batch definition from row {i}: {row}: {ex}".format(row=row, ex=ex, i=i))
        return sorted(batch_definitions, key=lambda batch: batch.batch_code)

    @staticmethod
    def toCsv(csv_file, batch_definitions):
        csv_file.seek(0)
        writer = csv.DictWriter(csv_file, ORDERED_FIELDS, dialect=csv.excel)

        writer.writeheader()
        log.debug("{len} batches to save in csv".format(len=len(batch_definitions)))

        writer.writerows([batch_def.toDict() for batch_def in batch_definitions])

        csv_file.flush()

    ## @brief Add a batch definition to an opened csv file.
    # The file will be completely overwritten and redefined by this action to, so it doe snot have to deal with a lot of optionals
    @staticmethod
    def addToCsv(csv_file, batch_definition):
        try:
            current = BatchDefinition.fromCsv(csv_file)
            log.debug("{len} current batches".format(len=len(current)))
        except ValueError as err:
            log.error("Could not load batches from csv_file: {err}".format(err=err))
            current = []
        finally:
            csv_file.seek(0)

        BatchDefinition.toCsv(csv_file, current+[batch_definition])

    def __eq__(self, other):
        if isinstance(other, BatchDefinition):
            return self.__dict__ == other.__dict__
        else:
            return False
