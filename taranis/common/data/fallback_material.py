"""
Definition of the FallbackMaterial class
"""

import uuid

from .mixins import CommonEqualityMixin

## @brief What material to use in case the normal Material cannot be determined
class FallbackMaterial(CommonEqualityMixin):

    ##
    #   @param material_id: UUID of the material
    #   @type material_id uuid.UUID
    def __init__(self, material_id):
        assert isinstance(material_id, uuid.UUID)

        self.material_id = material_id
