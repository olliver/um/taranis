"""
Definition of the Material class
"""

import datetime
import uuid

from .mixins import CommonEqualityMixin

## @brief Information about the production of the material on a spool
class Material(CommonEqualityMixin):
    ##
    #   @param manufacturing_timestamp When is the material produced?
    #   @type manufacturing_timestamp datetime.datetime
    #   @param material_id uuid.UUID
    #   @param programming_station_id int
    #   @param batch_code str
    #
    def __init__(self, manufacturing_timestamp, material_id, programming_station_id, batch_code):
        assert isinstance(material_id, uuid.UUID)

        self.manufacturing_timestamp = manufacturing_timestamp
        self.material_id = material_id
        self.programming_station_id = programming_station_id
        self.batch_code = batch_code

    def __repr__(self):
        return "Material({ts}, {mat_id}, {ps_id}, {batch})".format(
            ts=self.manufacturing_timestamp,
            mat_id=self.material_id,
            ps_id=self.programming_station_id,
            batch=self.batch_code
        )
