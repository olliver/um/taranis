from enum import Enum

##    @brief units used for 3D printing filament spools
class Unit(Enum):
    unused = 0x00
    millimeter = 0x01
    milligram = 0x02
    milliliter = 0x03
    undefined = 0xff

    @classmethod
    def tostring(cls, val):
        return val.name

    @classmethod
    def fromstring(cls, str):
        return getattr(cls, str)

