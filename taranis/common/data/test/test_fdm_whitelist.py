import io
import unittest

from ..fdm_whitelist import FdmWhitelist, MaterialPermission, MaterialNotPermittedException

import datetime
import uuid

from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


class TestMaterialPermission(unittest.TestCase):
    def setUp(self):
        self.ultimaker_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                              backend=default_backend())
        self.ultimaker_public_key = self.ultimaker_private_key.public_key()

        self.manufacturer1_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                                  backend=default_backend())
        self.manufacturer1_public_key = self.manufacturer1_private_key.public_key()

        self.manufacturer2_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                                  backend=default_backend())
        self.manufacturer2_public_key = self.manufacturer2_private_key.public_key()

    def testValidity(self):
        now = datetime.datetime(year=2016, month=8, day=26)
        one_week_from_now = datetime.datetime(year=2016, month=9, day=2)
        one_month_from_now = datetime.datetime(year=2016, month=9, day=26)
        two_months_from_now = datetime.datetime(year=2016, month=9, day=26)

        permission = MaterialPermission(uuid.uuid4(), one_month_from_now)  # Permission holds till one month from now

        self.assertTrue(permission.isPermitted(now))
        self.assertTrue(permission.isPermitted(one_week_from_now))
        self.assertFalse(permission.isPermitted(one_month_from_now))  # Must be renewed BEFORE validity expires
        self.assertFalse(permission.isPermitted(two_months_from_now))

    def testCryptoRoundTrip(self):
        permission = MaterialPermission(uuid.uuid4(),
                                        datetime.datetime(year=2016, month=8, day=26),
                                        self.manufacturer1_public_key)

        cypher = permission.encrypt()

        # the intended manufacturer should be able to decrypt:
        self.assertEqual(permission, MaterialPermission.decrypt(cypher, self.manufacturer1_private_key))

        # a different manufacturer must not be able to decrypt:
        with self.assertRaises(MaterialNotPermittedException):
            self.assertEqual(permission, MaterialPermission.decrypt(cypher, self.manufacturer2_private_key))


class TestFdmWhitelist(unittest.TestCase):
    def setUp(self):
        self.ultimaker_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                              backend=default_backend())
        self.ultimaker_public_key = self.ultimaker_private_key.public_key()

        self.manufacturer_a_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                                   backend=default_backend())
        self.manufacturer_a_public_key = self.manufacturer_a_private_key.public_key()

        self.manufacturer_b_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048,
                                                                   backend=default_backend())
        self.manufacturer_b_public_key = self.manufacturer_b_private_key.public_key()

        self.now = datetime.datetime(year=2016, month=1, day=1)
        self.then = datetime.datetime(year=2016, month=2, day=2)
        self.permission1 = MaterialPermission(uuid.uuid4(), self.now, self.manufacturer_a_public_key)
        self.permission2 = MaterialPermission(uuid.uuid4(), self.then, self.manufacturer_a_public_key)

        self.permission3 = MaterialPermission(uuid.uuid4(), self.now, self.manufacturer_b_public_key)
        self.permission4 = MaterialPermission(uuid.uuid4(), self.then, self.manufacturer_b_public_key)

    def testCryptoRoundTrip(self):
        whitelist = FdmWhitelist([self.permission1, self.permission2, self.permission3, self.permission4])

        whitelist_file = io.BytesIO()
        signature_file = io.BytesIO()

        whitelist.encryptAndSign(self.ultimaker_private_key, whitelist_file,
                                 signature_file)  # This happens at Ultimaker

        between_now_and_then = datetime.datetime(year=2016, month=1, day=15)
        assert self.now < between_now_and_then < self.then

        # The decryption below happens at each programming station:

        whitelist_file.seek(0, 0)
        signature_file.seek(0, 0)

        whitelist_for_a = FdmWhitelist.fromEncryptedFile(whitelist_file, signature_file,
                                                         self.ultimaker_public_key, self.manufacturer_a_private_key,
                                                         current_time=between_now_and_then)

        self.assertNotIn(self.permission1, whitelist_for_a.permitted_materials)  # The first permission has expired
        self.assertIn(self.permission2, whitelist_for_a.permitted_materials)  # The first permission has expired

        whitelist_file.seek(0, 0)
        signature_file.seek(0, 0)

        whitelist_for_b = FdmWhitelist.fromEncryptedFile(whitelist_file, signature_file,
                                                         self.ultimaker_public_key, self.manufacturer_b_private_key,
                                                         current_time=between_now_and_then)

        self.assertNotIn(self.permission3, whitelist_for_b.permitted_materials)  # The first permission has expired
        self.assertIn(self.permission4, whitelist_for_b.permitted_materials)  # The first permission has expired
