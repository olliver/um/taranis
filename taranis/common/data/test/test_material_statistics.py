import unittest

import datetime

from ..material_statistics import MaterialStatistics
from ..unit import Unit


class BasicTests(unittest.TestCase):

    def setUp(self):
        self.stats = MaterialStatistics(Unit.milligram, 750, 500, datetime.datetime(year=2016, month=6, day=15,
                                                                               hour=15, minute=15, second=15,
                                                                               tzinfo=datetime.timezone.utc))

    def testRepresentation(self):
        representation = repr(self.stats)

        self.assertEqual(representation, "MaterialStatistics(Unit.milligram, 750, 500, 2016-06-15 15:15:15+00:00)")

    def testEquality(self):
        stat2 = MaterialStatistics(Unit.milligram, 750, 500, datetime.datetime(year=2016, month=6, day=15,
                                                                               hour=15, minute=15, second=15,
                                                                               tzinfo=datetime.timezone.utc))

        self.assertEqual(self.stats, stat2)
