import unittest

from ..unit import Unit


class TestUnit(unittest.TestCase):

    def testFromString(self):
        self.assertEqual(Unit.milligram, Unit.fromstring("milligram"))
        self.assertEqual(Unit.milliliter, Unit.fromstring("milliliter"))
        self.assertEqual(Unit.millimeter, Unit.fromstring("millimeter"))

    def testToString(self):
        self.assertEqual("milligram", Unit.tostring(Unit.milligram))
        self.assertEqual("milliliter", Unit.tostring(Unit.milliliter))
        self.assertEqual("millimeter", Unit.tostring(Unit.millimeter))
