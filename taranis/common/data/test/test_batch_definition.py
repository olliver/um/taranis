import unittest
import os

import io

from ..batch_definition import BatchDefinition
from ..unit import Unit

import uuid

example_batch_file = """batch_code,material_id,quantity,unit
1123456789,2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49,750,g
987654321f,a8955dc3-9d7e-404d-8c03-0fd6fee7f22d,2000,ml
sadhgfkgkh,3ee70a86-77d8-4b87-8005-e4a1bc57d2ce,200,m
TESTBATCH,2433b8fb-dcd6-4e36-9cd5-9f4ee551c04c,750,g
TESTBATCH,3ee70a86-77d8-4b87-8005-e4a1bc57d2ce,750,g
TESTBATCH,0e01be8c-e425-4fb1-b4a3-b79f255f1db9,750,g
TESTBATCH,e509f649-9fe6-4b14-ac45-d441438cb4ef,750,g
TESTBATCH,532e8b3d-5fd4-4149-b936-53ada9bd6b85,750,g
TESTBATCH,d9549dba-b9df-45b9-80a5-f7140a9a2f34,750,g
TESTBATCH,44a029e6-e31b-4c9e-a12f-9282e29a92ff,750,g
TESTBATCH,fe3982c8-58f4-4d86-9ac0-9ff7a3ab9cbc,750,g
TESTBATCH,9cfe5bf1-bdc5-4beb-871a-52c70777842d,750,g
TESTBATCH,9c1959d0-f597-46ec-9131-34020c7a54fc,750,g
TESTBATCH,d9fc79db-82c3-41b5-8c99-33b3747b8fb3,750,g
TESTBATCH,fe15ed8a-33c3-4f57-a2a7-b4b78a38c3cb,750,g
TESTBATCH,2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49,750,g
TESTBATCH,5253a75a-27dc-4043-910f-753ae11bc417,750,g
TESTBATCH,5df7afa6-48bd-4c19-b314-839fe9f08f1f,750,g
TESTBATCH,7c9575a6-c8d6-40ec-b3dd-18d7956bfaae,750,g
TESTBATCH,763c926e-a5f7-4ba0-927d-b4e038ea2735,750,g
TESTBATCH,7cbdb9ca-081a-456f-a6ba-f73e4e9cb856,750,g
TESTBATCH,3400c0d1-a4e3-47de-a444-7b704f287171,750,g
TESTBATCH,0b4ca6ef-eac8-4b23-b3ca-5f21af00e54f,750,g
TESTBATCH,e873341d-d9b8-45f9-9a6f-5609e1bcff68,750,g
TESTBATCH,8b75b775-d3f2-4d0f-8fb2-2a3dd53cf673,750,g
TESTBATCH,a8955dc3-9d7e-404d-8c03-0fd6fee7f22d,750,g
TESTBATCH,881c888e-24fb-4a64-a4ac-d5c95b096cd7,750,g
TESTBATCH,173a7bae-5e14-470e-817e-08609c61e12b,750,g
TESTBATCH,10961c00-3caf-48e9-a598-fa805ada1e8d,750,g
TESTBATCH,00181d6c-7024-479a-8eb7-8a2e38a2619a,750,g
TESTBATCH,4d816290-ce2e-40e0-8dc8-3f702243131e,750,g
TESTBATCH,b9176a2a-7a0f-4821-9f29-76d882a88682,750,g
TESTBATCH,7ff6d2c8-d626-48cd-8012-7725fa537cc9,750,g
TESTBATCH,bd0d9eb3-a920-4632-84e8-dcd6086746c5,750,g
TESTBATCH,8a38a3e9-ecf7-4a7d-a6a9-e7ac35102968,750,g
TESTBATCH,e92b1f0b-a069-4969-86b4-30127cfb6f7b,750,g
TESTBATCH,5e786b05-a620-4a87-92d0-f02becc1ff98,750,g
TESTBATCH,a9c340fe-255f-4914-87f5-ec4fcb0c11ef,750,g
TESTBATCH,1aca047a-42df-497c-abfb-0e9cb85ead52,750,g
TESTBATCH,6df69b13-2d96-4a69-a297-aedba667e710,750,g
TESTBATCH,c64c2dbe-5691-4363-a7d9-66b2dc12837f,750,g
TESTBATCH,e256615d-a04e-4f53-b311-114b90560af9,750,g
TESTBATCH,6a2573e6-c8ee-4c66-8029-3ebb3d5adc5b,750,g
"""

example_batch_file_with_labels = """batch_code,material_id,quantity,unit,label
1123456789,2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49,750,g,label9
987654321f,a8955dc3-9d7e-404d-8c03-0fd6fee7f22d,2000,ml,labeld
sadhgfkgkh,3ee70a86-77d8-4b87-8005-e4a1bc57d2ce,200,m,labele
TESTBATCH,2433b8fb-dcd6-4e36-9cd5-9f4ee551c04c,750,g,labelc
TESTBATCH,3ee70a86-77d8-4b87-8005-e4a1bc57d2ce,750,g,labele
TESTBATCH,0e01be8c-e425-4fb1-b4a3-b79f255f1db9,750,g,label9
TESTBATCH,e509f649-9fe6-4b14-ac45-d441438cb4ef,750,g,labelf
TESTBATCH,532e8b3d-5fd4-4149-b936-53ada9bd6b85,750,g,label5
TESTBATCH,d9549dba-b9df-45b9-80a5-f7140a9a2f34,750,g,label4
TESTBATCH,44a029e6-e31b-4c9e-a12f-9282e29a92ff,750,g,labelf
TESTBATCH,fe3982c8-58f4-4d86-9ac0-9ff7a3ab9cbc,750,g,labelc
TESTBATCH,9cfe5bf1-bdc5-4beb-871a-52c70777842d,750,g,labeld
TESTBATCH,9c1959d0-f597-46ec-9131-34020c7a54fc,750,g,labelc
TESTBATCH,d9fc79db-82c3-41b5-8c99-33b3747b8fb3,750,g,label3
TESTBATCH,fe15ed8a-33c3-4f57-a2a7-b4b78a38c3cb,750,g,labelb
TESTBATCH,2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49,750,g,label9
TESTBATCH,5253a75a-27dc-4043-910f-753ae11bc417,750,g,label7
TESTBATCH,5df7afa6-48bd-4c19-b314-839fe9f08f1f,750,g,labelf
TESTBATCH,7c9575a6-c8d6-40ec-b3dd-18d7956bfaae,750,g,labele
TESTBATCH,763c926e-a5f7-4ba0-927d-b4e038ea2735,750,g,label5
TESTBATCH,7cbdb9ca-081a-456f-a6ba-f73e4e9cb856,750,g,label6
TESTBATCH,3400c0d1-a4e3-47de-a444-7b704f287171,750,g,label1
TESTBATCH,0b4ca6ef-eac8-4b23-b3ca-5f21af00e54f,750,g,labelf
TESTBATCH,e873341d-d9b8-45f9-9a6f-5609e1bcff68,750,g,label8
TESTBATCH,8b75b775-d3f2-4d0f-8fb2-2a3dd53cf673,750,g,label3
TESTBATCH,a8955dc3-9d7e-404d-8c03-0fd6fee7f22d,750,g,labeld
TESTBATCH,881c888e-24fb-4a64-a4ac-d5c95b096cd7,750,g,label7
TESTBATCH,173a7bae-5e14-470e-817e-08609c61e12b,750,g,labelb
TESTBATCH,10961c00-3caf-48e9-a598-fa805ada1e8d,750,g,labeld
TESTBATCH,00181d6c-7024-479a-8eb7-8a2e38a2619a,750,g,labela
TESTBATCH,4d816290-ce2e-40e0-8dc8-3f702243131e,750,g,labele
TESTBATCH,b9176a2a-7a0f-4821-9f29-76d882a88682,750,g,label2
TESTBATCH,7ff6d2c8-d626-48cd-8012-7725fa537cc9,750,g,label9
TESTBATCH,bd0d9eb3-a920-4632-84e8-dcd6086746c5,750,g,label5
TESTBATCH,8a38a3e9-ecf7-4a7d-a6a9-e7ac35102968,750,g,label8
TESTBATCH,e92b1f0b-a069-4969-86b4-30127cfb6f7b,750,g,labelb
TESTBATCH,5e786b05-a620-4a87-92d0-f02becc1ff98,750,g,label8
TESTBATCH,a9c340fe-255f-4914-87f5-ec4fcb0c11ef,750,g,labelf
TESTBATCH,1aca047a-42df-497c-abfb-0e9cb85ead52,750,g,label2
TESTBATCH,6df69b13-2d96-4a69-a297-aedba667e710,750,g,label0
TESTBATCH,c64c2dbe-5691-4363-a7d9-66b2dc12837f,750,g,labelf
TESTBATCH,e256615d-a04e-4f53-b311-114b90560af9,750,g,label9
TESTBATCH,6a2573e6-c8ee-4c66-8029-3ebb3d5adc5b,750,g,labelb
"""

example_batch_file_with_labels_without_ids = """batch_code,material_id,quantity,unit,label,material_type,material_color
TEST_BATCH,?,2000,ml,label0,PP,NaTuRaL
TEST_BATCH,?,750,g,label1,PLA,Red
TEST_BATCH,?,2000,ml,label2,TPU 95A,Red
TEST_BATCH,?,2000,ml,label3,ABS,Blue
TEST_BATCH,?,350,ml,label4,PVA,Natural"""

# example_batch_file_with_labels_withoud_ids = """batch_code,material_id,quantity,unit,label,material_type,material_color
#         test1,?,750,g,label9,PLA,rEd
#         test2,?,2000,ml,label10,PP,NaTuRaL"""


class TestBatchDefinition(unittest.TestCase):
    def testFromDict1(self):
        material_id = uuid.uuid4()

        d = {"batch_code": "blablablablablabla",
             "material_id": str(material_id),
             "quantity": "750",
             "unit":"gram",}

        definition = BatchDefinition.fromDict(d)
        self.assertEqual(definition.batch_code, d["batch_code"])
        self.assertEqual(definition.material_id, material_id)
        self.assertEqual(definition.quantity, 750000)
        self.assertEqual(definition.unit, Unit.milligram)

    def testFromDict2(self):
        material_id = uuid.uuid4()

        d = {"batch_code": "3847596285065067346803465863806",
             "material_id": str(material_id),
             "quantity": "1",
             "unit":"kilo",}

        definition = BatchDefinition.fromDict(d)
        self.assertEqual(definition.batch_code, d["batch_code"])
        self.assertEqual(definition.material_id, material_id)
        self.assertEqual(definition.quantity, 1000000)
        self.assertEqual(definition.unit, Unit.milligram)
        self.assertEqual(definition.label, None)

    def testFromDict3(self):
        material_id = uuid.uuid4()

        d = {"batch_code": "3847596285065067346803465863806",
             "material_id": str(material_id),
             "quantity": "1",
             "unit":"kilo",
             "label":"articlenumber#"}

        definition = BatchDefinition.fromDict(d)
        self.assertEqual(definition.batch_code, d["batch_code"])
        self.assertEqual(definition.material_id, material_id)
        self.assertEqual(definition.quantity, 1000000)
        self.assertEqual(definition.unit, Unit.milligram)
        self.assertEqual(definition.label, "articlenumber#")

    def testFromDictByTypeAndColor(self):
        material_id = uuid.uuid4()  # Some random material ID, that belongs to "pimpelpaars PLA"
        material_lut = {("pla", "pimpelpaars"):material_id}

        d = {"batch_code": "3847596285065067346803465863806",
             "material_id": "",  # The ID is not filled, will be inferred from the type and color, via the lut aboves
             "quantity": "1",
             "unit":"kilo",
             "label":"articlenumber#",
             "material_type":"PLA",
             "material_color":"pimpelpaars"}

        definition = BatchDefinition.fromDict(d, material_lut)
        self.assertEqual(definition.batch_code, d["batch_code"])
        self.assertEqual(definition.material_id, material_id)
        self.assertEqual(definition.quantity, 1000000)
        self.assertEqual(definition.unit, Unit.milligram)
        self.assertEqual(definition.label, "articlenumber#")

    def testFromDictByTypeAndColorCaseInsensitive(self):
        material_id = uuid.uuid4()  # Some random material ID, that belongs to "pimpelpaars PLA"
        material_lut = {("pla", "pimpelpaars"):material_id}  # The LUT is all lowercase

        d = {"batch_code": "3847596285065067346803465863806",
             "material_id": "",  # The ID is not filled, will be inferred from the type and color, via the lut aboves
             "quantity": "1",
             "unit":"kilo",
             "label":"articlenumber#",
             "material_type":"PLA",
             "material_color":"pImPeLpAaRs"}  # The entry in the CSV can be any case, it will be lowercased

        definition = BatchDefinition.fromDict(d, material_lut)
        self.assertEqual(definition.batch_code, d["batch_code"])
        self.assertEqual(definition.material_id, material_id)
        self.assertEqual(definition.quantity, 1000000)
        self.assertEqual(definition.unit, Unit.milligram)
        self.assertEqual(definition.label, "articlenumber#")

    def testLoadFromCsv(self):
        csv_file = io.StringIO(example_batch_file)

        definitions = BatchDefinition.fromCsv(csv_file)

        self.assertEqual(len(definitions), 43)

    def testLoadFromCsvWithLabels(self):
        csv_file = io.StringIO(example_batch_file_with_labels)

        definitions = BatchDefinition.fromCsv(csv_file)

        self.assertEqual(len(definitions), 43)

        assert all(b.label != None for b in definitions)

    def testAddToAndLoadFromEmptyCsv(self):
        orig_definition = BatchDefinition(batch_code="Random batch code",
                                     label="Testerlabel",
                                     material_id=uuid.uuid4(),
                                     quantity=750000,
                                     unit=Unit.milligram)

        csv_file = io.StringIO("")

        BatchDefinition.addToCsv(csv_file, orig_definition)

        loaded_definitions = BatchDefinition.fromCsv(csv_file)

        self.assertIn(orig_definition, loaded_definitions)

    def testAddToAndLoadFromExistingCsv(self):

        mat_id = uuid.uuid4()
        orig_definition = BatchDefinition(batch_code="Random batch code",
                                     label="Testerlabel",
                                     material_id=mat_id,
                                     quantity=750000,
                                     unit=Unit.milligram)

        csv_file = io.StringIO("""batch_code,material_id,quantity,unit,label
1123456789,2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49,750,g,label9
987654321f,a8955dc3-9d7e-404d-8c03-0fd6fee7f22d,2000,ml,labeld""")

        original_lines = csv_file.readlines()
        self.assertEqual(len(original_lines), 3)
        csv_file.seek(0)

        original_definitions = BatchDefinition.fromCsv(csv_file)

        BatchDefinition.addToCsv(csv_file, orig_definition)

        loaded_definitions = BatchDefinition.fromCsv(csv_file)

        self.assertIn(original_definitions[0], loaded_definitions)
        self.assertIn(original_definitions[1], loaded_definitions)
        self.assertIn(orig_definition, loaded_definitions)

        csv_file.seek(0)
        new_lines = csv_file.readlines()
        self.assertEqual(len(new_lines), 4)

        self.assertEqual(new_lines[-1].strip(),
                         "Random batch code,{mat},750000,milligram,Testerlabel".format(mat=str(mat_id).strip()))

    def testLoadFromExistingCsvWithTypeAndColor(self):
        mat_id_0 = uuid.UUID('a27d014a-b5c3-4cd6-b029-0d4901fea571')
        mat_id_1 = uuid.UUID('bf514a62-bb87-44ed-9448-8b16b17db1b5')
        mat_id_2 = uuid.UUID('c5f143c7-d4b9-44d5-b66b-2361db8ba1ac')
        mat_id_3 = uuid.UUID('d40ededc-5034-429e-88a8-e6a3ac27a34c')
        mat_id_4 = uuid.UUID('e526b1c1-5044-4663-8920-d36662855c6d')

        material_lut = {("pp", "natural"):  mat_id_0,
                        ("pla", "red"):     mat_id_1,
                        ("tpu 95a", "red"): mat_id_2,
                        ("abs", "blue"):    mat_id_3,
                        ("pva", "natural"): mat_id_4}

        csv_file = io.StringIO(example_batch_file_with_labels_without_ids)
        # csv_file = open("/home/lvanbeek/taranis_src/taranis/taranis/common/data/test/example_batch_definitions_with_labels_without_ids.csv")#

        # break /usr/lib/python3.5/csv.py:183
        # The csv_file only has the color and type of the material and looks up the corresponding uuid via the material_lut
        loaded_definitions = BatchDefinition.fromCsv(csv_file, material_lut)

        self.assertEqual(loaded_definitions[0].material_id, uuid.UUID('a27d014a-b5c3-4cd6-b029-0d4901fea571'))
        self.assertEqual(loaded_definitions[1].material_id, uuid.UUID('bf514a62-bb87-44ed-9448-8b16b17db1b5'))
        self.assertEqual(loaded_definitions[2].material_id, uuid.UUID('c5f143c7-d4b9-44d5-b66b-2361db8ba1ac'))
        self.assertEqual(loaded_definitions[3].material_id, uuid.UUID('d40ededc-5034-429e-88a8-e6a3ac27a34c'))
        self.assertEqual(loaded_definitions[4].material_id, uuid.UUID('e526b1c1-5044-4663-8920-d36662855c6d'))

    # def testLoadFromRealCsvWithTypeAndColor(self):
    #     mat_id_0 = uuid.uuid4()
    #
    #     material_lut = {("tpu 95a", "white"): mat_id_0}
    #     csv_file = open("/home/lvanbeek/taranis-private-data/16/station_public/batch_definitions.csv")
    #
    #     import ipdb; ipdb.set_trace()
    #     # break /usr/lib/python3.5/csv.py:183
    #     # The csv_file only has the color and type of the material and looks up the corresponding uuid via the material_lut
    #     loaded_definitions = BatchDefinition.fromCsv(csv_file, material_lut)
    #
    #     self.assertEqual(loaded_definitions[0].material_id, mat_id_0)

    ## This test is commented out because it modifies a file in the same repository
    # def testAddToAndLoadFromExistingCsv2(self):
    #
    #     mat_id = uuid.uuid4()
    #     orig_definition = BatchDefinition(batch_code="Random batch code",
    #                                  label="Testerlabel",
    #                                  material_id=mat_id,
    #                                  quantity=750000,
    #                                  unit=Unit.milligram)
    #     here = os.path.abspath(os.path.dirname(__file__))
    #     csv_path = os.path.join(here, "example_batch_definitions_with_labels.csv")
    #     csv_file = open(csv_path, "r+")
    #
    #     original_lines = csv_file.readlines()
    #     original_len = len(original_lines)
    #     csv_file.seek(0)
    #
    #     original_definitions = BatchDefinition.fromCsv(csv_file)
    #
    #     BatchDefinition.addToCsv(csv_file, orig_definition)
    #
    #     loaded_definitions = BatchDefinition.fromCsv(csv_file)
    #
    #     self.assertIn(original_definitions[0], loaded_definitions)
    #     self.assertIn(original_definitions[1], loaded_definitions)
    #     self.assertIn(orig_definition, loaded_definitions)
    #
    #     csv_file.seek(0)
    #     new_lines = csv_file.readlines()
    #     self.assertEqual(len(new_lines), original_len+1)
    #
    #     self.assertEqual(new_lines[-1].strip(),
    #                      "Random batch code,{mat},750000,milligram,Testerlabel".format(mat=str(mat_id).strip()))
