# Definition of the MaterialStatistics class

import datetime

from .mixins import CommonEqualityMixin
from .unit import Unit

## @brief Statistical data on the stats on a spool
class MaterialStatistics(CommonEqualityMixin):

    ## Statistical data on the stats on a spool
    #   @param unit: In what units is the total and remain material quantity represented?
    #   @type unit Unit
    #   @param total: Quantity of the material originally put on the spool during production.
    #   @type total int
    #   @param remaining: Quantity of the material currently stored on the spool.
    #   @type remaining int
    #   @param total_usage_duration: Total time this spool has been used
    #   @type total_usage_duration datetime.timedelta
    def __init__(self, unit, total, remaining, total_usage_duration=datetime.timedelta(0)):
        self.unit = unit
        self.total = total
        self.remaining = remaining
        self.total_usage_duration = total_usage_duration

    def __repr__(self):
        return "MaterialStatistics({unit}, {total}, {remaining}, {ts})".format(
            unit=self.unit,
            total=self.total,
            remaining=self.remaining,
            ts=self.total_usage_duration)