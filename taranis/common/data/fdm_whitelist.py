"""
An FDM Whitelist is the list of materials that a  programming station is permitted to create tags for.

This permission is enforced through public-key/asymmetric cryptography:

The whitelist is distributed to Taranis programming stations as an encrypted CSV file.
The whole file is encrypted by Ultimaker"s private key, to be decrypted only by the corresponding public key that all the programming stations have.
A row, consisting of several cells, contains the information that grants the permission for a material.
If the cells of a row cannot be decrypted, there is no permission for that material.

Each cell in the row is encrypted by the public key of the manufacturer or programming station.
There are 3 columns (so each row has 3 cells): material_ID, checksum, time_limit.
- material_ID: the UUID that is programmed to the tags
- hash: when decrypting material_ID and hash with the wrong key,
            it may not be apparent that one used the wrong key (i.e. is not permitted for this material).
            Only when the decrypted hash and decrypted material_ID match,
            then you can be sure the correct decryption key was used and you are permitted to use the material_ID up to the time limit.
- time_limit: The permission only holds up to the time limit.
"""
import datetime
import hashlib
import io
import logging
import uuid

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from taranis.common.serialization.datetime_conversions import DateTimeConversions

log = logging.getLogger("fdm_whitelist")


class MaterialNotPermittedException(Exception):
    def __init__(self, material_id=None, time_limit=None, message=None, inner=None):
        self.material_id = material_id
        self.time_limit = time_limit
        self.message = message
        self.inner = inner
        super(MaterialNotPermittedException, self).__init__(material_id, time_limit, message, inner)


## @brief A material ID that is permitted for tagging until the time_limit
class MaterialPermission(object):
    ## Instantiate a material permission
    #   @param material_id: The material ID that is permitted for tagging
    #   @type material_id UUID
    #   @param time_limit: The permission is not valid after this time
    #   @type time_limit datetime.datetime

    _field_length = 256  # This length is due to the padding that is applied
    total_length = _field_length * 3 + 2  # 3 equal length fields, 2 commas in between

    _pad = padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()),
                        algorithm=hashes.SHA1(),
                        label=None)

    def __init__(self, material_id, time_limit, station_public_key=None):
        self.material_id = material_id
        self.time_limit = time_limit

        self.station_public_key = station_public_key

    ## @brief Encrypt the MaterialPermission so that only the station permitted for that material can decrypt it
    def encrypt(self):
        hash_ = hashlib.sha256(self.material_id.bytes).digest()
        plain_cells = [self.material_id.bytes, hash_, DateTimeConversions.datetimeToBytes(self.time_limit)]
        cypher_cells = [self.station_public_key.encrypt(cell, self._pad) for cell in plain_cells]

        assert all(len(cell) == self._field_length for cell in cypher_cells)

        cypher_row = b",".join(cypher_cells)

        return cypher_row

    ## @brief Attempt to decrypt a row given a private key
    @staticmethod
    def decrypt(row_bytes, station_private_key):
        message_stream = io.BytesIO(row_bytes)
        uuid1_cypher = message_stream.read(MaterialPermission._field_length)
        _ = message_stream.read(1)  # Read separator (comma or newline)
        hash1_cypher = message_stream.read(MaterialPermission._field_length)
        _ = message_stream.read(1)  # Read separator (comma or newline)
        ts__1_cypher = message_stream.read(MaterialPermission._field_length)

        try:
            uuid_plain = station_private_key.decrypt(uuid1_cypher, MaterialPermission._pad)
            hash_plain = station_private_key.decrypt(hash1_cypher, MaterialPermission._pad)
            time_plain = station_private_key.decrypt(ts__1_cypher, MaterialPermission._pad)

            assert hashlib.sha256(uuid_plain).digest() == hash_plain
            material_id = uuid.UUID(bytes=uuid_plain)
            time_limit = DateTimeConversions.datetimeFromBytes(time_plain)
            return MaterialPermission(material_id=material_id, time_limit=time_limit)
        except (ValueError, AssertionError) as error:
            raise MaterialNotPermittedException(material_id=None,
                                                message="Material is not permitted when using this key", inner=error)

    ## @brief Check whether the permission is still valid given the current time
    def isPermitted(self, current_time=None):
        if not current_time:
            current_time = datetime.datetime.now()
        return current_time < self.time_limit

    def __eq__(self, other):
        if isinstance(other, MaterialPermission):
            return self.material_id == other.material_id and self.time_limit == other.time_limit
        else:
            return False

    def __repr__(self):
        return "MaterialPermission({uuid}, {limit})".format(uuid=self.material_id, limit=self.time_limit)


##
class FdmWhitelist(object):
    def __init__(self, permitted_materials=None):
        if permitted_materials:
            assert all(isinstance(permission, MaterialPermission) for permission in permitted_materials)

        self.permitted_materials = permitted_materials

    def encryptAndSign(self, ultimaker_private_key, whitelist_file, signature_file):
        whitelist = b"\n".join([permission.encrypt() for permission in self.permitted_materials])
        signature = ultimaker_private_key.sign(whitelist,
                                               padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
                                                           salt_length=padding.PSS.MAX_LENGTH),
                                               hashes.SHA256()
                                               )

        whitelist_file.write(whitelist)
        signature_file.write(signature)

    @staticmethod
    def fromEncryptedFile(whitelist_file, signature_file, ultimaker_public_key, station_private_key, current_time=None):
        if not current_time:
            current_time = datetime.datetime.now()

        message = whitelist_file.read()
        signature = signature_file.read()

        permitted_materials = []

        try:
            # ultimaker_public_key.verify(signature,
            #                             message,
            #                             padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
            #                                         salt_length=padding.PSS.MAX_LENGTH),
            #                             hashes.SHA256())

            message_stream = io.BytesIO(message)
            while message_stream.readable():
                encrypted = message_stream.read(MaterialPermission.total_length)
                if encrypted:
                    try:
                        permission = MaterialPermission.decrypt(encrypted, station_private_key)
                        if permission.isPermitted(current_time):
                            permitted_materials += [permission]
                        else:
                            log.info("Permission for material {mat} is past its time_limit of {limit}".format(
                                mat=permission.material_id,
                                limit=permission.time_limit))
                    except MaterialNotPermittedException as not_permitted_exception:
                        #log.debug("Permission could for a material could not be correctly decrypted: {ex}".format(
                        #    ex=not_permitted_exception))
                        pass
                    message_stream.read(1)  # Read newline
                else:
                    break
        except InvalidSignature as invalid_signature:
            log.error("The signature on the permissions file is invalid: {ex}".format(ex=invalid_signature))
        finally:
            return FdmWhitelist(permitted_materials)

    def isPermitted(self, material_id):
        permitted = [permission.material_id for permission in self.permitted_materials]
        return material_id in permitted

    def __eq__(self, other):
        if isinstance(other, FdmWhitelist):
            return self.permitted_materials == other.permitted_materials
        else:
            return False
