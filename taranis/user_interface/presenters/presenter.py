## A presenter is the bridge between frontend Views and the backend Models
# It contains the logic on how a View modifies the Model and how changes in a Model are showed to the user.
# The actual graphical part of this is handled by the View.
# A Presenter must be independent of the View implementation, as long as each View adheres to their common interface.


class Presenter(object):
    def __init__(self):
        self.view = None

    ## Which view to show this presenter with?
    # @param view a View-subclass that matches this Presenter
    def setView(self, view):
        self.view = view