from .presenter import Presenter

from taranis.services.configuration.tracing import TracingConfiguration
from taranis.user_interface.configuration.ui_config import UiConfig
import shutil
import os

import logging
log = logging.getLogger(__name__.split(".")[-1])


class LogExportPresenter(Presenter):
    def __init__(self, application):
        super(LogExportPresenter, self).__init__()

        self.application = application

        self.destination = os.path.expandvars(os.path.expanduser(UiConfig.logExportDestination))

    def onDestinationSelected(self, destination):
        self.destination = destination

        self.view.showSelectedDestination(self.destination)

    def onExportClicked(self):
        self.view.indicateUpdateStarted()

        try:
            self.application.filament_service.exportTraceabilityLog(self.destination)
            self.view.indicateUpdateSuccessful()
        except Exception as ex:
            log.exception(ex)
            self.view.indicateUpdateFailed()

    def onBackClicked(self):
        self.application.startAdministration()

    def activate(self):
        self.view.showSelectedDestination(self.destination)

