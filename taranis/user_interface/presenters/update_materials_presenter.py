from .presenter import Presenter
from taranis.errors import showError

import logging
log = logging.getLogger(__name__.split(".")[-1])


class UpdateMaterialsPresenter(Presenter):
    def __init__(self, application):
        super(UpdateMaterialsPresenter, self).__init__()

        self.application = application

    def onUpdateClicked(self):
        log.debug("UpdateMaterialsPresenter.onUpdateClicked")
        self.view.indicateUpdateStarted()
        try:
            success = self.application.whitelist.updateMaterialPermissions()
        except Exception as ex:
            log.exception(ex)
            success = False

        try:
            # Load the whitelist when opening this screen. The whitelist should be reloaded in to before a next screen is active.
            # We assume that the call is short enough for that
            success &= self.application.whitelist.loadWhitelist()
        except Exception as ex:
            log.exception(ex)
            success = False

        if success:
            self.view.indicateUpdateSuccessful()
        else:
            self.view.indicateUpdateFailed()

    def onBackClicked(self):
        self.application.startAdministration()

