from griffin import dbusif

from .presenter import Presenter
from taranis.errors import showError

import logging
log = logging.getLogger(__name__.split(".")[-1])


class UpdateSystemPresenter(Presenter):
    def __init__(self, application):
        super(UpdateSystemPresenter, self).__init__()

        self.application = application

        self.system = None

    def onUpdateClicked(self, version):
        self.view.indicateUpdateStarted()
        log.debug("onUpdateClicked")

        self.system = dbusif.RemoteObject("system")

        try:
            self.system.startUpdate(version)
        except Exception as ex:
            log.exception(ex)

        self.view.startRefreshTimer()

    def onFromUsbClicked(self, update_file):
        signature_file = update_file+".sig"

        self.view.indicateUpdateStarted()
        log.debug("onFromUsbClicked")

        self.system = dbusif.RemoteObject("system")

        try:
            self.system.startLocalUpdate(update_file, signature_file)
        except Exception as ex:
            log.exception(ex)

        self.view.startRefreshTimer()

    def onBackClicked(self):
        self.application.startAdministration()

    def getUpdateState(self):
        if self.system:
            state = self.system.getUpdateState()
            log.debug("UpdateSystemPresenter.getUpdateState: {stat}".format(stat=state))
            return state
        else:
            return "IDLE", 0

    def activate(self):
        try:
            self.system = dbusif.RemoteObject("system")
            if self.system.isDeveloperModeActive():
                self.view.showIpAddresses()
        except Exception as ex:
            log.exception("Cannot check if developer mode is active")

        try:
            with open("/etc/taranis_version", "r") as version_file:
                version = version_file.read().strip()
                self.view.setVersion(version)
        except:
            self.view.setVersion(_("Unknown version"))