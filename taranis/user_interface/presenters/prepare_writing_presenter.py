from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])

class PrepareWritingPresenter(Presenter):
    def __init__(self, application):
        super(PrepareWritingPresenter, self).__init__()

        self.application = application

        self.continue_enabled = False

    def onContinueWithCurrentClicked(self):
        log.debug("onContinueWithCurrentClicked")
        self.application.startWriteTags()

    def onSelectFromListClicked(self):
        log.debug("onSelectFromListClicked")
        self.application.startSelectDefinedBatch()

    def onBackClicked(self):
        self.application.backToStart()

    def activate(self):
        self.view.setContinueEnabled(self.continue_enabled)
        if self.continue_enabled:
            if not self.application.current_batch.material_name:
                self.application.current_batch.material_name = self.application.describeMaterial(self.application.current_batch.material_id)
            self.view.setCurrentBatchDescription(str(self.application.current_batch))
        else:
            self.view.setCurrentBatchDescription("undefined batch")


