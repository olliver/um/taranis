from .presenter import Presenter


class StartPresenter(Presenter):
    def __init__(self, application):
        super(StartPresenter, self).__init__()

        self.application = application

    def onWriteTagsClicked(self):
        self.application.prepareWriteTags()

    def onVerifyTagsClicked(self):
        self.application.startVerifyTags()

    def onAdministrationClicked(self):
        self.application.startEnterPin()

    def onSpoolDetected(self, spool_id, has_content):
        self.view.indicateSpoolPresence(True)

    def onSpoolRemoved(self):
        self.view.indicateSpoolPresence(False)

    def onEraseClicked(self):
        self.application.startErasing()
