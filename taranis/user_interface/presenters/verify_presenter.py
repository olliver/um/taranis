import datetime
from taranis.common.stopwatch import stopwatch

from .presenter import Presenter
from taranis.errors import showError

import logging
log = logging.getLogger(__name__.split(".")[-1])

## @brief Presenter to verify tags
# Tags are read when they are detected. The associated view is used to indicate the content of the tag  to the user
class VerifyPresenter(Presenter):
    def __init__(self, application):
        super(VerifyPresenter, self).__init__()

        self.application = application

    ## @brief Use the view to present a summary of the spool
    def onSpoolRead(self, summary):
        self.view.setMaterialName(summary["name"])
        self.view.setTotalAmount(summary["total"])
        self.view.setRemainingAmount(summary["remaining"])
        self.view.setUnitForAmount(summary["unit"])
        self.view.setBatchCode(summary["batchcode"])
        self.view.setTimestamp(self.application.timestamp_to_string(summary["manufacturing_timestamp"]))

        if summary["signing"]:
            self.view.indicateSpoolOk()
        else:
            self.view.indicateSpoolNotOK()

    def onStopClicked(self):
        self.application.stopVerifying()

    ## @brief Called when the backend detects a spool with a tag. This starts the reading
    # @param spool_id the ID of the spool
    # @param checks whether the spool has any content. If there is no content, there is no reading and an error indicated to the view
    def onSpoolDetected(self, spool_id, has_content):
        log.debug("onSpoolDetected(spool_id={id}, has_content={hc})".format(id=spool_id, hc=has_content))
        self.view.indicateBusyReading()
        if has_content:
            with stopwatch("gui_read_spool"):
                summary = self.readSpool(spool_id)
            self.onSpoolRead(summary)
        else:
            self.view.indicateSpoolHasNoData()

    def onSpoolRemoved(self):
        log.debug("onSpoolRemoved()")
        self.view.indicateWaitingForSpool()

    ## @brief Read the spool with the given ID and show its content using the view
    def readSpool(self, spool_id):
        timestamp, material_id, total, remaining, unit_, batch_code, crypto_ok, error_code = self.application.filament_service.getSpoolSummary(spool_id)
        manufacturing_timestamp = datetime.datetime.fromtimestamp(timestamp)

        log.debug("Reading finished")
        if error_code != 0:
            showError(error_code, log)

        total_amount_desc = self.application.describeAmount(total, unit_)
        remaining_amount_desc = self.application.describeAmount(remaining, unit_)
        material_desc = self.application.describeMaterial(material_id)

        log.info("Spool {id} contains:\n - total: {amount}\n - remaining: {remaining}\n - material: {desc}\n - batch: {batch}\n - timestamp: {ts}".format(
                id=spool_id,
                amount=total_amount_desc,
                remaining=remaining_amount_desc,
                desc=material_desc,
                batch=batch_code,
                ts=manufacturing_timestamp))

        summary = {"manufacturing_timestamp":manufacturing_timestamp, "name":material_desc, "total":total_amount_desc, "remaining":remaining_amount_desc, "unit":"", "batchcode":batch_code, "signing":crypto_ok}

        return summary

    def activate(self):
        log.debug("VerifyPresenter.activate()")
        if self.application.current_spool:
            log.debug("VerifyPresenter.activate: self.application.current_spool = {id_hc}".format(id_hc=self.application.current_spool))
            self.onSpoolDetected(*self.application.current_spool)
