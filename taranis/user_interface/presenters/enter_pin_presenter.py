from taranis.user_interface.configuration.ui_config import UiConfig

from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])


class EnterPinPresenter(Presenter):
    def __init__(self, application):
        super(EnterPinPresenter, self).__init__()

        self.application = application

    def onOkClicked(self, pin_txt):
        entered_pin = int(pin_txt)

        with open(UiConfig.getPinFilePath(), 'r') as pin_file:
            obfuscated_correct_pin = pin_file.read().strip()
            obfuscated_correct_pin = int(obfuscated_correct_pin)

            # The correct PIN is the displayed number + UiConfig.getPinObfuscationConstant()
            # So to get the correct PIN from the file, subtract the UiConfig.getPinObfuscationConstant()
            correct_pin = obfuscated_correct_pin - UiConfig.getPinObfuscationConstant()

            # log.debug("Entered_pin: {e}, correct_pin: {c}".format(e=entered_pin, c=correct_pin))

            if entered_pin == correct_pin:
                self.view.clear()
                self.application.startAdministration()
            else:
                self.view.clear()
        pass

    def onBackClicked(self):
        self.view.clear()
        self.application.backToStart()