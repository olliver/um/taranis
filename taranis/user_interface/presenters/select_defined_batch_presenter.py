import logging

from taranis.user_interface.configuration.ui_config import UiConfig

from .presenter import Presenter

from taranis.common.data.batch_definition import BatchDefinition
import uuid


log = logging.getLogger(__name__.split(".")[-1])

class SelectDefinedBatchPresenter(Presenter):
    def __init__(self, application):
        super(SelectDefinedBatchPresenter, self).__init__()

        self.application = application

        self.batches = []

    def loadBatches(self):
        # TODO EM-1489: Gather a lookup table that maps (material_type, material_color) to its UUID

        materials_db = self.application.material_service.getMaterials()
        materials_lut = {(mat["material"].lower(), mat["color"].lower()):uuid.UUID(mat["guid"]) for mat in materials_db}

        with open(UiConfig.batchDefinitionsPath) as csv_file:
            unsorted_batches = BatchDefinition.fromCsv(csv_file, materials_lut)

            allowed_batches = [batch for batch in unsorted_batches if self.application.whitelist.isPermitted(str(batch.material_id))]

            log.debug("{unsorted} unsorted_batches, {allowed} allowed_batches".format(unsorted=len(unsorted_batches), allowed=len(allowed_batches)))

            for batch in allowed_batches:
                if not batch.material_name:
                    batch.material_name = self.application.describeMaterial(batch.material_id)

            self.batches = sorted(allowed_batches, key=lambda x: str(x))

            self.view.showItems(self.batches)

    def onItemIndexSelected(self, index):
        return self.onItemSelected(self.batches[index])

    def onItemSelected(self, selected_batch):
        self.application.current_batch = selected_batch

        self.application.startWriteTags()

    def onBackClicked(self):
        self.application.prepareWriteTags()

    def activate(self):
        self.loadBatches()