from taranis.common.serialization.material_serializer import MAX_BATCHCODE_LEN
from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])

class EnterBatchCodePresenter(Presenter):
    def __init__(self, application):
        super(EnterBatchCodePresenter, self).__init__()

        self.application = application

        self.partial_batch_definition = None

    def onOkClicked(self, batch_code):
        if self.isValid(batch_code):
            log.info("Batch code '{code}'' entered".format(code=batch_code))
            self.application.startEnterLabel(batch_code, *self.partial_batch_definition)
        else:
            log.info("Batch code '{code}' is not valid".format(code=batch_code))
            self.view.clear()

    def onBackClicked(self):
        self.application.startSelectQuantity(self.application.current_batch.material_id)

    def isValid(self, batch_code):
        assert isinstance(batch_code, str)
        return len(bytes(batch_code, encoding="utf-8")) <= MAX_BATCHCODE_LEN