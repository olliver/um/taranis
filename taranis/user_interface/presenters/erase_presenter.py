from taranis.common.data.unit import Unit
from taranis.errors import showError
from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])


class ErasePresenter(Presenter):
    def __init__(self, application):
        super(ErasePresenter, self).__init__()

        self.application = application


    def onStopClicked(self):
        self.application.stopErasing()

    def onSpoolErased(self, success):
        if success:
            self.view.setStatus("Spool erased OK")
            self.view.indicateSpoolOk()
        else:
            self.view.setStatus("Spool error")
            self.view.indicateSpoolNotOK()

    def onSpoolDetected(self, spool_id, has_content):
        log.debug("onSpoolDetected(spool_id={id}, has_content={hc})".format(id=spool_id, hc=has_content))
        self.view.indicateBusyWriting()
        success = self.eraseSpool(spool_id)
        if success:
            self.onSpoolErased(success)
        else:
            self.view.indicateSpoolHasData()

    def onSpoolRemoved(self):
        log.debug("onSpoolRemoved()")
        self.view.indicateWaitingForSpool()

    def eraseSpool(self, spool_id):
        log.debug("Erasing {id}".format(id=spool_id))
        success= self.application.filament_service.eraseSpool(spool_id)

        # if not success:
        #     showError(error, log)

        return success

    def activate(self):
        log.debug("ErasePresenter.activate()")
        if self.application.current_spool:
            log.debug("ErasePresenter.activate: self.application.current_spool = {id_hc}".format(id_hc=self.application.current_spool))
            self.onSpoolDetected(*self.application.current_spool)
