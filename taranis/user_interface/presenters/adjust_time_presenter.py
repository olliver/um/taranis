from .presenter import Presenter

import dbus
from dbus import Interface
from griffin import dbusif

import logging
log = logging.getLogger(__name__.split(".")[-1])

## @brief Adjust the system time to a given datetime in some time zone
class AdjustTimePresenter(Presenter):
    def __init__(self, application):
        super(AdjustTimePresenter, self).__init__()

        self.application = application

        self.adjusted_datetime = None
        self.adjusted_timezone = None
        self.ntp_sync = None

    ## @brief Confirm the change and adjust the system clocks
    def onAdjustClicked(self):
        log.info("Adjusting time & zone to {dt} & {tz}".format(dt=self.adjusted_datetime, tz=self.adjusted_timezone))

        self.view.setErrorMessage(message="")

        errors = ""

        timedate1 = dbusif.RemoteObject(name="org.freedesktop.timedate1",
                                        path="/org/freedesktop/timedate1",
                                        interface="org.freedesktop.timedate1")

        try:
            if self.ntp_sync != None:
                log.debug("Call timedate1.SetNTP({value})".format(value=self.ntp_sync))
                timedate1.SetNTP(self.ntp_sync, False) # False = do not require user interaction
                log.debug("Finished successful timedate1.SetNTP({value})".format(value=self.ntp_sync))

                self.ntp_sync = None  # So we don't go back to this setting by default next time this is used
        except dbus.exceptions.DBusException as dbe:
            log.exception(dbe)
            errors += dbe.get_dbus_message()

        try:
            if self.adjusted_datetime:
                stamp = int((self.adjusted_datetime.timestamp() * 1000000) + self.adjusted_datetime.microsecond)
                log.debug("Call timedated.SetTime({stamp})".format(stamp=stamp))
                timedate1.SetTime(stamp, False, False)  # False, False = not a relative time, do not require user interaction

                log.debug("Finished successful: timedate1.SetTime({stamp})".format(stamp=stamp))
                self.adjusted_datetime = None  # So we don't go back to this setting by default next time this is used
        except dbus.exceptions.DBusException as dbe:
            log.exception(dbe)
            errors += dbe.get_dbus_message()

        try:
            if self.adjusted_timezone:
                log.debug("Call timedate1.SetTimezone({tz})".format(tz=self.adjusted_timezone))
                timedate1.SetTimezone(self.adjusted_timezone, False)  # False = do not require user interaction
                log.debug("Finished successful timedate1.SetTimezone({tz})".format(tz=self.adjusted_timezone))

                self.adjusted_timezone = None  # So we don't go back to this setting by default next time this is used
        except dbus.exceptions.DBusException as dbe:
            log.exception(dbe)
            errors += dbe.get_dbus_message()

        if errors:
            self.view.setErrorMessage(errors)
        else:
            self.application.backToStart()

    def onEnableSyncChanged(self, new_value):
        log.info("onEnableSyncChanged({value})".format(value=new_value))
        self.ntp_sync = new_value
        self.view.setEnableSync(self.ntp_sync)

    ## @brief Go back to previous screen
    def onBackClicked(self):
        self.adjusted_datetime, self.adjusted_timezone = None, None  # So we don't go back to the previous setting

        self.application.backToStart()

    ## @brief Remember the selected datetime
    def onDatetimeSelected(self, datetime):
        log.info("onDatetimeSelected(datetime={dt})".format(dt=datetime))
        self.adjusted_datetime = datetime

    ## @brief Remember the selected time zone
    # @param timezone_name must be part of the Olson database
    def onTimezoneSelected(self, timezone_name):
        log.info("onTimezoneSelected(timezone_name={tz})".format(tz=timezone_name))
        self.adjusted_timezone = timezone_name

    def activate(self):
        self.view.setErrorMessage("")

        # Dealing with DBus properties is inspired from https://zignar.net/2014/09/08/getting-started-with-dbus-python-systemd/
        name = 'org.freedesktop.timedate1'
        path = '/org/freedesktop/timedate1'
        iTimedated = 'org.freedesktop.timedate1'
        iProperty = 'org.freedesktop.DBus.Properties'

        timedated_obj = dbusif._bus.get_object(name, path)
        # timedated_timedated = Interface(timedated_obj, dbus_interface=iTimedated)  # This the same thing we already have in onAdjustClicked
        timedated_Properties = Interface(timedated_obj, dbus_interface=iProperty)  # But we can use this for properties

        using_ntp = bool(timedated_Properties.Get(iTimedated, "NTP"))

        self.view.setEnableSync(using_ntp)