from .presenter import Presenter
from collections import OrderedDict

import logging
log = logging.getLogger(__name__.split(".")[-1])

class SelectMaterialPresenter(Presenter):
    def __init__(self, application):
        super(SelectMaterialPresenter, self).__init__()

        self.application = application

        self.materials = []
        self.material_descriptions = {}

    def loadMaterials(self):
        self.materials = self.application.whitelist.getPermittedMaterials()

        #Map descriptions to material UUIDs
        self.material_descriptions = OrderedDict([(self.application.describeMaterial(permitted), permitted) for permitted in self.materials])

        self.view.showItems(self.material_descriptions.keys())

    def onItemIndexSelected(self, index):
        selected_desc = list(self.material_descriptions.keys())[index]
        return self.onItemSelected(selected_desc)

    def onItemSelected(self, selected_material_description):
        selected_material = self.material_descriptions[selected_material_description]
        log.info("Material {desc} is selected, which has UUID {id}".format(desc=selected_material_description,
                                                                           id=selected_material))
        self.application.startSelectQuantity(selected_material)

    def onBackClicked(self):
        self.application.prepareWriteTags()