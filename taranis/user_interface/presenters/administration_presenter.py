from griffin import dbusif

from .presenter import Presenter

import os

import logging
log = logging.getLogger(__name__.split(".")[-1])


class AdministrationPresenter(Presenter):
    def __init__(self, application):
        super(AdministrationPresenter, self).__init__()

        self.application = application

    def onBackClicked(self):
        self.application.backToStart()

    def onTimeClicked(self):
        self.application.startTimeChange()

    def onUpdateMaterialsClicked(self):
        self.application.startMaterialsUpdate()

    def onUpdateSystemClicked(self):
        self.application.startSystemUpdate()

    def onManuallyEnterClicked(self):
        log.debug("onManuallyEnterClicked")
        self.application.startSelectMaterial()

    def onLogExportClicked(self):
        self.application.startLogExport()

    def onDeveloperModeClicked(self):
        systemService = dbusif.RemoteObject(name="system")
        devmode_active = systemService.isDeveloperModeActive()
        new_mode = not devmode_active
        log.info("Developermode is current {act} active, new mode will be the inverse: {new}".format(act=devmode_active, new=new_mode))
        systemService.setDeveloperMode(new_mode)   # If it is already on, disable it.

        self.view.alertDeveloperModeActive(new_mode)
