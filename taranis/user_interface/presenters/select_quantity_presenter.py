from collections import OrderedDict

from .presenter import Presenter

from taranis.user_interface.configuration.ui_config import UiConfig


import logging
log = logging.getLogger(__name__.split(".")[-1])

class SelectQuantityPresenter(Presenter):
    def __init__(self, application):
        super(SelectQuantityPresenter, self).__init__()

        self.application = application

        self.selected_material = None

        self.quantities = UiConfig.selectableQuantities
        self.quantity_descriptions = {}

    def loadQuantities(self):
        self.quantity_descriptions = OrderedDict([(self.application.describeAmount(*quantity), quantity) for quantity in self.quantities])
        self.view.showItems(self.quantity_descriptions.keys())

    def onItemIndexSelected(self, index):
        selected_desc = list(self.quantity_descriptions.keys())[index]
        return self.onItemSelected(selected_desc)

    def onItemSelected(self, selected_quantity_description):
        selected_quantity = self.quantity_descriptions[selected_quantity_description]
        log.info("Quantity {desc} is selected: {selected}".format(desc=selected_quantity_description,
                                                                  selected=selected_quantity))
        self.application.startEnterBatch(self.selected_material, *selected_quantity)

    def onBackClicked(self):
        self.application.startSelectMaterial()