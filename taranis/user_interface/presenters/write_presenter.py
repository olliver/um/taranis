import datetime
from taranis.common.data.unit import Unit
from taranis.common.stopwatch import stopwatch
from taranis.errors import showError
from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])

## @brief Workhorse of this GUI: this waits for spools to be detected and then writes them
class WritePresenter(Presenter):
    def __init__(self, application):
        super(WritePresenter, self).__init__()

        self.application = application

    ## @brief Show a summary of the spool after writing, for verification whether the writing succeeded
    def onSpoolRead(self, summary):
        self.view.setMaterialName(summary["name"])
        self.view.setTotalAmount(summary["total"])
        self.view.setRemainingAmount(summary["remaining"])
        self.view.setUnitForAmount(summary["unit"])
        self.view.setBatchCode(summary["batchcode"])
        self.view.setTimestamp(self.application.timestamp_to_string(summary["manufacturing_timestamp"]))

        if summary["signing"] and summary["spool_has_same_content"]:
            self.view.indicateSpoolOk()
        else:
            self.view.indicateSpoolNotOK()

    def onStopClicked(self):
        self.application.stopWriting()

    ## @brief Show what was written to the spool and whether the write was successful
    def onSpoolWritten(self, success, summary):
        self.view.setMaterialName(summary["name"])
        self.view.setTotalAmount(summary["total"])
        self.view.setRemainingAmount(summary["remaining"])
        self.view.setUnitForAmount(summary["unit"])
        self.view.setBatchCode(summary["batchcode"])
        self.view.setTimestamp(self.application.timestamp_to_string(summary["manufacturing_timestamp"]))

        if success:
            self.view.setStatus("Spool written OK")
            self.view.indicateSpoolOk()
        else:
            self.view.setStatus("Spool error")
            self.view.indicateSpoolNotOK()

    ## @brief Called when the backend detects a spool with a tag. This starts the writing
    # @param spool_id the ID of the spool
    # @param checks whether the spool has any content. If there is some content, the writing won't start and an error indicated to the view
    def onSpoolDetected(self, spool_id, has_content):
        log.debug("onSpoolDetected(spool_id={id}, has_content={hc})".format(id=spool_id, hc=has_content))
        if not has_content:
            self.view.indicateBusyWriting()
            with stopwatch("gui_write_spool"):
                success, summary = self.writeSpool(spool_id)
                self.onSpoolWritten(success, summary)
        else:
            self.view.indicateBusyReading()
            with stopwatch("gui_read_spool"):
                summary = self.readSpool(spool_id)
                self.onSpoolRead(summary)

    def onSpoolRemoved(self):
        log.debug("onSpoolRemoved()")
        self.view.indicateWaitingForSpool()

    ## @brief Read a spool for verification purposes
    def readSpool(self, spool_id):
        timestamp, material_id, total, remaining, unit_, batch_code, crypto_ok, error_code = self.application.filament_service.getSpoolSummary(spool_id)

        manufacturing_timestamp = datetime.datetime.fromtimestamp(timestamp)
        # If we read a spool, at least check that it has the same content as what we should write
        # This allows to run a spool twice, as some companies do in their process.
        # The first time it is really written and then the second pass serves as a verification
        # If the first time did not occur (e.g. forgotten by the operator), then the spool is still programmed
        material_id_match = material_id == str(self.application.current_batch.material_id)
        total_match = total == self.application.current_batch.quantity
        remaining_match = remaining == self.application.current_batch.quantity
        batch_code_match = batch_code == self.application.current_batch.batch_code
        spool_has_same_content = material_id_match and total_match and remaining_match and batch_code_match
        log.info("spool_has_same_content={s}: material_id={m}, total={t}, remaining={r}, batch_code={b}".format(
            s=spool_has_same_content, m=material_id_match, t=total_match, r=remaining_match, b=batch_code_match))

        log.debug("Reading finished")
        if error_code != 0:
            showError(error_code, log)

        total_amount_desc = self.application.describeAmount(total, unit_)
        remaining_amount_desc = self.application.describeAmount(remaining, unit_)
        material_desc = self.application.describeMaterial(material_id)

        log.info("Spool {id} contains:\n - total: {amount}\n - remaining: {remaining}\n - material: {desc}\n - batch: {batch}\n - timestamp: {ts}".format(
                id=spool_id,
                amount=total_amount_desc,
                remaining=remaining_amount_desc,
                desc=material_desc,
                batch=batch_code,
                ts=manufacturing_timestamp))

        summary = {"spool_has_same_content":spool_has_same_content,
                   "manufacturing_timestamp":manufacturing_timestamp,
                   "name":material_desc,
                   "total":total_amount_desc,
                   "remaining":remaining_amount_desc,
                   "unit":"",
                   "batchcode":batch_code,
                   "signing":crypto_ok}

        return summary

    ## @brief Write the current BatchDefinition to the spool
    def writeSpool(self, spool_id):
        log.debug("Writing...")
        batch = self.application.current_batch
        log.debug("Writing {}, {}, {}, {}".format(batch.batch_code, batch.material_id, batch.quantity, batch.unit))
        try:
            success, timestamp, error = self.application.filament_service.initializeSpool(spool_id,
                                                                              str(batch.material_id),
                                                                              batch.quantity,
                                                                              Unit.tostring(batch.unit),
                                                                              batch.batch_code,
                                                                              verify=False)

            if not success:
                showError(error, log)

            total_amount_desc = self.application.describeAmount(batch.quantity, Unit.tostring(batch.unit))
            remaining_amount_desc = self.application.describeAmount(batch.quantity, Unit.tostring(batch.unit))
            material_desc = self.application.describeMaterial(batch.material_id)
            manufacturing_timestamp = datetime.datetime.fromtimestamp(timestamp)

            log.debug("Spool {id} contains:\n - total: {amount}\n - remaining: {remaining}\n - material: {desc}\n - batch: {batch}".format(
                    id=spool_id,
                    amount=total_amount_desc,
                    remaining=remaining_amount_desc,
                    desc=material_desc,
                    batch=batch.batch_code))

            summary = {"manufacturing_timestamp":manufacturing_timestamp, "name":material_desc, "total":total_amount_desc, "remaining":remaining_amount_desc, "unit":"", "batchcode":batch.batch_code}

            return success, summary
        except Exception as ex:
            log.exception(ex)

    def activate(self):
        log.debug("WritePresenter.activate()")
        if self.application.current_spool:
            log.debug("WritePresenter.activate: self.application.current_spool = {id_hc}".format(id_hc=self.application.current_spool))
            self.onSpoolDetected(*self.application.current_spool)
