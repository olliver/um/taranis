import os
import uuid
import json
import shutil

import datetime
from dbus import DBusException
from taranis.common.data.batch_definition import BatchDefinition
from taranis.common.data.unit import Unit
from taranis.errors import showError
from taranis.user_interface.configuration.ui_config import UiConfig

from .presenter import Presenter
from .start_presenter import StartPresenter
from .verify_presenter import VerifyPresenter
from .update_materials_presenter import UpdateMaterialsPresenter
from .update_system_presenter import UpdateSystemPresenter
from .adjust_time_presenter import AdjustTimePresenter
from .administration_presenter import AdministrationPresenter
from .prepare_writing_presenter import PrepareWritingPresenter
from .write_presenter import WritePresenter
from .select_defined_batch_presenter import SelectDefinedBatchPresenter
from .select_material_presenter import SelectMaterialPresenter
from .select_quantity_presenter import SelectQuantityPresenter
from .enter_batchcode_presenter import EnterBatchCodePresenter
from .erase_presenter import ErasePresenter
from .log_export_presenter import LogExportPresenter
from .enter_pin_presenter import EnterPinPresenter
from .enter_label_presenter import EnterLabelPresenter

from griffin import dbusif

import logging
log = logging.getLogger(__name__.split(".")[-1])

## @brief Main application logic for the Taranis GUI
# It is composed of several sub-presenters, each of which has a corresponding view
# This class handles the navigation logic, the subscription to DBus signals and overall state
class ApplicationPresenter(Presenter):
    def __init__(self):
        super(ApplicationPresenter, self).__init__()
        self.start = StartPresenter(self)
        self.prepare_writing = PrepareWritingPresenter(self)
        self.write = WritePresenter(self)
        self.verify = VerifyPresenter(self)
        self.update_materials = UpdateMaterialsPresenter(self)
        self.update_system = UpdateSystemPresenter(self)
        self.adjust_time = AdjustTimePresenter(self)
        self.administration = AdministrationPresenter(self)
        self.select_defined_batch = SelectDefinedBatchPresenter(self)
        self.select_material = SelectMaterialPresenter(self)
        self.select_quantity = SelectQuantityPresenter(self)
        self.enter_batch_code = EnterBatchCodePresenter(self)
        self.enter_label = EnterLabelPresenter(self)
        self.erase = ErasePresenter(self)
        self.log_export = LogExportPresenter(self)
        self.enter_pin = EnterPinPresenter(self)

        self.whitelist = dbusif.RemoteObject("material_permission")
        self.filament_service = dbusif.RemoteObject("filament_spool_interfacing")
        self.material_service = dbusif.RemoteObject(name="material")

        # See https://www.freedesktop.org/wiki/Software/systemd/timedated/ for this interface
        self.timedated = dbusif.RemoteObject(name="org.freedesktop.timedate1",
                                             path="/org/freedesktop/timedate1",
                                             interface="org.freedesktop.timedate1")

        self._current_batch = None
        self._current_spool = None

        self.attachOwnSignals()

    ## @brief Which BatchDefinition are we currently writing or constructing
    @property
    def current_batch(self):
        if not self._current_batch:
            try:
                with open(".current_batch.json", "r") as current_batch_file:
                    definition = json.load(current_batch_file)
                    self._current_batch = BatchDefinition.fromDict(definition)
            except Exception as ex:
                log.exception(ex)
        return self._current_batch

    @property
    def current_spool(self):
        return self._current_spool

    @current_batch.setter
    def current_batch(self, value):
        self._current_batch = value

        try:
            with open(".current_batch.json", "w") as current_batch_file:
                json.dump(value.toDict(), current_batch_file)
        except Exception as ex:
            log.exception(ex)

    def startTimeChange(self):
        self.adjust_time.activate()
        self.view.showAdjustTime()

    ## @brief Show the screen to prepare writing.
    # Sets the whether the continue button is enabled
    def prepareWriteTags(self):
        # self.filament_service.connectSignal("spoolDetected", self.write.onSpoolDetected)
        self.prepare_writing.continue_enabled = self.current_batch != None
        self.prepare_writing.activate()
        self.view.showPrepareWriteTags()

    ## @brief Loads available batches and shows corresponding screen
    def startSelectDefinedBatch(self):
        self.select_defined_batch.activate()
        self.view.showSelectBatch()

    ## @brief Loads available materials and shows corresponding screen
    def startSelectMaterial(self):
        self.select_material.loadMaterials()
        self.view.showSelectMaterial()

    ## @brief Passes elements of a  partial BatchDefinition to a presenter that extends it with the quantity
    # @param selected_material The material selected in a previous screen
    def startSelectQuantity(self, selected_material):
        self.select_quantity.selected_material = selected_material
        log.info("Selected material for partial batch definition: {mat}".format(mat=selected_material))

        self.select_quantity.loadQuantities()
        self.view.showSelectQuantity()

    ## @brief Passes elements of a partial BatchDefinition to a presenter that extends it with the batch_code
    def startEnterBatch(self, selected_material, quantity, unit):
        log.info("Selected quantity for partial batch definition: {mat}, {qnt} {unit}".format(mat=selected_material, qnt=quantity, unit=unit))
        self.enter_batch_code.partial_batch_definition = (selected_material, quantity, unit)
        self.view.showEnterBatchCode()

    ## @brief Passes elements of a partial BatchDefinition to a presenter that extends it with the batch_code
    def startEnterLabel(self, *partial_batch_definition):
        log.info("Selected quantity for partial batch definition: {mat}, {qnt} {unit}".format(mat=partial_batch_definition[0],
                                                                                              qnt=partial_batch_definition[1],
                                                                                              unit=partial_batch_definition[2]))
        self.enter_label.partial_batch_definition = partial_batch_definition
        self.view.showEnterLabel()

    ## @brief Collects all the fields of a BatchDefinition, sets the current_batch with those and starts the writing
    def finishEnterBatchDefinition(self, label, batch_code, selected_material, quantity, unit, another=False):
        log.info("Entered batch code for partial batch definition: {mat}, {qnt} {unit}, {batch}".format(mat=selected_material, qnt=quantity, unit=unit, batch=batch_code))
        self.current_batch = BatchDefinition(batch_code=batch_code,
                                             material_id=selected_material,
                                             quantity=quantity,
                                             unit=unit,
                                             label=label)

        if not os.path.exists(UiConfig.batchDefinitionsPath):
            with open(UiConfig.batchDefinitionsPath, "w+") as f:
                log.info("Created new empty batch definitions file")
        with open(UiConfig.batchDefinitionsPath, "r+") as batch_def_file:
            BatchDefinition.addToCsv(batch_def_file, self.current_batch)

        if not another:
            self.startWriteTags()
        else:
            self.startSelectMaterial()

    ## @brief Start the write tags presenter and view
    def startWriteTags(self):
        log.info("startWriteTags with currentBatch {curr}".format(curr=self.current_batch))
        self.cleanSignals()

        self.view.showWriteTags()

        self.filament_service.connectSignal("spoolDetected", self.write.onSpoolDetected)
        self.filament_service.connectSignal("spoolRemoved", self.write.onSpoolRemoved)
        self.write.activate()

    ## @brief Start the verify tags presenter and view
    def startVerifyTags(self):
        log.debug("startVerifyTags")
        self.cleanSignals()

        self.view.showVerifyTags()

        self.filament_service.connectSignal("spoolDetected", self.verify.onSpoolDetected)
        self.filament_service.connectSignal("spoolRemoved", self.verify.onSpoolRemoved)

        self.verify.activate()

    ## @brief Start the erase tags presenter and view
    def startErasing(self):
        log.debug("startErasing")
        self.cleanSignals()

        self.view.showErase()

        self.filament_service.connectSignal("spoolDetected", self.erase.onSpoolDetected)
        self.filament_service.connectSignal("spoolRemoved", self.erase.onSpoolRemoved)

        self.erase.activate()

    def startMaterialsUpdate(self):
        self.view.showUpdateMaterials()

    def startSystemUpdate(self):
        self.update_system.activate()
        self.view.showUpdateSystem()

    def startEnterPin(self):
        self.view.showEnterPin()

    def startAdministration(self):
        self.view.showAdministration()

    def stopVerifying(self):
        self.cleanSignals()
        self.backToStart()

        self.attachOwnSignals()

    def stopWriting(self):
        log.debug("stopWriting")
        self.cleanSignals()
        self.prepareWriteTags()

        self.attachOwnSignals()

    def stopErasing(self):
        log.debug("stopErasing")
        self.cleanSignals()
        self.backToStart()

        self.attachOwnSignals()

    def startLogExport(self):
        log.debug("Exporting logs...")
        self.view.showLogExport()
        self.log_export.activate()

    def backToStart(self):
        self.view.showStart()
        self.filament_service.connectSignal("spoolDetected", self.start.onSpoolDetected)
        self.filament_service.connectSignal("spoolRemoved", self.start.onSpoolRemoved)
    
    ## @brief Generate a human readable description given a materials's UUID
    # @param material_to_program the material ID, as either a uuid.UUID or an UUID converted to string
    # @returns str with a human readable description of the material
    def describeMaterial(self, material_to_program):
        if isinstance(material_to_program, uuid.UUID):
            material_to_program = str(material_to_program)

        try:
#            brand, name, color = ("Brand", "Plastic", "Pink")# self.material_service.getMaterialNameFields(material_to_program)
            brand, name, color = self.material_service.getMaterialNameFields(material_to_program)
            material_description = "{brand} {name} {color}".format(brand=brand, name=name, color=color)
            if material_description == "? ? ?":
                material_description = material_to_program
        except DBusException as e:
            log.exception(e)
            showError(16, log, detail=e)
            material_description = material_to_program

        return material_description

    ## @brief Generate a human readable description of an amount, given the number and the unit
    # @param the number of units in the amount
    # @param the unit in which the amount is described
    # @returns str with a human readable description of the amount
    def describeAmount(self, number, unit):
        if unit == Unit.tostring(Unit.milligram) or unit == Unit.milligram:
            amount_description = str(int(number) / 1000) + " gram"
        else:
            amount_description = str(int(number)) + unit
        return amount_description

    def onSpoolDetected(self, spool_id, has_content):
        log.debug("ApplicationPresenter.onSpoolDetected({id}, {hc})".format(id=spool_id, hc=has_content))
        self._current_spool = (spool_id, has_content)

    def onSpoolRemoved(self):
        log.debug("ApplicationPresenter.onSpoolRemoved()")
        self._current_spool = None

    def attachOwnSignals(self):
        log.debug("ApplicationPresenter.attachOwnSignals()")
        self.filament_service.connectSignal("spoolDetected", self.onSpoolDetected)
        self.filament_service.connectSignal("spoolRemoved", self.onSpoolRemoved)

    def cleanSignals(self):
        log.debug("ApplicationPresenter.cleanSignals()")
        self.filament_service.cleanSignals()

    ## Convert a naive timestamp, known by the programmer to be in UTC, as a string representation in ISO format
    @staticmethod
    def timestamp_to_string(utc_timestamp):
        return utc_timestamp.replace(tzinfo=datetime.timezone.utc).\
            astimezone(tz=None).\
            replace(tzinfo=None).\
            replace(microsecond=0).isoformat(" ")

