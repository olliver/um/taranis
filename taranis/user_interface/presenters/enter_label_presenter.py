from taranis.common.serialization.material_serializer import MAX_BATCHCODE_LEN
from .presenter import Presenter

import logging
log = logging.getLogger(__name__.split(".")[-1])

class EnterLabelPresenter(Presenter):
    def __init__(self, application):
        super(EnterLabelPresenter, self).__init__()

        self.application = application

        self.partial_batch_definition = None

    def onOkClicked(self, label):
        if self.isValid(label):
            log.info("Label '{code}'' entered".format(code=label))
            self.application.finishEnterBatchDefinition(label, *self.partial_batch_definition, another=False)
        else:
            log.info("Label'{code}' is not valid".format(code=label))
            self.view.clear()

    def onBackClicked(self):
        self.application.startEnterBatch(self.application.current_batch.material_id,
                                         self.application.current_batch.quantity,
                                         self.application.current_batch.unit)

    def enterAnotherBatchClicked(self, label):
        if self.isValid(label):
            log.info("Label '{code}'' entered".format(code=label))
            self.application.finishEnterBatchDefinition(label, *self.partial_batch_definition, another=True)
        else:
            log.info("Label'{code}' is not valid".format(code=label))
            self.view.clear()

    def isValid(self, label):
        return isinstance(label, str)