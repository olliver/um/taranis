## Base View-class
# A View takes care of the controls on screen.
# Any update made by the presenter is displayed to the user by the View.
# A View does no application logic at all.
#
# A View can be used in multiple inheritance. When a view is implemented in Qt, the View must be the first base class


class View(object):
    """All views must inherit from this.
    In case of Qt-based views, this class must be first in the base class order.
    HelloWorldView(View, QtWidgets.QWidget) *does* work
    HelloWorldView(QtWidgets.QWidget, View) does *not*
    See http://stackoverflow.com/questions/7901178/pyqt4-and-inheritance for some hints as to why
    and http://python-history.blogspot.nl/2010/06/method-resolution-order.html for a detailed explanation
    """
    def __init__(self, presenter):
        super(View, self).__init__()
        self.presenter = presenter

        self.presenter.setView(self)