from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets

from ..view import View

import taranis
import os

import logging
log = logging.getLogger(__name__.split(".")[-1])


class SpoolIoView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter,
                 stop_text,
                 busy_writing_status_text,
                 busy_reading_status_text,
                 spool_has_data_text,
                 spool_has_no_data_text,
                 action_text,
                 waiting_text=_("Waiting for spool")):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        self.stop_text = stop_text
        self.busy_writing_status_text = busy_writing_status_text
        self.busy_reading_status_text = busy_reading_status_text
        self.spool_has_data_text = spool_has_data_text
        self.spool_has_no_data_text = spool_has_no_data_text
        self.action_text = action_text
        self.waiting_text = waiting_text

        self.initUi()

        self._stop.clicked.connect(self.onStopClicked)

        if taranis.user_interface.views.qt.__path__:
            here = taranis.user_interface.views.qt.__path__[0]
        else:
            here = os.path.abspath(os.path.dirname(__file__))

        self.negative_beep = os.path.join(here, "151309__tcpp__beep1-resonant-error-beep.wav")
        self.positive_beep = os.path.join(here, "263126__pan14__tone-beep-lower-slower.wav")

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        self._action = QtWidgets.QLabel(self.action_text)

        self._status = QtWidgets.QLabel()
        self._status.setAlignment(Qt.AlignCenter)

        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self._material_name_label = QtWidgets.QLabel(_("Material:"))
        self._total_amount_label = QtWidgets.QLabel(_("Total amount:"))
        self._remaining_amount_label = QtWidgets.QLabel(_("Remaining amount:"))
        self._batch_label = QtWidgets.QLabel(_("Batch code:"))
        self._timestamp_label = QtWidgets.QLabel(_("Date written:"))

        self._material_name = QtWidgets.QLabel()
        self._total_amount = QtWidgets.QLabel()
        self._remaining_amount = QtWidgets.QLabel()
        self._batch = QtWidgets.QLabel()
        self._timestamp = QtWidgets.QLabel()

        self._stop = QtWidgets.QPushButton(self.stop_text)

        #                              # row, column, rowspan, colspan
        layout.addWidget(self._action, 0, 0, 1, 1)
        layout.addWidget(self._status, 0, 1, 1, 1)
        layout.addWidget(line, 1, 0, 1, 2)
        layout.addWidget(self._material_name_label, 2, 0)
        layout.addWidget(self._total_amount_label, 3, 0)
        layout.addWidget(self._remaining_amount_label, 4, 0)
        layout.addWidget(self._batch_label, 5, 0)
        layout.addWidget(self._timestamp_label, 6, 0)
        layout.addWidget(self._material_name, 2, 1)
        layout.addWidget(self._total_amount, 3, 1)
        layout.addWidget(self._remaining_amount, 4, 1)
        layout.addWidget(self._batch, 5, 1)
        layout.addWidget(self._timestamp, 6, 1)
        layout.addWidget(self._stop, 7, 0, 1, 2)

        layout.setColumnStretch(0, 1)

        layout.setColumnStretch(1, 3)

        self.setLayout(layout)

        self.setUpdatesEnabled(True)

        self.indicateWaitingForSpool()

    def onStopClicked(self):
        self.parent().setStyleSheet("")
        self._status.setText("")
        self._material_name.setText("")
        self._total_amount.setText("")
        self._remaining_amount.setText("")
        self._batch.setText("")
        self._timestamp.setText("")
        self.presenter.onStopClicked()

    def setStatus(self, status):
        self._status.setText(status)

    def setMaterialName(self, materialName):
        self._material_name.setText(materialName)

    def setTotalAmount(self, totalAmount):
        self._total_amount.setText(str(totalAmount))

    def setRemainingAmount(self, remainingAmount):
        self._remaining_amount.setText(str(remainingAmount))

    def setUnitForAmount(self, unitForAmount):
        self._total_amount.setText(self._total_amount.text() + unitForAmount)
        self._remaining_amount.setText(self._remaining_amount.text() + unitForAmount)

    def setBatchCode(self, batchCode):
        self._batch.setText(batchCode)

    def setTimestamp(self, timestamp_txt):
        self._timestamp.setText(timestamp_txt)

    def indicateSpoolOk(self):
        self._status.setText(_("Spool OK"))
        self.parent().setStyleSheet("background-color:green; color: white")
        self._stop.setStyleSheet("background-color:LightGray; color: black")

        self.playSound(self.positive_beep)

    def indicateSpoolNotOK(self):
        self.parent().setStyleSheet("background-color:purple; color: white")
        self._stop.setStyleSheet("background-color:LightGray; color: black")

        self.playSound(self.negative_beep)

    def indicateBusyWriting(self):
        log.debug("indicateBusyWriting")
        self.parent().setStyleSheet("background-color:LightBlue; color: black")
        self._stop.setStyleSheet("background-color:LightGray; color: black")
        self._status.setText(self.busy_writing_status_text)
        self._material_name.setText("...")
        self._total_amount.setText("...")
        self._remaining_amount.setText("...")
        self._batch.setText("...")
        self._timestamp.setText("...")
        self.repaint()
        self.update()

    def indicateBusyReading(self):
        log.debug("indicateBusyReading")
        self.parent().setStyleSheet("background-color:LightBlue; color: black")
        self._stop.setStyleSheet("background-color:LightGray; color: black")
        self._status.setText(_("Reading spool ..."))
        self._material_name.setText("...")
        self._total_amount.setText("...")
        self._remaining_amount.setText("...")
        self._batch.setText("...")
        self._timestamp.setText("...")
        self.repaint()
        self.update()

    def indicateSpoolHasData(self):
        log.debug("indicateSpoolHasData")
        self._status.setText(self.spool_has_data_text)
        self._material_name.setText(" - ")
        self._total_amount.setText(" - ")
        self._remaining_amount.setText(" - ")
        self._batch.setText(" - ")
        self._timestamp.setText(" - ")
        self.indicateSpoolNotOK()
        self.repaint()
        self.update()

    def indicateSpoolHasNoData(self):
        log.debug("indicateSpoolHasNoData")
        self._status.setText(self.spool_has_no_data_text)
        self._material_name.setText(" - ")
        self._total_amount.setText(" - ")
        self._remaining_amount.setText(" - ")
        self._batch.setText(" - ")
        self._timestamp.setText(" - ")
        self.indicateSpoolNotOK()
        self.repaint()
        self.update()


    def indicateWaitingForSpool(self):
        log.debug("indicateWaitingForSpool")
        self.parent().setStyleSheet("")
        self._status.setText(self.waiting_text)
        self._material_name.setText("")
        self._total_amount.setText("")
        self._remaining_amount.setText("")
        self._batch.setText("")
        self._timestamp.setText("")
        self.repaint()
        self.update()

    def playSound(self, path):
        try:
            os.system("aplay {path} &".format(path=path))
        except Exception as ex:
            log.exception(ex)
