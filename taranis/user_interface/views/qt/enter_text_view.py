from PyQt5.QtCore import Qt

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QSizePolicy

from ..view import View

from .widgets.keyboard import VirtualKeyboard

import logging
log = logging.getLogger(__name__.split(".")[-1])

## @brief A generic view for entering text using a virtual keyboard.
# @param hint_text A hint to show what text to enter, to the user
# @param middle_button An optional button, with a text and presenter action attached to it.
#   If you want to use this button, pass a (str, function)-tuple
class EnterTextView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter, hint_text, middle_button=None):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        self.hint_text = hint_text

        self.middle_button = middle_button

        self.initUi()

        self._ok.clicked.connect(self.onOkClicked)
        self._back.clicked.connect(self.presenter.onBackClicked)

        if self.middle_button:
            self._middle.clicked.connect(self.onMiddleButtonClicked)

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        # self._hint = QtWidgets.QLabel(self.hint_text)
        self._edit = VirtualKeyboard(self.hint_text)
        self._edit.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self._ok = QtWidgets.QPushButton(_("OK"))
        self._back = QtWidgets.QPushButton(_("Back"))

        extra = 0
        if self.middle_button:
            self._middle = QtWidgets.QPushButton(self.middle_button[0])
            extra += 1

        # layout.addWidget(self._hint, 0, 0, 1, 2, Qt.AlignHCenter)
        layout.addWidget(self._edit, 1, 0, 1, 2+extra)
        layout.setRowStretch(1, 10)

        layout.addWidget(self._back, 2, 0)
        if self.middle_button:
            layout.addWidget(self._middle, 2, 1)

        layout.addWidget(self._ok, 2, 1+extra)

        self.setLayout(layout)

    def onOkClicked(self):
        text = self._edit.toPlainText()
        self.presenter.onOkClicked(text)

    def onMiddleButtonClicked(self):
        text = self._edit.toPlainText()
        self.middle_button[1](text)

    def clear(self):
        self._edit.clear()