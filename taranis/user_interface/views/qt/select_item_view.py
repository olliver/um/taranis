from PyQt5.QtCore import QModelIndex
from PyQt5 import QtWidgets
from PyQt5 import QtGui


from PyQt5.QtWidgets import QSizePolicy

from ..view import View

import logging
log = logging.getLogger(__name__.split(".")[-1])

class SelectFromListView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        self.initUi()

        self.model = QtGui.QStandardItemModel(self._list)
        self._list.setModel(self.model)

        self._back.clicked.connect(self.presenter.onBackClicked)
        self._ok.clicked.connect(self.onOkClicked)
        self._list.doubleClicked.connect(self.onItemDoubleClicked)

    def initUi(self):
        layout = QtWidgets.QVBoxLayout()

        self._list = QtWidgets.QListView()

        horizontal_layout = QtWidgets.QHBoxLayout()
        self._ok = QtWidgets.QPushButton(_("OK"))
        self._back = QtWidgets.QPushButton(_("Back"))

        self._list.setSizePolicy(QSizePolicy.MinimumExpanding,  QSizePolicy.MinimumExpanding)
        self._list.setStyleSheet("font-family: 'Courier';")

        layout.addWidget(self._list)
        horizontal_layout.addWidget(self._back)
        horizontal_layout.addWidget(self._ok)
        layout.addLayout(horizontal_layout)

        self.setLayout(layout)

    def showItems(self, items):
        self.model.clear()

        for batch in items:
            # Create an item with a caption
            log.debug("Adding item {}".format(batch))
            item = QtGui.QStandardItem(str(batch))

            item.setEditable(False)
            item.setCheckable(False)
            item.setSelectable(True)

            # Add the item to the model
            self.model.appendRow(item)

        self.repaint()

    def onItemDoubleClicked(self, model_index):
        assert isinstance(model_index, QModelIndex)

        index = model_index.row()

        self.presenter.onItemIndexSelected(index)

    def onOkClicked(self):
        indexes = self._list.selectedIndexes()

        if indexes:
            index = indexes[0].row()
            self.presenter.onItemIndexSelected(index)
        else:
            log.debug("Nothing selected: self._list.selectedIndexes() = {indexes}".format(indexes=indexes))