from PyQt5.QtCore import Qt
from PyQt5.QtCore import QTime, QDateTime, QTimeZone
from PyQt5 import QtWidgets

from ..view import View
from ...presenters.adjust_time_presenter import AdjustTimePresenter

import datetime
from tzlocal import reload_localzone

class AdjustTimeView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, AdjustTimePresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._adjust.clicked.connect(self.onAdjustClicked)
        self._back.clicked.connect(self.onBackClicked)
        self._time_edit.dateTimeChanged.connect(self.ondatetimeChanged)
        self._timezone.currentIndexChanged.connect(self.onTimezoneSelectionChanged)
        self._sync_enable.stateChanged.connect(self.onEnableSyncChanged)

    def setVisible(self, *args, **kwargs):
        old1 = self._time_edit.blockSignals(True)  # Do not fire a change signal when setting from code
        self.adjusted_time = datetime.datetime.now()
        self._time_edit.blockSignals(old1)  # But do enable signals after code is done changing values

        old2 = self._timezone.blockSignals(True)  # Do not fire a change signal when setting from code
        zone = reload_localzone()
        self.timezone = zone.zone
        self._timezone.blockSignals(old2) # But do enable signals after code is done changing values

        return super(AdjustTimeView, self).setVisible(*args, **kwargs)

    def initUi(self):
        layout = QtWidgets.QVBoxLayout()

        self._time_edit = QtWidgets.QDateTimeEdit()

        timezones = QTimeZone.availableTimeZoneIds()
        self._zones = [bytes(zone).decode("utf-8") for zone in timezones]

        # print(zones)
        self._timezone = QtWidgets.QComboBox()
        self._timezone.setView(QtWidgets.QListView())
        # self._timezone.setStyleSheet("QComboBox QAbstractItemView::item { min-height: 35px; min-width: 50px; }"
        #                              "QListView::item:selected { color: black; background-color: lightgray}");
        self._timezone.insertItems(len(self._zones), self._zones)

        self._sync_enable = QtWidgets.QCheckBox(_("Automatic date + time (requires internet access)"))

        self._errorLabel = QtWidgets.QLabel("")

        horizontal_layout = QtWidgets.QHBoxLayout()
        self._adjust = QtWidgets.QPushButton(_("OK"))
        self._back = QtWidgets.QPushButton(_("Back"))

        layout.addWidget(self._time_edit)
        layout.addWidget(self._timezone)
        layout.addWidget(self._sync_enable)
        layout.addWidget(self._errorLabel)
        layout.addStretch()

        horizontal_layout.addWidget(self._back)
        horizontal_layout.addWidget(self._adjust)
        layout.addLayout(horizontal_layout)

        self.setLayout(layout)

    def onAdjustClicked(self):
        self.presenter.onAdjustClicked()

    def ondatetimeChanged(self, qt_datetime):
        assert isinstance(qt_datetime, QDateTime)

        self.presenter.onDatetimeSelected(qt_datetime.toPyDateTime())

    def onTimezoneSelectionChanged(self, index):
        selected = self._zones[index]
        self.presenter.onTimezoneSelected(selected)

    def onBackClicked(self):
        self.presenter.onBackClicked()

    def onEnableSyncChanged(self, state):
        self.presenter.onEnableSyncChanged(bool(state))

    @property
    def adjusted_time(self):
        try:
            qt_date = self._time_edit.date()
            qt_time = self._time_edit.time()
            current = datetime.datetime(year=qt_date.year(),
                                        month=qt_date.month(),
                                        day=qt_date.day(),
                                        hour=qt_time.hour(),
                                        minute=qt_time.minute(),
                                        second=qt_time.second())
            return current
        except Exception as e:
            print(e)

    @adjusted_time.setter
    def adjusted_time(self, value):
        qt_time = QTime()
        qt_time.setHMS(value.hour, value.minute, value.second)
        self._time_edit.setTime(qt_time)
        self._time_edit.setDate(value)

    @property
    def timezone(self):
        selected = self._zones[self._timezone.currentIndex()]
        return selected

    @timezone.setter
    def timezone(self, timezone_name):
        index = self._zones.index(timezone_name)
        self._timezone.setCurrentIndex(index)

    def setErrorMessage(self, message):
        self._errorLabel.setText(message)

        if message:
            self._errorLabel.setStyleSheet("color: red")
        else:
            self._errorLabel.setStyleSheet("")

        self.repaint()

    def setEnableSync(self, sync_enabled):
        self._time_edit.setEnabled(not sync_enabled)
        self._sync_enable.setChecked(sync_enabled)