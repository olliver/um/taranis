from PyQt5.QtCore import QTimer
from PyQt5 import QtWidgets

import datetime

from PyQt5.QtWidgets import QSizePolicy

from ..view import View
from ...presenters.administration_presenter import AdministrationPresenter

class AdministrationView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, AdministrationPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._time.clicked.connect(self.presenter.onTimeClicked)
        self._export.clicked.connect(self.presenter.onLogExportClicked)
        self._update_materials.clicked.connect(self.presenter.onUpdateMaterialsClicked)
        self._update_system.clicked.connect(self.presenter.onUpdateSystemClicked)
        self._manual.clicked.connect(self.presenter.onManuallyEnterClicked)
        self._back.clicked.connect(self.presenter.onBackClicked)

        self._time_ticker = QTimer()
        self._time_ticker.timeout.connect(self.showCurrentTime)
        self._time_ticker.start(100)

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        self._time = QtWidgets.QPushButton(_("Set time"))
        self._export = QtWidgets.QPushButton(_("Export log book to USB"))
        self._update_materials = QtWidgets.QPushButton(_("Update materials"))
        self._update_system = QtWidgets.QPushButton(_("Update system"))
        self._manual = QtWidgets.QPushButton(_("Manually enter batch information"))
        self._back = QtWidgets.QPushButton(_("Back"))

        buttons = [self._time, self._export, self._update_materials, self._update_system, self._manual, self._back]

        for button in buttons:
            button.setSizePolicy(QSizePolicy.MinimumExpanding,  QSizePolicy.MinimumExpanding)

        for button in buttons:
            layout.addWidget(button)

        self.long_press_timer = QTimer()
        self.long_press_timer.timeout.connect(self.developerModePressed)
        self._update_system.pressed.connect(lambda *args: self.long_press_timer.start(5000))
        self._update_system.released.connect(lambda *args: self.long_press_timer.stop())

        self.setLayout(layout)

    def showCurrentTime(self):
        txt = datetime.datetime.now().replace(microsecond=0).isoformat(" ")
        txt = txt.replace(" ", "    ")
        self._time.setText(txt)

    def developerModePressed(self):
        self.presenter.onDeveloperModeClicked()

    def alertDeveloperModeActive(self, active):
        self._update_system.setText("Developer mode will be {act}activated after a reboot".format(act={False:"DE-", True:""}[active]))