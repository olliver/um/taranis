from PyQt5.QtCore import Qt
from PyQt5.QtCore import QTimer
from PyQt5 import QtWidgets

import datetime

from PyQt5.QtWidgets import QSizePolicy

from ..view import View
from ...presenters.start_presenter import StartPresenter

class StartView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, StartPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._write.clicked.connect(self.presenter.onWriteTagsClicked)
        self._verify.clicked.connect(self.presenter.onVerifyTagsClicked)
        self._erase.clicked.connect(self.presenter.onEraseClicked)
        self._admin.clicked.connect(self.presenter.onAdministrationClicked)

        self._time_ticker = QTimer()
        self._time_ticker.timeout.connect(self.showCurrentTime)
        self._time_ticker.start(100)

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        self._time = QtWidgets.QLabel(_("It is time"))
        self._time.setAlignment(Qt.AlignCenter)

        self._presence_indicator = QtWidgets.QLabel("")
        # self._presence_indicator.setAlignment(Qt.AlignRight)

        self._write = QtWidgets.QPushButton(_("Write spools"))
        self._verify = QtWidgets.QPushButton(_("Verify spools"))
        self._erase = QtWidgets.QPushButton(_("Erase spools"))
        self._admin = QtWidgets.QPushButton(_("Administration"))

        buttons = [self._write, self._verify, self._admin]

        self._write.setSizePolicy(QSizePolicy.MinimumExpanding,  QSizePolicy.MinimumExpanding)
        self._verify.setSizePolicy(QSizePolicy.MinimumExpanding,  QSizePolicy.MinimumExpanding)

        layout.addWidget(self._time, 0, 0)
        layout.addWidget(self._presence_indicator, 0, 1)

        layout.setColumnStretch(0, 50)
        layout.setColumnStretch(1, 1)

        layout.addWidget(self._write, 1, 0, 1, 2)
        layout.addWidget(self._verify, 2, 0, 1, 2)
        layout.addWidget(self._erase, 3, 0, 1, 2)
        layout.addWidget(self._admin, 4, 0, 1, 2)

        self.setLayout(layout)

    def showCurrentTime(self):
        txt = datetime.datetime.now().replace(microsecond=0).isoformat(" ")
        txt = txt.replace(" ", "    ")
        self._time.setText(txt)

    def indicateSpoolPresence(self, is_present):
        if is_present:
            self._presence_indicator.setText(u"\u25CF")
        else:
            self._presence_indicator.setText(u"")

