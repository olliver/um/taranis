import enum

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QHBoxLayout, QLineEdit, QPushButton, QSizePolicy, QVBoxLayout, QWidget, QLabel
from PyQt5.QtCore import QSize
from PyQt5.QtCore import pyqtSignal


class KeyboardLayout(enum.Enum):
    Qwerty = 0
    Digits = 1



class InputState:
    LOWER = 0
    UPPER = 1


## @brief A button that can be in 2 states and will emit a different character in each case
class KeyButton(QPushButton):
    sigKeyButtonClicked = pyqtSignal(str)

    def __init__(self, lower_key, upper_key=None):
        super(KeyButton, self).__init__()

        self._state = InputState.LOWER

        self.lower_key = lower_key
        self.upper_key = upper_key if upper_key != None else lower_key.upper()

        self.cases = (self.lower_key, self.upper_key)

        self.setText(self.lower_key)

        self.clicked.connect(self.emitKey)

    def emitKey(self):
        self.sigKeyButtonClicked.emit(self.cases[self.state])

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value
        self.setText(self.cases[self.state])

## @brief An on-screen keyboard composed of KeyButtons.
# When the shift-button is pressed on the VirtualKeyboard, all the keys switch to a different state
class VirtualKeyboard(QWidget):
    sigInputString = pyqtSignal(str)

    def __init__(self, hint_text=None, layout=KeyboardLayout.Qwerty, hidden_text=False):
        super(VirtualKeyboard, self).__init__()

        self.global_layout = QVBoxLayout(self)
        self.keys_layout = QVBoxLayout()
        self.button_layout = QHBoxLayout()

        self.state_button = QPushButton(u"\u21E7")
        self.back_button = QPushButton(u"\u232B")
        self.ok_button = QPushButton(u"\u23CE")
        # self.cancelButton = QPushButton("Cancel")
        self.hidden_text = hidden_text

        QWERTY = [
                        ["`~","1!", "2@", "3#", "4$", "5%", "6^", "7&", "8*", "9(", "0)", "-_", "=+", self.back_button],
                        [l for l in "qwertyuiop"],
                        [l for l in "asdfghjkl"]+[";:", "\'\"", "\\|", self.ok_button],
                        [l for l in "zxcvbnm"]+[",<", ".>", "/?", self.state_button],
    #                    [" "],
                    ]
        NUMERIC = [["1","2","3"], ["4","5","6"], ["7","8","9"], ["0", self.back_button]]

        layouts = {KeyboardLayout.Qwerty:QWERTY,
                   KeyboardLayout.Digits: NUMERIC}

        self.keys_per_line = layouts[layout]
        self.input_string = ""

        self._state = InputState.LOWER
        self.keys = {}

        self.input_line = QLineEdit()
        if self.hidden_text:
            self.input_line.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        for lineIndex, line in enumerate(self.keys_per_line):
            row = QHBoxLayout()
            for keypair in line:
                if not isinstance(keypair, QPushButton):
                    key_button = KeyButton(*keypair)
                    self.keys[keypair] = key_button
                    key_button.sigKeyButtonClicked.connect(self.addInputByKey)
                else:
                    key_button = keypair

                # key_button.setMinimumSize(QSize(50, 50))
                # key_button.setStyleSheet("min-height: 1.5em; min-width: 1.5em;")
                key_button.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
                row.addWidget(key_button)

            row.setSizeConstraint(QHBoxLayout.SetMaximumSize)
            self.keys_layout.addLayout(row)
        self.global_layout.setSizeConstraint(QVBoxLayout.SetMaximumSize)

        self.state_button.clicked.connect(self.switchState)
        self.back_button.clicked.connect(self.backspace)
        self.ok_button.clicked.connect(self.emitInputString)
        # self.cancelButton.clicked.connect(self.emitCancel)

        self.long_press_timer = QTimer()
        self.long_press_timer.timeout.connect(self.clear)
        self.back_button.pressed.connect(lambda *args: self.long_press_timer.start(1000))
        self.back_button.released.connect(lambda *args: self.long_press_timer.stop())

        # self.buttonLayout.addWidget(self.cancelButton)
        # self.buttonLayout.addWidget(self.backButton)
        # self.buttonLayout.addWidget(self.stateButton)
        # self.buttonLayout.addWidget(self.okButton)
        if hint_text:
            self.hint_label = QLabel(hint_text)
            top_layout = QHBoxLayout()
            top_layout.addWidget(self.hint_label)
            top_layout.addWidget(self.input_line)
            self.global_layout.addLayout(top_layout)
        else:
            self.global_layout.addWidget(self.input_line)

        self.global_layout.addLayout(self.keys_layout)

        self.global_layout.addLayout(self.button_layout)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        for key in self.keys.values():
            key.state = value
        self._state = value

    def switchState(self):
        self.state = not self.state

    def addInputByKey(self, key):
        self.input_string += (key.lower(), key.capitalize())[self.state]
        self.input_line.setText(self.input_string)

    def backspace(self):
        self.input_line.backspace()
        self.input_string = self.input_string[:-1]

    def emitInputString(self):
        self.sigInputString.emit(self.input_string)

    def emitCancel(self):
        self.sigInputString.emit("")

    def sizeHint(self):
        return QSize(480,272)

    def toPlainText(self):
        return self.input_string

    def clear(self):
        self.input_string = ""
        self.input_line.clear()


if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)
    win = VirtualKeyboard(layout=KeyboardLayout.Digits)
    win.sigInputString.connect(print)

    win.show()
    app.exec_()
