from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog

from ..view import View
from ...presenters.update_system_presenter import UpdateSystemPresenter

class UpdateSystemView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, UpdateSystemPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._stable.clicked.connect(self.onStableClicked)
        self._testing.clicked.connect(self.onTestingClicked)
        self._usb.clicked.connect(self.onUsbClicked)
        self._back.clicked.connect(self.onBackClicked)

        self._refresh_state_timer = QTimer()
        self._refresh_state_timer.timeout.connect(self.showState)

    def initUi(self):
        layout = QtWidgets.QVBoxLayout()

        self._version_label = QtWidgets.QLabel(_("Unknown version"))
        self._ip_addresses = QtWidgets.QLabel("")
        self._main_label = QtWidgets.QLabel(_("Update to"))
        self._stable = QtWidgets.QPushButton(_("Stable version"))
        self._testing = QtWidgets.QPushButton(_("Testing version"))
        self._usb = QtWidgets.QPushButton(_("Version on USB stick"))
        self._status_label = QtWidgets.QLabel("")
        self._progress_indicator = QtWidgets.QProgressBar()
        self._back = QtWidgets.QPushButton(_("Back"))

        self._progress_indicator.setRange(0, 100)

        layout.addWidget(self._version_label, alignment=Qt.AlignTop)
        layout.addWidget(self._ip_addresses)
        layout.addWidget(self._main_label)
        layout.addWidget(self._stable)
        layout.addWidget(self._testing)
        layout.addWidget(self._usb)
        layout.addWidget(self._status_label)
        layout.addWidget(self._progress_indicator)
        layout.addWidget(self._back, alignment=Qt.AlignBottom)

        self._progress_indicator.setVisible(True)

        self.setLayout(layout)

    def startRefreshTimer(self):
        self._refresh_state_timer.start(500)

    def showState(self):
        state, progress = self.presenter.getUpdateState()
        self._status_label.setText(state.lower().capitalize())
        self._progress_indicator.setValue(int(progress*100))

        if progress > 1.0:
            self._back.setDisabled(False)

        if "failed" in state.lower():
            self._back.setDisabled(False)

    def onStableClicked(self):
        self.presenter.onUpdateClicked("stable")

    def onTestingClicked(self):
        self.presenter.onUpdateClicked("testing")

    def onUsbClicked(self):
        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.ExistingFile)
        dialog.show()
        if dialog.exec():
            fileNames = dialog.selectedFiles()
            if fileNames:
                self.presenter.onFromUsbClicked(fileNames[0])

    def onBackClicked(self):
        self._progress_indicator.setVisible(False)
        self._refresh_state_timer.stop()
        self.presenter.onBackClicked()

    def indicateUpdateStarted(self):
        self._status_label.setText(_("Update started..."))
        self._back.setDisabled(True)
        self._progress_indicator.setVisible(True)
        self._progress_indicator.setValue(0)
        self.repaint()

    def setVersion(self, version_string):
        self._version_label.setText(version_string)

    def showIpAddresses(self):
        from PyQt5.QtNetwork import QNetworkInterface
        all_addresses = QNetworkInterface.allAddresses()
        address_strs = [addr.toString() for addr in all_addresses if not addr.isLoopback()]
        self._ip_addresses.setText("\n".join(address_strs))
