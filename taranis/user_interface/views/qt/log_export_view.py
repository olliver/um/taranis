from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog

from ..view import View
from ...presenters.log_export_presenter import LogExportPresenter

class LogExportView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, LogExportPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._select.clicked.connect(self.onSelectClicked)
        self._start.clicked.connect(self.onExportClicked)
        self._back.clicked.connect(self.onBackClicked)

    def initUi(self):
        layout = QtWidgets.QVBoxLayout()

        self._select = QtWidgets.QPushButton(_("Select destination"))
        self._destination_label = QtWidgets.QLabel("")
        self._start = QtWidgets.QPushButton(_("Start exporting log book"))
        self._status_label = QtWidgets.QLabel("")
        self._progress_indicator = QtWidgets.QProgressBar()
        self._back = QtWidgets.QPushButton(_("Back"))

        self._progress_indicator.setRange(0, 100)

        layout.addWidget(self._select, alignment=Qt.AlignVCenter)
        layout.addWidget(self._destination_label)
        layout.addWidget(self._start, alignment=Qt.AlignVCenter)
        layout.addWidget(self._status_label)
        layout.addWidget(self._progress_indicator)
        layout.addWidget(self._back, alignment=Qt.AlignBottom)

        self._progress_indicator.setVisible(True)

        self.setLayout(layout)

    def onSelectClicked(self):
        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.DirectoryOnly)
        dialog.show()
        if dialog.exec():
            fileNames = dialog.selectedFiles()
            if fileNames:
                self.presenter.onDestinationSelected(fileNames[0])

    def showSelectedDestination(self, destination):
        self._destination_label.setText(destination)

    def onExportClicked(self):
        self.presenter.onExportClicked()

    def onBackClicked(self):
        self._progress_indicator.setVisible(False)
        self.presenter.onBackClicked()

    def indicateUpdateStarted(self):
        self._status_label.setText(_("Export in progress ..."))
        self._progress_indicator.setVisible(True)
        self._progress_indicator.setValue(50)
        self.repaint()

    def indicateUpdateSuccessful(self):
        self._status_label.setText(_("Export successful"))
        # self._progressIndicator.setVisible(True)
        self._progress_indicator.setRange(0, 100)
        self._progress_indicator.setValue(100)
        self.repaint()

    def indicateUpdateFailed(self):
        self._status_label.setText(_("Export failed. Please try again with a different destination"))
        self.repaint()
