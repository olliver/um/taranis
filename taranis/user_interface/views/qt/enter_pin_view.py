from PyQt5.QtCore import Qt

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QSizePolicy

from ..view import View

from .widgets.keyboard import VirtualKeyboard, KeyboardLayout

import logging
log = logging.getLogger(__name__.split(".")[-1])

class EnterPinView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        self.hint_text = _("PIN")

        self.initUi()

        self._ok.clicked.connect(self.onOkClicked)
        self._back.clicked.connect(self.presenter.onBackClicked)

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        # self._hint = QtWidgets.QLabel(self.hint_text)
        self._edit = VirtualKeyboard(self.hint_text, layout=KeyboardLayout.Digits, hidden_text=True)
        self._edit.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self._ok = QtWidgets.QPushButton(_("OK"))
        self._back = QtWidgets.QPushButton(_("Back"))

        # layout.addWidget(self._hint, 0, 0, 1, 2, Qt.AlignHCenter)
        layout.addWidget(self._edit, 1, 0, 1, 2)
        layout.setRowStretch(1, 10)
        layout.addWidget(self._ok, 2, 1)
        layout.addWidget(self._back, 2, 0)

        self.setLayout(layout)

    def onOkClicked(self):
        text = self._edit.toPlainText()
        self.presenter.onOkClicked(text)

    def clear(self):
        self._edit.clear()