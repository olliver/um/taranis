from PyQt5.QtCore import Qt
from PyQt5.QtCore import QTimer
from PyQt5 import QtWidgets

import datetime

from PyQt5.QtWidgets import QSizePolicy

from ..view import View
from ...presenters.prepare_writing_presenter import PrepareWritingPresenter

class PrepareWritingView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, PrepareWritingPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._continue.clicked.connect(self.presenter.onContinueWithCurrentClicked)
        self._select.clicked.connect(self.presenter.onSelectFromListClicked)
        self._back.clicked.connect(self.presenter.onBackClicked)

    def initUi(self):
        layout = QtWidgets.QGridLayout()

        self._continue = QtWidgets.QPushButton(_("Continue with current batch"))
        self._select = QtWidgets.QPushButton(_("Select batch and material"))
        self._back = QtWidgets.QPushButton(_("Back"))

        buttons = [self._continue, self._select]

        for button in buttons:
            button.setSizePolicy(QSizePolicy.MinimumExpanding,  QSizePolicy.MinimumExpanding)
            layout.addWidget(button)

        layout.addWidget(self._back)

        self.setLayout(layout)

    def setContinueEnabled(self, continue_enabled):
        self._continue.setEnabled(continue_enabled)

    def setCurrentBatchDescription(self, batch_description):
        self._continue.setText(_("Continue with:")+"\n{curr}".format(curr=batch_description))