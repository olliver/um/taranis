from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets

from ..view import View
from ...presenters.update_materials_presenter import UpdateMaterialsPresenter

class UpdateMaterialsView(View, QtWidgets.QWidget):
    def __init__(self, parent, presenter):

        View.__init__(self, presenter)
        QtWidgets.QWidget.__init__(self, parent)

        assert isinstance(self.presenter, UpdateMaterialsPresenter)  # Not really needed, but helps as a type hint when programming

        self.initUi()

        self._start.clicked.connect(self.onUpdateClicked)
        self._back.clicked.connect(self.onBackClicked)

    def initUi(self):
        layout = QtWidgets.QVBoxLayout()

        self._start = QtWidgets.QPushButton(_("Start update"))
        self._status_label = QtWidgets.QLabel("")
        self._progress_indicator = QtWidgets.QProgressBar()
        self._back = QtWidgets.QPushButton(_("Back"))

        self._progress_indicator.setRange(0, 100)

        layout.addWidget(self._start, alignment=Qt.AlignVCenter)
        layout.addWidget(self._status_label)
        layout.addWidget(self._progress_indicator)
        layout.addWidget(self._back, alignment=Qt.AlignBottom)

        self._progress_indicator.setVisible(True)

        self.setLayout(layout)

    def onUpdateClicked(self):
        self.presenter.onUpdateClicked()

    def onBackClicked(self):
        self._status_label.setText("")
        self._progress_indicator.setVisible(False)
        self.presenter.onBackClicked()

    def indicateUpdateStarted(self):
        self._status_label.setText(_("Update in progress..."))
        self._progress_indicator.setVisible(True)
        self._progress_indicator.setValue(50)
        self.repaint()

    def indicateUpdateSuccessful(self):
        self._status_label.setText(_("Update successful"))
        # self._progressIndicator.setVisible(True)
        self._progress_indicator.setRange(0, 100)
        self._progress_indicator.setValue(100)
        self.repaint()

    def indicateUpdateFailed(self):
        self._status_label.setText(_("Update failed. Please try again later"))
        self.repaint()
