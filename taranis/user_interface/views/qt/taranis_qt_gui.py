import os
import sys
import logging

import gettext

# import ipdb; ipdb.set_trace()
# nl = gettext.translation("taranis", languages=["nl"])
# nl.install()

gettext.install('taranis')

from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets

import taranis.user_interface.views.qt

from taranis.user_interface.views import view
from taranis.user_interface.views.qt import start_view
from taranis.user_interface.views.qt import update_materials_view
from taranis.user_interface.views.qt import update_system_view
from taranis.user_interface.views.qt import adjust_time_view
from taranis.user_interface.views.qt import administration_view
from taranis.user_interface.views.qt import prepare_writing_view
from taranis.user_interface.views.qt import select_item_view
from taranis.user_interface.views.qt import enter_text_view
from taranis.user_interface.views.qt import spool_io_view
from taranis.user_interface.views.qt import log_export_view
from taranis.user_interface.views.qt import enter_pin_view

from taranis.user_interface.presenters.application_presenter import ApplicationPresenter


log = logging.getLogger(__name__.split(".")[-1])


class TaranisQtGui(view.View, QtWidgets.QMainWindow):
    def __init__(self):
        presenter = ApplicationPresenter()
        view.View.__init__(self, presenter)
        QtWidgets.QMainWindow.__init__(self)

        assert isinstance(self.presenter, ApplicationPresenter)

        self.initUi()

        self.presenter.backToStart()

    def initUi(self):

        # Show exit buttons when running on a desktop screen (handy for debugging
        size = QtWidgets.QApplication.desktop().screenGeometry()
        if size.width() > 1024 and size.height() > 800:
            self.setMinimumSize(QSize(800, 480))
        else:
            self.setWindowFlags(Qt.FramelessWindowHint)
            self.showMaximized()

        self.central_widget = QtWidgets.QStackedWidget()
        self.setCentralWidget(self.central_widget)

        self.start = start_view.StartView(self, self.presenter.start)
        self.start_write = prepare_writing_view.PrepareWritingView(self, self.presenter.prepare_writing)
        self.write = spool_io_view.SpoolIoView(self, self.presenter.write,
                                               busy_writing_status_text=_("Writing spool ..."),
                                               busy_reading_status_text=None,
                                               spool_has_data_text=_("Spool contains data already, reading ..."),
                                               spool_has_no_data_text=None,
                                               stop_text=_("Back"),
                                               action_text=_("Write spool"))
        self.verify = spool_io_view.SpoolIoView(self, self.presenter.verify,
                                                busy_writing_status_text=None,
                                                busy_reading_status_text=_("Reading spool ..."),
                                                spool_has_data_text=None,
                                                spool_has_no_data_text=_("Spool contains no data"),
                                                stop_text=_("Back"),
                                                action_text=_("Verify spool"))
        self.update_materials = update_materials_view.UpdateMaterialsView(self, self.presenter.update_materials)
        self.update_system = update_system_view.UpdateSystemView(self, self.presenter.update_system)
        self.adjust_time = adjust_time_view.AdjustTimeView(self, self.presenter.adjust_time)
        self.administration = administration_view.AdministrationView(self, self.presenter.administration)
        self.select_batch = select_item_view.SelectFromListView(self, self.presenter.select_defined_batch)
        self.select_material = select_item_view.SelectFromListView(self, self.presenter.select_material)
        self.select_quantity = select_item_view.SelectFromListView(self, self.presenter.select_quantity)
        self.enter_batch_code = enter_text_view.EnterTextView(self, self.presenter.enter_batch_code, _("Batch code"))
        self.enter_label = enter_text_view.EnterTextView(self, self.presenter.enter_label, _("Label"),
                                                         middle_button=(_("OK, enter more"),
                                                                        self.presenter.enter_label.enterAnotherBatchClicked))
        self.enter_pin = enter_pin_view.EnterPinView(self, self.presenter.enter_pin)
        self.erase = spool_io_view.SpoolIoView(self, self.presenter.erase,
                                               busy_writing_status_text=_("Erasing spool ..."),
                                               busy_reading_status_text=None,
                                               spool_has_data_text=_("Spool still contains data"),
                                               spool_has_no_data_text=_("Spool contains no data"),
                                               stop_text=_("Back"),
                                               action_text=_("Erase spool"))
        self.log_export = log_export_view.LogExportView(self, self.presenter.log_export)

        self.central_widget.addWidget(self.start)
        self.central_widget.addWidget(self.start_write)
        self.central_widget.addWidget(self.verify)
        self.central_widget.addWidget(self.update_materials)
        self.central_widget.addWidget(self.update_system)
        self.central_widget.addWidget(self.adjust_time)
        self.central_widget.addWidget(self.administration)
        self.central_widget.addWidget(self.write)
        self.central_widget.addWidget(self.select_batch)
        self.central_widget.addWidget(self.select_material)
        self.central_widget.addWidget(self.select_quantity)
        self.central_widget.addWidget(self.enter_batch_code)
        self.central_widget.addWidget(self.enter_label)
        self.central_widget.addWidget(self.erase)
        self.central_widget.addWidget(self.log_export)
        self.central_widget.addWidget(self.enter_pin)

    def showPrepareWriteTags(self):
        self.central_widget.setCurrentWidget(self.start_write)

    def showSelectBatch(self):
        self.central_widget.setCurrentWidget(self.select_batch)

    def showSelectMaterial(self):
        self.central_widget.setCurrentWidget(self.select_material)

    def showSelectQuantity(self):
        self.central_widget.setCurrentWidget(self.select_quantity)

    def showEnterBatchCode(self):
        self.central_widget.setCurrentWidget(self.enter_batch_code)

    def showEnterLabel(self):
        self.central_widget.setCurrentWidget(self.enter_label)

    def showWriteTags(self):
        self.write.indicateWaitingForSpool()
        self.central_widget.setCurrentWidget(self.write)

    def showVerifyTags(self):
        self.verify.indicateWaitingForSpool()
        self.central_widget.setCurrentWidget(self.verify)

    def showUpdateMaterials(self):
        self.central_widget.setCurrentWidget(self.update_materials)

    def showUpdateSystem(self):
        self.central_widget.setCurrentWidget(self.update_system)

    def showLogExport(self):
        self.central_widget.setCurrentWidget(self.log_export)

    def showAdjustTime(self):
        self.central_widget.setCurrentWidget(self.adjust_time)

    def showEnterPin(self):
        self.central_widget.setCurrentWidget(self.enter_pin)

    def showAdministration(self):
        self.central_widget.setCurrentWidget(self.administration)

    def showErase(self):
        self.central_widget.setCurrentWidget(self.erase)

    def showStart(self):
        self.central_widget.setCurrentWidget(self.start)


def main():
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    app = QtWidgets.QApplication(sys.argv)

    if taranis.user_interface.views.qt.__path__:
        here = taranis.user_interface.views.qt.__path__[0]
    else:
        here = os.path.abspath(os.path.dirname(__file__))

    styleFile = os.path.join(here, "touchscreen.css")
    with open(styleFile, "r") as fh:
        app.setStyleSheet(fh.read())

    main_view = TaranisQtGui()

    main_view.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
