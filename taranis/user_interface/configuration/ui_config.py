from taranis.common.data.unit import Unit

import os

public_configuration_path = os.path.join("/media/", "ultimaker", "nfc_config_pub/")


class UiConfig(object):
    logExportDestination = os.path.join(public_configuration_path, "log_export.csv")
    selectableQuantities = [(350000, Unit.milligram), (700000, Unit.milligram), (750000, Unit.milligram), (800000, Unit.milligram),]

    @staticmethod
    def getPinFilePath():
        return os.path.join(public_configuration_path, "pin.txt")

    @staticmethod
    def getPinObfuscationConstant():
        # https://xkcd.com/221/
        return 866929256790 # chosen by fair random.randint(0, 999999999999)
                            # guaranteed to be random (enough, for this)

    batchDefinitionsPath = os.path.join(public_configuration_path, "batch_definitions.csv")
