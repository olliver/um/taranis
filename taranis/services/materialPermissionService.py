## @package docstring
# Material permissions service
#
# Offers a list of the materials that are permitted for production

import logging
import urllib.request
import urllib.error
import uuid

from griffin import dbusif
from taranis.common.data.fdm_whitelist import FdmWhitelist
from taranis.scripts.configuration.whitelist_decryption import WhitelistDecryptionConfiguration
from taranis.services.configuration.fdm_whitelist_download import FdmWhitelistDownload

log = logging.getLogger(__name__.split(".")[-1])

dbusif.__DBUS_MAXIMUM_CALL_TIME = None


## @brief Service that can answer queries on which materials are currently permitted for production on this machine
# It uses an encrypted whitelist for this and the corresponding keys for decryption + verification
class MaterialPermissionService(dbusif.ServiceObject):
    def __init__(self):
        super(MaterialPermissionService, self).__init__("material_permission")

        self.__whitelist = None
        self.loadWhitelist()

    ## @brief read, verify and decrypt the whitelist
    @dbusif.method(out_signature="b")
    def loadWhitelist(self):
        try:
            self.__whitelist = FdmWhitelist.fromEncryptedFile(
                open(WhitelistDecryptionConfiguration.encrypted_path, "rb"),
                open(WhitelistDecryptionConfiguration.signature_path, "rb"),
                WhitelistDecryptionConfiguration.getWhitelistVerificationKey(),
                WhitelistDecryptionConfiguration.getWhitelistDecryptionKey())

            log.info("Loaded permissions for materials {materials}".format(materials=self.getPermittedMaterials()))
            return True
        except Exception as ex:
            log.exception(ex)
            self.__whitelist = None
            return False

    ## @brief check whether a material with the given ID is permitted on the machine the service runs on
    # @param material_id_str uuid.UUID converted to string, corresponding to the material to be checked
    # @returns boolean indicating whether the material is permitted or not
    @dbusif.method("s", "b")
    def isPermitted(self, material_id_str):
        if self.__whitelist:
            return self.__whitelist.isPermitted(uuid.UUID(material_id_str))
        else:
            return False

    ## @brief list all the materials permitted on this station
    # @returns an array of uuid.UUIDs converted to strings, each of which corresponds with a materials
    @dbusif.method(out_signature="as")
    def getPermittedMaterials(self):
        if self.__whitelist:
            return [str(permission.material_id) for permission in self.__whitelist.permitted_materials]
        else:
            return []

    ## @brief Fetch a new encrypted and signed whitelist from the given URI
    # @returns boolean indicating whether the update was successful
    @dbusif.method(out_signature="b")
    def updateMaterialPermissions(self):
        try:
            self._downloadTo(FdmWhitelistDownload.EncryptedWhitelistUri, WhitelistDecryptionConfiguration.encrypted_path)
            self._downloadTo(FdmWhitelistDownload.WhitelistSignatureUri, WhitelistDecryptionConfiguration.signature_path)
            log.info("Whitelist and signature downloaded")
        except (OSError, urllib.error.URLError) as error:
            log.error("Whitelist cannot be updated")
            return False
        self.loadWhitelist()
        return True

    ## @brief download a file from a remote location to a local one
    def _downloadTo(self, from_, to_):
        log.info("Downloading from {frm} to {to}".format(frm=from_, to=to_))
        try:
            with urllib.request.urlopen(from_) as orig, open(to_, "wb") as destination:
                destination.write(orig.read())
        except urllib.error.URLError as error:
            log.error("Cannot download from {}: {}", from_, error)
            raise error
        except IOError as error:
            log.error("Cannot store download at {}: {}", to_, error)
            raise error

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    service = MaterialPermissionService()
    dbusif.runMainLoop()

