## @package docstring
#
# Offers an Interface to a FilamentSpool

import logging
import threading
import binascii
import time
import signal
import uuid
import os

from griffin import dbusif
from pynfc.ntag_read import NTagReadWrite, UnknownTagTypeException, NTagInfo
from nfc.ndef import error
from taranis.common.configuration.tag_configuration import TagConfiguration
from taranis.common.data import Material
from taranis.common.data import MaterialStatistics
from taranis.common.data.unit import Unit
from taranis.common.serialization import MaterialSerializerV0
from taranis.common.serialization import MaterialStatisticsSerializerV0
from taranis.errors import showError
from taranis.scripts.configuration.signatures import SignatureConfiguration
from taranis.scripts.configuration.station import Station
from taranis.scripts.filament_spool_reader import FilamentSpoolReader
from taranis.scripts.filament_spool_writer import FilamentSpoolWriter

from taranis.common.traceability_logger import TraceabilityLogger
from taranis.common.stopwatch import stopwatch

from taranis.services.configuration.tracing import TracingConfiguration
from taranis.services.test.dummy_ntag_read_write import DummyNtagReadWrite

log = logging.getLogger(__name__.split(".")[-1])

dbusif.__DBUS_MAXIMUM_CALL_TIME = None

def _uidToStr(uid):
    return str(binascii.hexlify(uid).decode("ascii"))

def _strToUid(string):
    return binascii.unhexlify(string.encode("ascii"))

## @brief Offers an interface to get/set information on a filament spool
class FilamentSpoolInterfacingService(dbusif.ServiceObject):
    def __init__(self, use_dummy=False):
        super(FilamentSpoolInterfacingService, self).__init__("filament_spool_interfacing")

        try:
            self.ntag_interface = DummyNtagReadWrite() if use_dummy else NTagReadWrite(logging.getLogger("ntagReadWrite"))

            material_serializer = MaterialSerializerV0()
            stats_serializer = MaterialStatisticsSerializerV0()

            self.writer = FilamentSpoolWriter(self.ntag_interface, material_serializer, stats_serializer)
            self.reader = FilamentSpoolReader(self.ntag_interface, material_serializer, stats_serializer)
        except OSError as ose:
            showError(18, log, ose)
            exit(-18)  # There is no point in trying with out a device

        signal.signal(signal.SIGTERM, self.stopService)
        self.stop_event = threading.Event()

        self.traceability_logger = TraceabilityLogger(TracingConfiguration.tracingLogFilename)

        self.nfc_lock = threading.Lock()

        self.startDetectionLoop()

    ## @brief Stop the service and let it exit nicely
    @dbusif.method()
    def stopService(self, *args):
        log.info("Got a request to terminate the service. ({args})".format(args=args))
        self.stopDetectionLoop()
        self.ntag_interface.close()
        dbusif.abortMainLoop()

    ## @brief Read a spool completely. Also determines the cryptographic signing
    def readSpool(self, spool_id):
        log.debug("readSpool: start")

        with stopwatch("readSpool"):
            if not self.ntag_interface:
                return None, None, False, 18  # Could not open NFC device

            with self.nfc_lock:
                if not spool_id:
                    log.debug("readSpool: Wait for tag before reading")
                    self.writer.waitForOneTag()

                try:
                    log.debug("Reading...")
                    current_tag_type, current_uid = self.writer.determineTagType()
                    read_material, serial_number, read_stats, crypto_ok = self.reader.readTagContent(tag_type=current_tag_type)
                    log.info("Tag {serial} contains: {mat}, {stats}. Its signing is{ok}OK"
                             .format(serial=serial_number,
                                     mat=read_material,
                                     stats=read_stats,
                                     ok={True:" ", False:" NOT "}[crypto_ok]))

                    if not SignatureConfiguration.signature_enabled:
                        log.warn("readSpool: Because signatures are not enabled, set crypto_ok to True")
                        crypto_ok = True

                    log.debug("readSpool: return")
                    return read_material, read_stats, crypto_ok, 0
                except (IndexError, ValueError) as read_error:
                    showError(10, log, detail=read_error)
                    return None, None, False, 10
                except error.LengthError as length_error:
                    showError(11, log, detail=length_error)
                    return None, None, False, 11
                except OSError as os_error:
                    showError(2, log, detail=os_error)
                    return None, None, False, 2
                except Exception as other:
                    showError(10, log, detail=other)
                    return None, None, False, 10

    ## @brief Tells the material ID on the spool, the total and remaining amount with the unit and whether the cryptographic signing of that material is OK.
    #  If the signing is NOT OK, then the spool data may be spoofed or corrupted
    #  @param spool_id which spool to get the material of
    #  @return: tuple of (str: material_id, int: total, int: remaining, str: unit, str: batch_code, bool: crypto_ok, int: error_code)
    @dbusif.method(in_signature="s", out_signature="dsiissbi")
    def getSpoolSummary(self, spool_id):
        read_material, read_stats, crypto_ok, error_code = self.readSpool(spool_id)

        if isinstance(read_material, Material) and isinstance(read_stats, MaterialStatistics):
            timestamp = read_material.manufacturing_timestamp.timestamp()
            return_data = (timestamp, str(read_material.material_id), read_stats.total, read_stats.remaining, Unit.tostring(read_stats.unit), read_material.batch_code, crypto_ok, error_code)
            log.debug("getSpoolSummary: returning {ret}".format(ret=return_data))
            return return_data
        else:
            return 0.0, "", 0, 0, "", "", False, error_code

    ## @brief Tells the material ID on the spool and whether the cryptographic signing of that material is OK.
    #  If the signing is NOT OK, then the spool data may be spoofed or corrupted
    #  @param spool_id which spool to get the material of
    #  @return: tuple of (str: material_id, bool: crypto_ok)
    @dbusif.method(in_signature="s", out_signature="sbi")
    def getMaterialOnSpool(self, spool_id):
        read_material, read_stats, crypto_ok, error_code = self.readSpool(spool_id)

        assert isinstance(read_material, Material)
        return str(read_material.material_id), crypto_ok, error_code

    ## @brief Tells the amount of material left on the spool
    #  @param spool_id which spool to get the amounts of
    #  @return: tuple of: (int: total, int: remaining, str: unit)
    @dbusif.method(in_signature="s", out_signature="iis")
    def getSpoolStatistics(self, spool_id):
        read_material, read_stats, crypto_ok, error_code = self.readSpool(spool_id)

        assert isinstance(read_stats, MaterialStatistics)
        return read_stats.total, read_stats.remaining, Unit.tostring(read_stats.unit), error_code

    ## @brief write the amount of material still remaining back to the spool
    #  @param spool_id which spool to set the amounts of
    #  @param remaining_amount how much material is remaining
    #  @param unit in what unit is remaining_amount specified?
    #         This must be equal to the original amount, otherwise an error will be raised
    @dbusif.method(in_signature="sis")
    def writeSpoolStatistics(self, spool_id, remaining_amount, unit):
        raise NotImplementedError("writeSpoolStatistics is not implemented")

    ## @brief Write the given information to the spool
    #  @param spool_id str: indicating which spool to set the amounts of
    #  @param material_id str: uuid of the material on the spool
    #  @param total_amount int: how much material is on the spool?
    #  @param unit str: in what unit is total_amount specified?
    #  @param batchcode str: what batch is is this spool from
    #  @param overwrite bool: True to overwrite a previous write by this method
    #  @param verify bool: whether to verify that the write was 100% OK by reading back the data
    #  @return tuple of: (bool: success, int: error_code)
    @dbusif.method(in_signature="ssissbb", out_signature="bdi")
    def initializeSpool(self, spool_id, material_id, total_amount, unit, batchcode, overwrite=False, verify=False):
        with stopwatch("initializeSpool"):
            log.debug("initializeSpool: start")

            if not self.ntag_interface:
                return False, -1.0, 18  # Could not open NFC device

            with self.nfc_lock:
                if not spool_id:
                    # Unfortunately, we need to do this, as there is no guarantee there is a tag when the method called
                    log.debug("initializeSpool: Wait for tag before programming")
                    self.writer.waitForOneTag(self.stop_event)

                try:
                    current_tag_type, current_uid = self.writer.determineTagType()
                except OSError as ose:
                    showError(7, log, detail=ose)
                    return False, -1.0, 7
                except UnknownTagTypeException as unknown_tag_type_error:
                    showError(8, log, detail="Capability byte is {cap_byte}".format(
                        cap_byte=unknown_tag_type_error.capability_byte))
                    return False, -1.0, 8

                # Define the content for the tag
                try:
                    write_material, write_stats = self.writer.generateObjects(material=uuid.UUID(material_id),
                                                                               programming_station=Station.station_id,
                                                                               batch=batchcode,
                                                                               total=total_amount,
                                                                               unit=Unit.fromstring(unit))
                    message_for_tag = self.writer.assembleMessage(current_uid, write_material, write_stats)
                except AttributeError as attr_error:
                    showError(9, log, detail=" Error with value {error}".format(error=attr_error))
                    return False, -1.0, 9

                if not overwrite and self.writer.checkTagHasContent(current_tag_type):
                    showError(3, log)
                    return False, -1.0, 3

                # If we want to overwrite the tag, it already has a password set from a previous write by this code.
                # Then, we need to authenticate to enable writing to protected memory areas
                log.info("Writing tag {uid}...".format(uid=_uidToStr(current_uid)))
                if overwrite:
                    log.info("Overwriting previous content of tag {uid}...".format(uid=_uidToStr(current_uid)))
                    try:
                        authenticated = self.writer.authenticate()
                        if not authenticated:
                            showError(12, log)
                    except OSError as ose:
                        showError(15, log, detail=ose)
                        return False, -1.0, 15

                tag_content = bytearray(message_for_tag.to_bytes())
                # tag_content[-5] += 1
                # tag_content[100] += 1

                # Beef of this function: the actual writing
                try:
                    self.writer.writeTag(content=tag_content, tag_type=current_tag_type)
                    self.writer.configureTag(tag_type=current_tag_type)

                    # After writing, we may verify that everything went OK or not.
                    # When there is a different spool all of a sudden, the UID won't match and thus the crypto is not OK
                    if not verify:
                        log.info("Writing succeeded")

                        self.traceability_logger.trace(current_uid, write_material, write_stats)
                        return True, write_material.manufacturing_timestamp.timestamp(), 0
                    else:
                        read_material, read_stats, crypto_ok = self.readSpool(spool_id)
                        material_ok = read_material == write_material
                        stats_ok = read_stats == write_stats

                        if material_ok and stats_ok and crypto_ok:
                            log.info("Writing succeeded")
                            return True, read_material.manufacturing_timestamp.timestamp(), 0
                        else:
                            if not crypto_ok:
                                showError(13, log)
                            else:
                                showError(4, log, detail="material: {mat}, "
                                                          "stats: {stats}, "
                                                          "crypto: {crypto}".format(mat=material_ok, stats=stats_ok,
                                                                                    crypto=crypto_ok))
                            return False, -1.0, 4
                except (OSError, ValueError) as write_error:
                    showError(1, log, detail=write_error)
                    return False, -1.0, 1

    ## @brief erase the presented spool's tag
    #  @return bool: success
    @dbusif.method("s", "b")
    def eraseSpool(self, spool_id):
        with stopwatch("eraseSpool"):
            # The erase-process in taranis.scripts.eraseTag is a bit more complicated with error handling when the password is incorrect.
            # This has never been used in practice to date, so that logic can be added here when needed. YAGNI.
            if not self.ntag_interface:
                return False  # , 18  # Could not open NFC device

            with self.nfc_lock:
                log.debug("eraseSpool: Wait for tag before programming")
                if not spool_id:
                    self.writer.waitForOneTag(self.stop_event)
                current_tag_type, current_uid = self.writer.determineTagType()
                pages = TagConfiguration.LAST_MESSAGE_PAGE
                content = bytes(pages*NTagInfo.BYTES_PER_PAGE)  # These all default to 0
                self.writer.tag_interface.write_user_memory(content, current_tag_type)

                log.info("Tag {uid} cleared".format(uid=_uidToStr(current_uid)))
                return True

    ## @brief Start detecting spools. A signal will be raised when one is detected
    @dbusif.method()
    def startDetectionLoop(self):
        log.info("Start spool detection")
        self.detection_thread = threading.Thread(target=self._loopSpoolDetections, daemon=True)
        self.detection_thread.start()
        return

    ## @brief Stop detecting spools
    @dbusif.method()
    def stopDetectionLoop(self):
        log.info("Stop spool detection")
        self.stop_event.set()
        if hasattr(self, "detection_thread") and self.detection_thread:
            self.detection_thread.join()
        return

    ## @brief Loop to detect whether spools are present.
    # Emits a signal when a spool is detected
    def _loopSpoolDetections(self):
        previous_count = 0

        while not self.stop_event.is_set():
            # log.debug("_loopSpoolDetections: Waiting for 1 tag...")
            while not self.stop_event.is_set():
                with self.nfc_lock:
                    current_count = self.writer.checkTagCount()
                    if current_count == 1:
                        if previous_count == 0:
                            try:
                                uid, has_content = self.identifySpool()
                                self._raiseSpoolDetected(uid, has_content)
                            except OSError:
                                log.warn("Could not determine tag type, is there a tag present?")
                                continue
                            except UnknownTagTypeException as unknown_tag_type_error:
                                log.warn("Tag type unknown. Capability byte is {cap_byte}".format(cap_byte=unknown_tag_type_error.capability_byte))
                                continue
                        previous_count = current_count
                        break
                    else:
                        if previous_count == 1:
                            self._raiseSpoolRemoved()
                        else:
                            time.sleep(0.1)
                        previous_count = 0

    @dbusif.method(out_signature="sb")
    def identifySpool(self):
        log.debug("identifySpool: identifying spool")
        tag_type, uid = self.writer.determineTagType()
        log.debug("identifySpool: {}, {}".format(tag_type, uid))
        has_content = self.writer.checkTagHasContent(tag_type)
        log.debug("identifySpool: {}".format(has_content))
        return uid, has_content

    ## @brief Trigger the spoolDetectedSignal but do some logging and conversion as well
    def _raiseSpoolDetected(self, spool_id, has_content):
        spool_id_str = _uidToStr(spool_id)
        log.debug("_raiseSpoolDetected(spool_id={id}, has_content={hc})".format(id=spool_id_str, hc=has_content))
        self.spoolDetected(spool_id_str, has_content)

    ## @brief Triggers the spoolDetected-signal. The spool_id is given to indicate which spool is detected
    @dbusif.signal("sb")  # Emits the spool ID when it is detected
    def spoolDetected(self, spool_id, has_content):
        log.debug("spoolDetected(spool_id='{id}', has_content={hc})".format(id=spool_id, hc=has_content))
        return True, True

    ## @brief Trigger the spoolRemovedSignal but do some logging and conversion as well
    def _raiseSpoolRemoved(self):
        log.debug("_raiseSpoolRemoved()")
        self.spoolRemoved()

    ## @brief Triggers the spoolRemoved-signal
    @dbusif.signal()
    def spoolRemoved(self):
        log.debug("spoolRemoved()")
        return True, True

    ## @brief export the traceability log file to the given destination path
    @dbusif.method("s")
    def exportTraceabilityLog(self, destination_path):
        self.traceability_logger.export(destination=destination_path)

if __name__ == "__main__":
    try:
        logging.basicConfig(filename=".FilamentSpoolInterfacingService.log", level=logging.DEBUG,
                            format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    except PermissionError:
        logging.basicConfig(level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

    import sys
    service = FilamentSpoolInterfacingService(use_dummy="use_dummy" in sys.argv)
    try:
        dbusif.runMainLoop()
    except KeyboardInterrupt:
        print("Exiting")
