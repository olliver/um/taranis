import random
import time
import unittest.mock
from threading import Event, Thread
from pynfc.ntag_read import NTagInfo, TagType

import logging
logging.basicConfig(level=logging.DEBUG)

UID_LENGTH = 7

ACTION_DURATION = 0.015

def chunks(seq, n):
    """Yield successive n-sized chunks from seq."""
    for i in range(0, len(seq), n):
        yield seq[i:i + n]


class DummyNtagTag216:
    def __init__(self, uid=None, user_memory=None):
        self._pages = [bytes([0, 0, 0, 0])] * 0xe6

        random_numbers = [random.randint(0, 255) for _ in range(UID_LENGTH)]
        _uid = uid if uid else bytes(random_numbers)
        _internal = bytes([0])
        _lock_bytes = bytes([0, 0])
        _capability_container = bytes([0, 0, 0, 0])
        # self.tag_type = TagType.NTAG_216

        self.write_page(0, bytes([_uid[0], _uid[1], _uid[2], 0x00]))
        self.write_page(1, bytes([_uid[3], _uid[4], _uid[5], 0x00]))
        self.write_page(2, bytes([_uid[6]]) + _internal + _lock_bytes)
        self.write_page(3, _capability_container)

        if user_memory:
            for userpage, chunk in enumerate(chunks(user_memory, NTagInfo.BYTES_PER_PAGE)):
                self.write_page(userpage + 4, chunk)

    def write_page(self, page, content):
        if len(content) != NTagInfo.BYTES_PER_PAGE:
            missing = NTagInfo.BYTES_PER_PAGE - len(content)
            content += bytes(missing)

        self._pages[page] = content

    def read_page(self, page):
        return self._pages[page]

    @property
    def uid(self):
        logging.debug("Getting uid")
        first_pages = b''.join(self._pages[:3])
        logging.debug("uid: {uid}".format(uid=first_pages))
        return first_pages[:UID_LENGTH]

    @property
    def tag_type(self):
        logging.debug("Getting tag_type")
        return TagType.NTAG_216


class DummyNtagReadWrite:
    def __init__(self, *args, **kwargs):
        self._tags = []
        self._selected_tag = None

        self._event = Event()
        self._sequence_thread = None

        self.open()

    def _sequence(self):
        history = [DummyNtagTag216()]
        while not self._event.is_set():
            logging.debug("_sequence: sleeping")
            time.sleep(1)

            if history and random.random() > 0.5:
                logging.debug("_sequence: taking tag from history (len={hlen}".format(hlen=len(history)))
                detected_tag = random.choice(history)
            else:
                logging.debug("_sequence: generating new tag")
                detected_tag = DummyNtagTag216()
                history += [detected_tag]

            self._tags = [detected_tag]

            logging.debug("_sequence: sleeping")
            time.sleep(5)
            logging.debug("_sequence: clearing tags")
            self._tags = []
            logging.debug("_sequence: sleeping")
            time.sleep(1)

    def open(self):
        logging.debug("open")

        self._sequence_thread = Thread(target=self._sequence)
        self._sequence_thread.start()

    def list_targets(self):
        logging.debug("list_targets")
        time.sleep(ACTION_DURATION)
        return [tag.uid for tag in self._tags]

    def count_targets(self):
        logging.debug("count_targets")
        time.sleep(ACTION_DURATION)
        return len(self._tags)

    def setup_target(self):
        logging.debug("setup_target")
        time.sleep(ACTION_DURATION)
        if self._tags:
            self._selected_tag = self._tags[0]
            return self._selected_tag.uid
        else:
            self._selected_tag = None
            raise IOError()

    def set_easy_framing(self, enable=True):
        logging.debug("set_easy_framing(enable={en})".format(en=enable))
        time.sleep(ACTION_DURATION)
        pass

    def transceive_bytes(self):
        logging.debug("transceive_bytes")
        raise NotImplementedError()

    def read_page(self, page):
        logging.debug("read_page({pg})".format(pg=page))
        time.sleep(ACTION_DURATION)
        return self._selected_tag.read_page(page)

    def determine_tag_type(self):
        logging.debug("determine_tag_type")
        time.sleep(ACTION_DURATION)
        self.setup_target()
        self.set_easy_framing()

        logging.debug("determine_tag_type: tt")
        if self._selected_tag:
            tt = self._selected_tag.tag_type
            logging.debug("determine_tag_type: tt={}".format(tt))
        else:
            logging.debug("tag_type cannot be determined")

        logging.debug("determine_tag_type: uid")
        uid = self._selected_tag.uid
        ret = (tt, uid)
        logging.debug("determine_tag_type, returning {}".format(ret))
        return ret

    def read_user_memory(self, tag_type):
        """Read the complete user memory, ie. the actual content of the tag.
        Configuration bytes surrounding the user memory is omitted"""
        logging.debug("read_user_memory(tag_type={tt})".format(tt=tag_type))
        time.sleep(ACTION_DURATION)
        start = tag_type['user_memory_start']
        end = tag_type['user_memory_end'] + 1  # + 1 because the Python range generator excluded the last value

        user_memory = []
        for page in range(start, end):
            user_memory += list(self.read_page(page))

        return bytes(user_memory)

    def fast_read_user_memory(self, *args, **kwargs):
        logging.debug("fast_read_user_memory")
        time.sleep(ACTION_DURATION)
        return self.read_user_memory(*args, **kwargs)

    def write_page(self, page, content, *args, **kwargs):
        logging.debug("write_page(page={pg}, content={c})".format(pg=page, c=content))
        time.sleep(ACTION_DURATION)
        return self._selected_tag.write_page(page, content)

    def write_user_memory(self, data, tag_type, debug=False):
        """Read the complete user memory, ie. the actual content of the tag.
        Configuration bytes surrounding the user memory are omitted, given the correct tag type.
        Otherwise, we cannot know where user memory start and ends"""
        logging.debug("write_user_memory")
        start = tag_type['user_memory_start']
        end = tag_type['user_memory_end'] + 1  # + 1 because the Python range generator excluded the last value
        mem_size = (end-start)

        page_contents = [data[i:i+NTagInfo.BYTES_PER_PAGE] for i in range(0, len(data), NTagInfo.BYTES_PER_PAGE)]
        content_size = len(page_contents)

        if content_size > mem_size:
            raise ValueError("{type} user memory ({mem_size} 4-byte pages) too small for content ({content_size} 4-byte pages)".
                             format(type=tag_type, mem_size=mem_size, content_size=content_size))

        logging.info("Writing {} pages".format(len(page_contents)))
        for page, content in zip(range(start, end), page_contents):
            self.write_page(page, content, debug)

    def authenticate(self, *args, **kwargs):
        logging.debug("authenticate")
        time.sleep(ACTION_DURATION)
        return True

    def enable_uid_mirror(self, *args, **kwargs):
        logging.debug("enable_uid_mirror")
        time.sleep(ACTION_DURATION)
        pass

    def check_uid_mirror(self, *args, **kwargs):
        logging.debug("check_uid_mirror")
        time.sleep(ACTION_DURATION)
        return unittest.mock.MagicMock()

    def set_password(self, *args, **kwargs):
        logging.debug("set_password")
        time.sleep(ACTION_DURATION)
        pass

    def close(self):
        logging.debug("close")
        self._event.set()
        self._sequence_thread.join()

