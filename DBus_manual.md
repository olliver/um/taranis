Reload config: 
```bash
dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig
```

To run a Taranis service
```bash
python3 -m taranis.services.materialPermissionService
pushd .; cd ~/git/opinicus; python3 main.py griffin.material.materialService MaterialService; popd
python3 -m taranis.services.filamentSpoolInterfacingService
```

Edit the DBus permissions:
```bash
sudo vim /etc/dbus-1/system.d/nl.ultimaker.griffin.conf
```

To call a method: 
```bash
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.material_permission /nl/ultimaker/material_permission nl.ultimaker.getPermittedMaterials
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.material_permission /nl/ultimaker/material_permission nl.ultimaker.updateMaterialPermissions
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.filament_spool_interfacing /nl/ultimaker/filament_spool_interfacing nl.ultimaker.initializeSpool string:"042db70af04981" string:"2f9d2279-9b0e-4765-bf9b-d1e1e13f3c49" int32:750000 string:"milligram" string:"TESTBATCH"
dbus-send --system --print-reply --type=method_call --dest=org.freedesktop.timedate1 /org/freedesktop/timedate1 org.freedesktop.timedate1.SetTimezone string:"Europe/Amsterdam" boolean:false
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.system /nl/ultimaker/system nl.ultimaker.getVersion
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.system /nl/ultimaker/system nl.ultimaker.checkForFirmwareUpdates
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.system /nl/ultimaker/system nl.ultimaker.startUpdate string:"testing"
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.system /nl/ultimaker/system nl.ultimaker.getUpdateState
dbus-send --system --print-reply --type=method_call --dest=nl.ultimaker.material /nl/ultimaker/material nl.ultimaker.getMaterialNameFields string:"6df69b13-2d96-4a69-a297-aedba667e710"
```

Listen to spoolDetected signal:
```bash
dbus-monitor --system "type='signal',interface='nl.ultimaker',member='spoolDetected'"  # prints some warning but works
```

Program spools in a loop:
```bash
python3 -m taranis.scripts.filamentServiceFrontend data/PLA/pla_black.json 
```

Interface definitions:
https://dbus.freedesktop.org/doc/dbus-specification.html
