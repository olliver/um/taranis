#! /usr/bin/env bash
python3 -m taranis.services.materialPermissionService &

pushd .
cd ~/git/opinicus
python3 main.py griffin.material.materialService MaterialService & 
popd

python3 -m taranis.services.filamentSpoolInterfacingService &
